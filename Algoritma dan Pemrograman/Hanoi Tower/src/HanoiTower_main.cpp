//file      :   HanoiTower_main.cpp
//author    :   steaKK

#include "HanoiTower.h"

int main() {
    HanoiTower H0;
    HanoiTower H1(3);
    HanoiTower H2(H1);
    H0 = H2;

    H0.set_data(0,3);
    H0.set_data(0,2);
    H0.set_data(1,1);
    H2 = H0;
    H0.print();
    H2.print();

    cout << "equal = " << H0.is_equal(H2) << endl;;



    return 0;
}
