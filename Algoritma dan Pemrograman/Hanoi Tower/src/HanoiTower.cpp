//file      :   HanoiTower.cpp
//author    :   steaKK

#include "HanoiTower.h"

HanoiTower::HanoiTower() {
    npole = DEFAULT_NPOLE;
    for(int i=0;i<npole;i++) {
        vector<int> temp;
        data.push_back(temp);
    }
}

HanoiTower::HanoiTower(int _npole) {
    npole = _npole;
    for(int i=0;i<npole;i++) {
        vector<int> temp;
        data.push_back(temp);
    }
}

HanoiTower::HanoiTower(const HanoiTower& _HanoiTower) {
    npole = _HanoiTower.npole;
    data = _HanoiTower.data;
}

HanoiTower& HanoiTower::operator=(const HanoiTower& _HanoiTower) {
    npole = _HanoiTower.npole;
    data = _HanoiTower.data;

    return *this;
}

HanoiTower::~HanoiTower() {

}

bool HanoiTower::operator==(const HanoiTower& _HanoiTower) {
    for(int i=0;i<data.size();i++) {
        for(int j=0;j<data[i].size();j++) {
            if(data[i][j]!=_HanoiTower.data[i][j]) return false;
        }
    }
    return true;
}

int HanoiTower::get_npole() {
    return npole;
}

int HanoiTower::get_data(int i) {
    return data[i].back();
}

void HanoiTower::set_npole(int _npole) {
    npole = _npole;
}

void HanoiTower::set_data(int i, int val) {
    data[i].push_back(val);
}

void HanoiTower::print() {
    cout << "npole = " << npole << endl;
    cout << "data = " << endl;
    for(int i=0;i<npole;i++) {
        if(data[i].empty()) cout << "empty" << endl;
        else {
            for(int j=0;j<data[i].size();j++) {
                cout << "[" << data[i][j] << "]" << " ";
            }
            cout << endl;
        }
    }
    cout << endl;
}

void HanoiTower::init() {
    set_data(0,3);
    set_data(0,2);
    set_data(0,1);
}

bool HanoiTower::is_equal(HanoiTower _HanoiTower) {
    if(npole!=_HanoiTower.get_npole()) {
        // cout << "kena di 1" << endl;
        return false;
    }
    else {
        for(int i=0;i<npole;i++) {
            if(data[i].size()!=_HanoiTower.data[i].size()) {
                // cout << "kena di 2" << endl;
                return false;
            }
            else {
                for(int j=0;j<data[i].size();j++) {
                    if(data[i][j]!=_HanoiTower.data[i][j]) {
                        // cout << "kena di 3" << endl;
                        return false;
                    }
                }
            }
        }
        // cout << "keluar" << endl;
        return true;
    }
}

bool HanoiTower::is_poleempty(int i) {
    if(data[i].empty()) return true;
    else return false;
}

bool HanoiTower::is_atobavailable(int a, int b) {
    if(data[a].empty()) return false;
    else {
        if(data[b].empty()) return true;
        else {
            if(data[a].back()<data[b].back()) return true;
            else return false;
        }
    }
}

bool HanoiTower::is_atobavailable(int a, int b) {
    if(abs(a-b)==1 && is_atobavailable(a,b)) return true;
    else return false;
}

bool HanoiTower::is_done() {
    for(int i=0;i<data.size()-1;i++) {
        if(!data[i].empty()) return false;
    }
    for(int i=0;i<data.back().size();i++) {
        if(data.back()[i]!=data.back().size()-i) return false;
    }
    return true;
}

void HanoiTower::move_atob(int a, int b) {
  data[b].push_back(data[a].back());
  data[a].pop_back();
}
