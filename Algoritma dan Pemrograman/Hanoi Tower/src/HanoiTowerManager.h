//file      :   HanoiTowerManager.h
//author    :   steaKK

#ifndef HanoiTowerManager_H
#define HanoiTowerManager_H

#include <iostream>
#include <omp.h>

#include "HanoiTower.h"

using namespace std;

class HanoiTowerManager {
public :
    HanoiTowerManager();
    HanoiTowerManager(HanoiTower);
    HanoiTowerManager(const HanoiTowerManager&);
    HanoiTowerManager& operator=(const HanoiTowerManager&);
    ~HanoiTowerManager();

    vector<HanoiTower> get_stack();
    vector<HanoiTower> get_trash();

    void print();

    bool is_ontrash(HanoiTower);

    void expand_tree();
    void expand_tree_t1();
    bool solve();
    bool solve_t1();

private :
    vector<HanoiTower> stack;
    vector<HanoiTower> trash;
};

#endif
