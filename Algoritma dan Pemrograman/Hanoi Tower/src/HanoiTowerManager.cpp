//file      :   HanoiTowerManager.cpp
//author    :   steaKK

#include "HanoiTowerManager.h"

HanoiTowerManager::HanoiTowerManager() {
    HanoiTower temp;
    temp.init();
    stack.push_back(temp);
    trash.push_back(temp);
}

HanoiTowerManager::HanoiTowerManager(HanoiTower _HanoiTower) {
    stack.push_back(_HanoiTower);
    trash.push_back(_HanoiTower);
}

HanoiTowerManager::HanoiTowerManager(const HanoiTowerManager& _HanoiTowerManager) {
    stack = _HanoiTowerManager.stack;
    trash = _HanoiTowerManager.trash;
}

HanoiTowerManager& HanoiTowerManager::operator=(const HanoiTowerManager& _HanoiTowerManager) {
    stack = _HanoiTowerManager.stack;
    trash = _HanoiTowerManager.trash;

    return *this;
}

HanoiTowerManager::~HanoiTowerManager() {

}

vector<HanoiTower> HanoiTowerManager::get_stack() {
    return stack;
}

vector<HanoiTower> HanoiTowerManager::get_trash() {
    return trash;
}

void HanoiTowerManager::print() {
    // cout << "stack size = " << stack.size() << endl;
    // cout << "trash size = " << trash.size() << endl;
    stack.back().print();
}

bool HanoiTowerManager::is_ontrash(HanoiTower _HanoiTower) {
    // cout << "trash size = " << trash.size() << endl;
    for(int i=0;i<trash.size();i++) {
        // cout << "ontrash ke " << i << endl;
        if(_HanoiTower.is_equal(trash[i])) return true;
    }
    return false;
}

// vector<HanoiTower> HanoiTowerManager::expand_tree() {
//     vector<HanoiTower> result;
//     HanoiTower temp = stack.back();
//     stack.pop_back();
//     for(int i=0;i<stack.back().get_npole();i++) {
//         for(int j=0;j<stack.back().get_npole();j++) {
//             if(stack.back().is_atobavailable(i,j)) {
//                 HanoiTower temp = stack.back();
//                 temp.move_atob(i,j);
//                 result.push_back(temp);
//             }
//         }
//     }
//     return result;
// }

void HanoiTowerManager::expand_tree() {
    vector<HanoiTower> result;
    HanoiTower current(stack.back());
    stack.pop_back();
    for(int i=0;i<current.get_npole();i++) {
        for(int j=0;j<current.get_npole();j++) {
            if(i!=j && current.is_atobavailable(i,j)) {
                HanoiTower temp(current);
                temp.move_atob(i,j);
                if(!is_ontrash(temp)) {
                    result.push_back(temp);
                }
            }
        }
    }
    for(int i=0;i<result.size();i++) {
        // cout << "aloha pushing" << endl;
        // result[i].print();
        stack.push_back(result[i]);
        trash.push_back(result[i]);
    }
}

void HanoiTowerManager::expand_tree_t1() {
    vector<HanoiTower> result;
    HanoiTower current(stack.back());
    stack.pop_back();
    for(int i=0;i<current.get_npole();i++) {
        for(int j=0;j<current.get_npole();j++) {
            if(i!=j && current.is_atobavailable_t1(i,j)) {
                HanoiTower temp(current);
                temp.move_atob(i,j);
                if(!is_ontrash(temp)) {
                    result.push_back(temp);
                }
            }
        }
    }
    for(int i=0;i<result.size();i++) {
        // cout << "aloha pushing" << endl;
        // result[i].print();
        stack.push_back(result[i]);
        trash.push_back(result[i]);
    }
}

bool HanoiTowerManager::solve_t1() {
    if(stack.back().is_done()) return true;
    else {
        expand_tree_t1();
        solve();
    }
}

bool HanoiTowerManager::solve() {
    if(stack.back().is_done()) return true;
    else {
        expand_tree();
        solve();
    }
}
