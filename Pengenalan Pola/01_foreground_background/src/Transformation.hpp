//filename	:	Transformation.hpp
//author	:	steaKK

#ifndef Transformation_HPP
#define Transformation_HPP

#include <iostream>
#include "CImg.h"

using namespace std;
using namespace cimg_library;

class Transformation {
public:
	// RGB TO GRAYSCALE
	static CImg<unsigned char> obsessive_coffee(CImg<unsigned char>);

private:

};

#endif
