//filename	:	Histogram.cpp
//author	:	steaKK

#include "Histogram.hpp"

Histogram::Histogram() {
	size = DEFAULT_SIZE;
	channel = DEFAULT_CHANNEL;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
}

Histogram::Histogram(int _size) {
	size = _size;
	channel = DEFAULT_CHANNEL;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
}

Histogram::Histogram(int _size, int _channel) {
	size = _size;
	channel = DEFAULT_CHANNEL;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
}

Histogram::Histogram(CImg<unsigned char> image) {
	size = DEFAULT_SIZE;
	channel = image.spectrum();
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
	for(int i=0;i<image.height();i++) {
		for(int j=0;j<image.width();j++) {
			for(int k=0;k<image.depth();k++) {
				for(int l=0;l<image.spectrum();l++) {
					data[channel*size+image(j,i,k,l)]++;
				}
			}
		}
	}
}

Histogram::Histogram(const Histogram& _Histogram) {
	size = _Histogram.size;
	channel = _Histogram.channel;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = _Histogram.data[i];
}

Histogram& Histogram::operator=(const Histogram& _Histogram) {
	size = _Histogram.size;
	channel = _Histogram.channel;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = _Histogram.data[i];
	return *this;
}

Histogram::~Histogram() {
	delete[] data;
}

int Histogram::get_size() {
	return size;
}

int Histogram::get_channel() {
	return channel;
}

int Histogram::get_data(int chn, int pos) {
	return data[chn*pos+pos];
}

void Histogram::set_data(int pos, int chn, int val) {
	data[chn*pos+pos] = val;
}

void Histogram::print() {
	if(channel<1) cout << "no legal channel number" << endl;
	else if (channel==1) {
		cout << "GRAY" << endl;
		for(int i=0;i<size;i++) cout << data[i] << " ";
		cout << endl;
	}
	else {
		for(int i=0;i<channel;i++) {
			if(i==0) cout << "RED" << endl;
			else if(i==1) cout << "GREEN" << endl;
			else cout << "BLUE" << endl;
			for(int j=0;j<size;j++) {
				cout << get_data(i,j) << " ";
			}
			cout << endl;
		}
	}
}

int Histogram::get_n_value() {
	int result = 0;
	for(int i=0;i<size*channel;i++) result += data[i];
	return result;
}

int Histogram::get_balanced_treshold(int depth) {
	int median = get_n_value()/2;
	int iter = 0;
	for(int i=0;i<depth;i++) {
		for(int j=0;j<data[i*size+j];j++) {
			iter++;
			if(iter==median) return i;
		}
	}
	return -1;
}

int Histogram::otsu_method(int depth) {
	int sumB = 0;
	int wB = 0;
	float maximum = 0.0;
	int sum1 = 0;
	for(int i=0;i<size;i++) sum1 += i*get_data(depth,i);
	for(int i=0;i<size;i++) {
		wB += get_data(depth,i);
		int wF = get_n_value() - wB;
		if(wB==0) {
			if(wF<=0) return i;
		}
		sumB += i*get_data(depth,i);
		int mB = sumB/wB;
		int mF = (sum1 - sumB) / wF;
		int between = wB * wF * (mB - mF) * (mB - mF);
		if(between >= maximum) {
			maximum = between;
			return i;
		}
	}
	return -1;
}
