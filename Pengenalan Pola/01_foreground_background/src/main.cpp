//filename	:	main.cpp
//author	:	steaKK

#include "Segmentation.hpp"

int main() {
	CImg<unsigned char> image("lena.jpg");

	//Histogram
	Histogram histogram(image);
	// histogram.print();

	CImg<unsigned char> result(Segmentation::thresholding(image,128));
	result.display();

	return 0;
}
