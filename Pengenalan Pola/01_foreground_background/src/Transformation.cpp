//filename	:	Transformation.cpp
//author	:	steaKK

#include "Transformation.hpp"

CImg<unsigned char> Transformation::obsessive_coffee(CImg<unsigned char> image) {
	CImg<unsigned char> result(image.width(), image.height(), 1, 1, 0);
	cimg_forXY(image,x,y) {
		int R = (int)image(x,y,0,0);
	    int G = (int)image(x,y,0,1);
	    int B = (int)image(x,y,0,2);
		int grayValueWeight = (int)(0.299*R + 0.587*G + 0.114*B);
		result(x,y,0,0) = grayValueWeight;
	}
	return result;
}
