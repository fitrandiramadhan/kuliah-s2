//filename	:	Histogram_Main.hpp
//author	:	steaKK

#include "Histogram.hpp"

int main() {
	CImg<unsigned char> image("lena.jpg");
	Histogram histogram(image);

	histogram.print();

	return 0;
}
