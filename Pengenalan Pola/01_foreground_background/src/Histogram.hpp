//filename	:	Histogram.hpp
//author	:	steaKK

#ifndef Histogram_HPP
#define Histogram_HPP

#include <iostream>

#include "CImg.h"
#define cimg_use_magick

using namespace std;
using namespace cimg_library;

class Histogram {
public:
	Histogram();
	Histogram(int);
	Histogram(int,int);
	Histogram(CImg<unsigned char>);
	Histogram(const Histogram&);
	Histogram& operator=(const Histogram&);
	~Histogram();

	int get_size();
	int get_channel();
	int get_data(int,int);
	void set_data(int,int,int);

	void print();

	int get_n_value();
	int get_balanced_treshold(int);
	int otsu_method(int);

private:
	static const int DEFAULT_SIZE = 256;
	static const int DEFAULT_CHANNEL = 3;

	int size;
	int channel;
	int* data;
};

#endif
