//filename	:	Segmentation.cpp
//author	:	steaKK

#include "Segmentation.hpp"

CImg<unsigned char> Segmentation::thresholding(CImg<unsigned char> image, int threshold) {
	CImg<unsigned char> result(image.width(),image.height(),1,1);
	cimg_forXY(image,x,y) {
		if(image(x,y,0,0) < threshold) result(x,y,0,0) = 0;
		else result(x,y,0,0) = 255;
	}
	return result;
}
