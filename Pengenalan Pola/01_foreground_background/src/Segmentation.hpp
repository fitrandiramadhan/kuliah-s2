//filename	:	Segmentation.hpp
//author	:	steaKK

#ifndef Segmentation_HPP
#define Segmentation_HPP

#include <iostream>
#include "CImg.h"

#include "Transformation.hpp"
#include "Histogram.hpp"

using namespace std;
using namespace cimg_library;

class Segmentation {
public:
	//FOREGROUND BACKGROUND
	static CImg<unsigned char> thresholding(CImg<unsigned char>, int);

private:

};

#endif
