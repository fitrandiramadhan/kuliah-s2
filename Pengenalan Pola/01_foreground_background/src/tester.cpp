//filename	:	tester.cpp
//author	:	steaKK

#include "CImg.h"

#include <stdlib.h>

#define cimg_use_magick

using namespace cimg_library;

int main() {

	CImg<unsigned char> image("lena.jpg");

	image.display();

	return 0;

}
