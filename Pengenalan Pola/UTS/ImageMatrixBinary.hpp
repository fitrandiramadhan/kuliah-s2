//filename	:	ImageMatrixBinary.hpp
//author	:	steaKK

#ifndef ImageMatrixBinary_HPP
#define ImageMatrixBinary_HPP

#include <iostream>

#include "CImg.h"

#define cimg_use_magick

using namespace std;
using namespace cimg_library;

class ImageMatrixBinary {

public:
	//CONSTRUCTORS
	ImageMatrixBinary();
	ImageMatrixBinary(int,int);
	ImageMatrixBinary(CImg<unsigned char>);
	ImageMatrixBinary(const ImageMatrixBinary&);
	ImageMatrixBinary& operator=(const ImageMatrixBinary&);
	~ImageMatrixBinary();

	//GETTERSETTER
	int get_height();
	void set_height(int);
	int get_width();
	void set_width(int);
	bool get_data(int,int);
	void set_data(int,int,bool);

	//TESTER
	void print();
	CImg<unsigned char> make_image();

	//METHOD
	ImageMatrixBinary inverse();
	bool is_edge_pixel(int,int);
	bool is_out_of_bounds(int,int);
	ImageMatrixBinary make_local(int,int);


private:
	//CONSTANT
	static const int DEFAULT_WIDTH = 100;
	static const int DEFAULT_HEIGHT = 100;

	//ATTRIBUTE
	int width;		//width of image (pixels)
	int height;	//height of image (pixels)

	//DATA
	bool* data;
};

#endif
