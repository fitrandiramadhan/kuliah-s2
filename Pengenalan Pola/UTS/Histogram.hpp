//filename	:	Histogram.hpp
//author	:	steaKK

#ifndef Histogram_HPP
#define Histogram_HPP

#include <iostream>

#include "CImg.h"
#define cimg_use_magick

using namespace std;
using namespace cimg_library;

class Histogram {
public:
	//CONSTRUCTOR
	Histogram();
	Histogram(int,int);
	Histogram(MatrixImage);
	Histogram(const Histogram&);
	Histogram& operator=(const Histogram&);
	~Histogram();

	//GETTERSETTER
	int get_spectrum();
	int get_channel();
	int get_data(int,int);
	void set_data(int,int,int);

	//TESTER
	void print();

	//METHOD


private:
	static const int DEFAULT_SPECTRUM = 256;
	static const int DEFAULT_CHANNEL = 3;

	int spectrum;
	int channel;
	int* data;
};

#endif
