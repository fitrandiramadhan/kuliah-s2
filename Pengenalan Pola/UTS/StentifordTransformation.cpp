//filename	:	StentifordTransformation.cpp
//author	:	steaKK

#include "StentifordTransformation.cpp"

StentifordTransformation::StentifordTransformation() {
	height = _height;
	width = _width;
	data = new bool[height*width];
	for(int i=0;i<height*width;i++) data[i] = false;
}

StentifordTransformation::StentifordTransformation(MatrixImageBinary _image) {
	height = _image.height();
	width = _image.width();
	data = new bool[height*width];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			if(_image(j,i,0,0)<128) data[i*width+j] = false;
			else data[i*width+j] = true;
		}
	}
}

StentifordTransformation::~StentifordTransformation() {
	delete[] data;
}

MatrixImageBinary StentifordTransformation::transform() {
	int n = 0;
	do {
		n=0;
		vector<Point> v = get_t1(); n+=v.size();
		for(int i=0;i<v.size();i++) image.set_data(v[i].get_y(),v[i].get_x(),false);
		v = get_t2(); n+=v.size();
		for(int i=0;i<v.size();i++) image.set_data(v[i].get_y(),v[i].get_x(),false);
		v = get_t3(); n+=v.size();
		for(int i=0;i<v.size();i++) image.set_data(v[i].get_y(),v[i].get_x(),false);
		v = get_t4(); n+=v.size();
		for(int i=0;i<v.size();i++) image.set_data(v[i].get_y(),v[i].get_x(),false);
	} while(n>0);
	return image;
}

vector<Point> StentifordTransformation::get_t1() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(m.get_data(i,j)) {
				MatrixImageBinary (image.make_local(i,j));
				if(m.is_t1()) {
					if(m.is_curve_end()) {
						Point p(j,i);
						result.push_back(p);
					}
				}
			}
		}
	}
	return result;
}

vector<Point> StentifordTransformation::get_t2() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(m.get_data(i,j)) {
				MatrixImageBinary m(image.make_local(i,j));
				if(m.is_t2()) {
					if(m.is_curve_end()) {
						Point p(j,i);
						result.push_back(p);
					}
				}
			}
		}
	}
	return result;
}

vector<Point> StentifordTransformation::get_t3() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(m.get_data(i,j)) {
				MatrixImageBinary m(image.make_local(i,j));
				if(m.is_t3()) {
					if(m.is_curve_end()) {
						Point p(j,i);
						result.push_back(p);
					}
				}
			}
		}
	}
	return result;
}

vector<Point> StentifordTransformation::get_t4() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(m.get_data(i,j)) {
				MatrixImageBinary m(image.make_local(i,j));
				if(m.is_t4()) {
					if(m.is_curve_end()) {
						Point p(j,i);
						result.push_back(p);
					}
				}
			}
		}
	}
	return result;
}

bool StentifordTransformation::is_t1(int i, int j) {
	return data[(i+1)*width+j] && data[(i-1)*width+j];
}

bool StentifordTransformation::is_t2(int i, int j) {
	return data[i*width+(j+1)] && data[i*width+(j-1)];
}

bool StentifordTransformation::is_t3(int i, int j) {
	return data[(i-1)*width+j] && data[(i+1)*width+j];
}

bool StentifordTransformation::is_t4(int i, int j) {
	return data[i*width+(j-1)] && data[i*width+(j+1)];
}

bool StentifordTransformation::is_curve_end(int i, int j) {
	return ((
		data[(i)*width+(j)] + data[(i)*width+(j)] +
		data[(i)*width+(j)] + data[(i)*width+(j)] +
		data[(i)*width+(j)] + data[(i)*width+(j)] +
		data[(i)*width+(j)] + data[(i)*width+(j)] +
		) == 1);

	return	(m.get_data(0,0) + m.get_data(0,1) + m.get_data(0,2) + m.get_data(1,0) +
			m.get_data(1,2) + m.get_data(2,0) + m.get_data(2,1) + m.get_data(2,2))
			== 1;
}
