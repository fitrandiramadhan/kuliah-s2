//filename	:	StentifordTransformation.hpp
//author	:	steaKK

#ifndef StentifordTransformation_HPP
#define StentifordTransformation_HPP

#include <iostream>
#include <vector>

#include "CImg.h"

#define cimg_use_magick

using namespace std;
using namespace cimg_library;

struct Point {
	int x;
	int y;
};

class StentifordTransformation {

	//CONSTRUCTOR
	StentifordTransformation(int _height, int _width);
	StentifordTransformation(CImg<unsigned char> _image)
	~StentifordTransformation();

	//METHOD
	MatrixImageBinary transform();

	vector<Point> get_t1();
	vector<Point> get_t2();
	vector<Point> get_t3();
	vector<Point> get_t4();

	bool is_t1(MatrixImageBinary);
	bool is_t2(MatrixImageBinary);
	bool is_t3(MatrixImageBinary);
	bool is_t4(MatrixImageBinary);

	bool is_curve_end(MatrixImageBinary);

	//UTILITY
	void clear_edge();
	CImg<unsigned char> make_image();
	void print();

private:
	//ATTRIBUTES
	int height;
	int width;
	bool* data;
};

#endif
