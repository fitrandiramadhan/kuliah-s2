//filename	:	SegmentationTransformation.hpp
//author	:	steaKK

#ifndef SegmentationTransformation_HPP
#define SegmentationTransformation_HPP

#include <iostream>

#include "CImg.h"

#include "ImageMatrix.hpp"
#include "RGBGrayscaleTransformation.hpp"
#include "ImageMatrixBinary.hpp"
// #include "Histogram.hpp"

using namespace std;

class SegmentationTransformation {
public:
	SegmentationTransformation();
	SegmentationTransformation(ImageMatrix source);
	~SegmentationTransformation();

	//FOREGROUND BACKGROUND
	ImageMatrixBinary transform();

private:
	ImageMatrix image;
};

#endif
