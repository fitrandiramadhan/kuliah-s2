//filename	:	ChainCode.cpp
//author	:	steaKK

#include "ChainCode.hpp"

ChainCode::ChainCode(int _height, int _width) {
	height = _height;
	width = _width;
	data = new bool[height*width];
	for(int i=0;i<height*width;i++) data[i] = false;
}

ChainCode::ChainCode(CImg<unsigned char> _image) {
	height = _image.height();
	width = _image.width();
	data = new bool[height*width];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) data[i*width+j] = 255/_image(j,i,0,0);
	}
}

ChainCode::~ChainCode() {
	delete[] data;
}

Point ChainCode::get_anchor() {
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(data[i*width+j]) {
				Point p; p.y=i; p.x=j;
				return p;
			}
		}
	}
	Point p; p.y=-1; p.x=-1;
	return p;
}

void ChainCode::transform_border() {
	//get_border
	vector<Point> v;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(!data[i*width+j]) {
				if(	data[(i-1)*width+(j-1)] || data[(i-1)*width+j] ||
					data[(i-1)*width+(j+1)] || data[i*width+(j-1)] ||
					data[i*width+(j+1)] || data[(i+1)*width+(j-1)] ||
					data[(i+1)*width+j] || data[(i+1)*width+(j+1)] ) {
					Point p; p.y=i; p.x=j;
					v.push_back(p);
				}
			}
		}
	}

	//clear
	for(int i=0;i<height*width;i++) data[i] = false;

	//draw_border
	for(int i=0;i<v.size();i++) data[(v[i].y)*width+(v[i].x)] = true;
}

void ChainCode::clear_edge() {
	for(int i=0;i<height;i++) {
		data[i*width+0] = false;
		data[(i+1)*width-1] = false;
	}
	for(int i=0;i<width;i++) {
		data[i] = false;
		data[((height-1)*width)+i] = false;
	}
}

void ChainCode::print() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			cout << data[i*width+j] << " ";
		}
		cout << endl;
	}
}

CImg<unsigned char> ChainCode::make_image() {
	CImg<unsigned char> result(width,height,1,1);
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			result(j,i,0,0) = (int)(data[i*width+j]*255);
		}
	}
	return result;
}
