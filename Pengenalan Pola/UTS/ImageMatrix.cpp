//filename	:	ImageMatrix.cpp
//author	:	steaKK

#include "ImageMatrix.hpp"

//CONSTRUCTORS
ImageMatrix::ImageMatrix() {
	height = DEFAULT_HEIGHT;
	width = DEFAULT_WIDTH;
	depth = DEFAULT_DEPTH;
	channel = DEFAULT_CHANNEL;
	spectrum = DEFAULT_SPECTRUM;

	data = new int[height*width*depth*channel];
	for(int i=0;i<height*width*depth*channel;i++) data[i] = 0;
}

ImageMatrix::ImageMatrix(int _height, int _width, int _depth, int _channel, int _spectrum) {
	height = _height;
	width = _width;
	depth = _depth;
	channel = _channel;
	spectrum = _spectrum;

	data = new int[height*width*depth*channel];
	for(int i=0;i<height*width*depth*channel;i++) data[i] = 0;
}

ImageMatrix::ImageMatrix(CImg<unsigned char> image) {
	height = image.height();
	width = image.width();
	depth = image.depth();
	channel = image.spectrum();
	spectrum = DEFAULT_SPECTRUM;

	data = new int[height*width*depth*channel];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			for(int k=0;k<depth;k++) {
				for(int c=0;c<channel;c++) {
					data[channel*(depth*(width*i+j)+k)+c] = image(j,i,k,c);
				}
			}
		}
	}
}

ImageMatrix::ImageMatrix(const ImageMatrix& _ImageMatrix) {
	height = _ImageMatrix.height;
	width = _ImageMatrix.width;
	depth = _ImageMatrix.depth;
	channel = _ImageMatrix.channel;
	spectrum = _ImageMatrix.spectrum;

	data = new int[height*width*depth*channel];
	for(int i=0;i<height*width*depth*channel;i++) data[i] = _ImageMatrix.data[i];
}

ImageMatrix& ImageMatrix::operator=(const ImageMatrix& _ImageMatrix) {
	height = _ImageMatrix.height;
	width = _ImageMatrix.width;
	depth = _ImageMatrix.depth;
	channel = _ImageMatrix.channel;
	spectrum = _ImageMatrix.spectrum;

	for(int i=0;i<height*width*depth*channel;i++) data[i] = _ImageMatrix.data[i];

	return *this;
}

ImageMatrix::~ImageMatrix() {
	delete[] data;
}

//GETTERSETTER
int ImageMatrix::get_height() {
	return height;
}

void ImageMatrix::set_height(int _height) {
	height = _height;
}

int ImageMatrix::get_width() {
	return width;
}

void ImageMatrix::set_width(int _width) {
	width = _width;
}

int ImageMatrix::get_depth() {
	return depth;
}

void ImageMatrix::set_depth(int _depth) {
	depth = _depth;
}

int ImageMatrix::get_channel() {
	return channel;
}

void ImageMatrix::set_channel(int _channel) {
	channel = _channel;
}

int ImageMatrix::get_spectrum() {
	return spectrum;
}

void ImageMatrix::set_spectrum(int _spectrum) {
	spectrum = _spectrum;
}

int ImageMatrix::get_data(int i, int j, int k, int c) {
	return data[channel*(depth*(width*i+j)+k)+c];
}

void ImageMatrix::set_data(int i, int j, int k, int c, int value) {
	data[channel*(depth*(width*i+j)+k)+c] = value;
}

//TESTER
void ImageMatrix::print() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			for(int k=0;k<depth;k++) {
				for(int c=0;c<channel;c++) {
					cout << data[channel*(depth*(width*i+j)+k)+c] << " ";
				}
			}
		}
	}
}

CImg<unsigned char> ImageMatrix::make_image() {
	CImg<unsigned char> result(width,height,depth,channel);
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			for(int k=0;k<depth;k++) {
				for(int c=0;c<channel;c++) {
					result(j,i,k,c) = data[channel*(depth*(width*i+j)+k)+c];
				}
			}
		}
	}
	return result;
}

//METHOD
ImageMatrix ImageMatrix::inverse() {
	ImageMatrix result(height,width,depth,channel,spectrum);
	for(int i=0;i<height*width*depth*channel;i++) result.data[i] = spectrum-data[i]-1;
	return result;
}
