//filename	:	Histogram.cpp
//author	:	steaKK

#include "Histogram.hpp"

Histogram::Histogram() {
	spectrum = DEFAULT_SPECTRUM;
	channel = DEFAULT_CHANNEL;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
}

Histogram::Histogram(int _spectrum, int _channel) {
	spectrum = _spectrum;
	channel = _channel;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
}

Histogram::Histogram(MatrixImage image) {
	spectrum = image.get_spectrum();
	channel = image.get_channel();
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = 0;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			for(int a=0;a<image.get_channel();a++) {
				data[image.get_data(i,j,k)*a+a]++;
			}
		}
	}
}

Histogram::Histogram(const Histogram& _Histogram) {
	size = _Histogram.size;
	channel = _Histogram.channel;
	data = new int[size*channel];
	for(int i=0;i<size*channel;i++) data[i] = _Histogram.data[i];
}

Histogram& Histogram::operator=(const Histogram& _Histogram) {
	size = _Histogram.size;
	channel = _Histogram.channel;
	for(int i=0;i<size*channel;i++) data[i] = _Histogram.data[i];
	return *this;
}

Histogram::~Histogram() {
	delete[] data;
}

int Histogram::get_spectrum() {
	return spectrum;
}

int Histogram::get_channel() {
	return channel;
}

int Histogram::get_data(int chn, int pos) {
	return data[pos*chn+pos];
}

void Histogram::set_data(int chn, int pos, int val) {
	data[pos*chn+pos] = val;
}

void Histogram::print() {
	for(int i=0;i<channel;i++) {
		for(int j=0;j<size;j++) {
			cout << data[pos*chn+pos] << " ";
		}
		cout << endl;
	}
}
