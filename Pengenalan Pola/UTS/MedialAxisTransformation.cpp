//filename	:	MedialAxisTransformation.cpp
//author	:	steaKK

MedialAxisTransformation::MedialAxisTransformation(MatrixImageBinary _m) {
	m = _m;
}

MedialAxisTransformation::~MedialAxisTransformation() {

}

MatrixImageBinary MedialAxisTransformation::transform() {
	List<Point> lp=new List<Point>(get_border());
	do {
		for(int i=0;i<lp.Count;i++) {
			if(!is_curve_end(lp[i])) {
				if(is_simple(m,lp[i])) m.data[lp[i].x,lp[i].y]=0;
			}
		}
		lp=get_border();
	} while(lp.Count>0);
	return m;
}

List<Point> MedialAxisTransformation::List<Point> get_border() {
	List<Point> result = new List<Point>();
	for(int i=0;i<m.width;i++) {
		for(int j=0;j<m.height;j++) {
			Point p = new Point(i,j);
			if(is_border(p)) result.Add(p);
		}
	}
	return result;
}

bool MedialAxisTransformation::is_simple(MatrixImageBinary m, Point p) {
	MatrixImageBinary m_local = new MatrixImageBinary(m);
	m_local.data[p.x,p.y] = 0;
	if(count_local_object(m,p).is_equal(count_local_object(m_local,p))) return true;
	else return false;
}

Point MedialAxisTransformation::count_local_object(MatrixImageBinary m, int x, int y) {
	Point result(0,0);

	MatrixImageBinary m_global(m.make_local(y,x));

	MatrixImageBinary m_local = new MatrixImageBinary(3,3,0);
	Point result(0,0);
	for(int i=0;i<3;i++) {
		for(int j=0;j<3;j++) {
			Point p_local = new Point(i,j);
			if(m_local.data[i,j]==0) {
				if(is_object_local(m_global,p_local)) {
					n.x++;
					m_local.data[i,j]=n.x;
					if(!is_out_of_bounds_local(m_local,p_local.top())) {
						if(is_object_local(m_global,p_local.top())) m_local.data[p_local.top().x,p_local.top().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.bottom())) {
						if(is_object_local(m_global,p_local.bottom())) m_local.data[p_local.bottom().x,p_local.bottom().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.left())) {
						if(is_object_local(m_global,p_local.left())) m_local.data[p_local.left().x,p_local.left().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.right())) {
						if(is_object_local(m_global,p_local.right())) m_local.data[p_local.right().x,p_local.right().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.top_left())) {
						if(is_object_local(m_global,p_local.top_left())) m_local.data[p_local.top_left().x,p_local.top_left().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.top_right())) {
						if(is_object_local(m_global,p_local.top_right())) m_local.data[p_local.top_right().x,p_local.top_right().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.bottom_left())) {
						if(is_object_local(m_global,p_local.bottom_left())) m_local.data[p_local.bottom_left().x,p_local.bottom_left().y]=n.x;
					}
					if(!is_out_of_bounds_local(m_local,p_local.bottom_right())) {
						if(is_object_local(m_global,p_local.bottom_right())) m_local.data[p_local.bottom_right().x,p_local.bottom_right().y]=n.x;
					}
				}
				else {
					n.y++;
					m_local.data[i,j]=n.y;
					if(!is_out_of_bounds_local(m_local,p_local.top())) {
						if(!is_out_of_bounds_local(m_global,p_local.top())) {
							if(!is_object_local(m_global,p_local.top())) m_local.data[p_local.top().x,p_local.top().y]=n.y;
						}
					}
					if(!is_out_of_bounds_local(m_local,p_local.bottom())) {
						if(!is_out_of_bounds_local(m_global,p_local.bottom())) {
							if(!is_object_local(m_global,p_local.bottom())) m_local.data[p_local.bottom().x,p_local.bottom().y]=n.y;
						}
					}
					if(!is_out_of_bounds_local(m_local,p_local.left())) {
						if(!is_out_of_bounds_local(m_global,p_local.left())) {
							if(!is_object_local(m_global,p_local.left())) m_local.data[p_local.left().x,p_local.left().y]=n.y;
						}
					}
					if(!is_out_of_bounds_local(m_local,p_local.right())) {
						if(!is_out_of_bounds_local(m_global,p_local.right())) {
							if(!is_object_local(m_global,p_local.right())) m_local.data[p_local.right().x,p_local.right().y]=n.y;
						}
					}
				}
			}
		}
	}
	return n;
}

bool MedialAxisTransformation::is_curve_end(MatrixImageBinary m) {
	int sum =
		m.get_data(0,0) + m.get_data(0,1) + m.get_data(0,2) + m.get_data(1,0) +
		m.get_data(1,2) + m.get_data(2,0) + m.get_data(2,1) + m.get_data(2,2);

	if(sum==1) return true;
	else return false;
}

bool MedialAxisTransformation::is_border(MatrixImageBinary m) {
	int sum =
		m.get_data(0,0) + m.get_data(0,1) + m.get_data(0,2) + m.get_data(1,0) +
		m.get_data(1,2) + m.get_data(2,0) + m.get_data(2,1) + m.get_data(2,2);
	if(sum>1) return true;
	else return false;
}

bool MedialAxisTransformation::is_out_of_bounds(Point p) {
	if(p.x<0 || p.y<0 || p.x>=m.width || p.y>=m.height) return true;
	else return false;
}

bool MedialAxisTransformation::is_object_local(MatrixImageBinary m_local, Point p) {
	if(!is_out_of_bounds_local(m_local,p)) {
		if(m_local.data[p.x,p.y]==1) return true;
		else return false;
	}
	else return false;
}

bool MedialAxisTransformation::is_out_of_bounds_local(MatrixImageBinary m_local, Point p) {
	if(p.x<0 || p.y<0 || p.x>=m_local.width || p.y>=m_local.height) return true;
	else return false;
}
