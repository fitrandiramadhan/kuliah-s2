//filename	:	Segmentation.cpp
//author	:	steaKK

#include "SegmentationTransformation.hpp"

SegmentationTransformation::SegmentationTransformation() {

}

SegmentationTransformation::SegmentationTransformation(ImageMatrix _image) : image(_image) {

}

SegmentationTransformation::~SegmentationTransformation() {

}

ImageMatrixBinary SegmentationTransformation::transform() {
	ImageMatrixBinary result(image.get_height(),image.get_width());
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(image.get_data(i,j,0,0)<128) result.set_data(i,j,true);
			else result.set_data(i,j,false);
		}
	}
	return result;
}
