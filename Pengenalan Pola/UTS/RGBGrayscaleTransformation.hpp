//filename	:	RGBGrayscaleTransformation.hpp
//author	:	steaKK

#ifndef RGBGrayscaleTransformation_HPP
#define RGBGrayscaleTransformation_HPP

#include <iostream>

#include "ImageMatrix.hpp"

using namespace std;

class RGBGrayscaleTransformation {
public:
	RGBGrayscaleTransformation();
	RGBGrayscaleTransformation(ImageMatrix _image);
	~RGBGrayscaleTransformation();

	ImageMatrix transform();

private:
	ImageMatrix image;
};

#endif
