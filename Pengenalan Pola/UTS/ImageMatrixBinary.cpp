//filename	:	ImageMatrixBinary.cpp
//author	:	steaKK

#include "ImageMatrixBinary.hpp"

//CONSTRUCTORS
ImageMatrixBinary::ImageMatrixBinary() {
	width = DEFAULT_WIDTH;
	height = DEFAULT_HEIGHT;

	data = new bool[width*height];
	for(int i=0;i<width*height;i++) data[i] = false;
}

ImageMatrixBinary::ImageMatrixBinary(int _height, int _width) {
	width = _width;
	height = _height;

	data = new bool[width*height];
	for(int i=0;i<width*height;i++) data[i] = false;
}

ImageMatrixBinary::ImageMatrixBinary(CImg<unsigned char> image) {
	width = image.width();
	height = image.height();

	data = new bool[width*height];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			if(image(j,i,0,0)>128) data[i*width+j] = 0;
			else data[i*width+j] = 1;
		}
	}
}

ImageMatrixBinary::ImageMatrixBinary(const ImageMatrixBinary& _ImageMatrixBinary) {
	width = _ImageMatrixBinary.width;
	height = _ImageMatrixBinary.height;

	data = new bool[width*height];
	for(int i=0;i<width*height;i++) data[i] = _ImageMatrixBinary.data[i];
}

ImageMatrixBinary& ImageMatrixBinary::operator=(const ImageMatrixBinary& _ImageMatrixBinary) {
	width = _ImageMatrixBinary.width;
	height = _ImageMatrixBinary.height;

	for(int i=0;i<width*height;i++) data[i] = _ImageMatrixBinary.data[i];

	return *this;
}

ImageMatrixBinary::~ImageMatrixBinary() {
	delete[] data;
}

//GETTERSETTER
int ImageMatrixBinary::get_height() {
	return height;
}

void ImageMatrixBinary::set_height(int _height) {
	height = _height;
}

int ImageMatrixBinary::get_width() {
	return width;
}

void ImageMatrixBinary::set_width(int _width) {
	width = _width;
}

bool ImageMatrixBinary::get_data(int i, int j) {
	return data[i*j+j];
}

void ImageMatrixBinary::set_data(int i, int j, bool value) {
	data[i*width+j] = value;
}

//TESTER
void ImageMatrixBinary::print() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			cout << data[i*width+j] << " ";
		}
		cout << endl;
	}
}

CImg<unsigned char> ImageMatrixBinary::make_image() {
	CImg<unsigned char> result(width,height,1,1);
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			if(data[i*width+j]) result(j,i,0,0) = 255;
			else result(j,i,0,0) = 0;
		}
	}
	return result;
}

//METHOD
ImageMatrixBinary ImageMatrixBinary::inverse() {
	ImageMatrixBinary result(height,width);
	for(int i=0;i<height*width;i++) result.data[i] = !data[i];
	return result;
}

bool ImageMatrixBinary::is_edge_pixel(int y, int x) {
	if(y==0 || x==0 || y==height-1 || x==width-1) return true;
	else return false;
}

bool ImageMatrixBinary::is_out_of_bounds(int y,int x) {
	if(y<0 || x<0 || y>=height || x>=width) return true;
	else return false;
}

ImageMatrixBinary ImageMatrixBinary::make_local(int y, int x) {
	ImageMatrixBinary result(3,3);
	for(int i=0;i<3;i++) {
		for(int j=0;j<3;j++) {
			if(is_out_of_bounds(i+y,j+x)) result.data[i*3+j] = false;
			else result.data[i*3+j] = data[(i+y-1)*width+j+x-1];
		}
	}
	return result;
}
