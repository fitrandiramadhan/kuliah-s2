//filename	:	tester.cpp
//author	:	steaKK

#include "CImg.h"

#include <iostream>
#include <stdlib.h>

#define cimg_use_magick

using namespace std;
using namespace cimg_library;

int main() {

	CImg<unsigned char> image("number_1.jpg");

	image.display();

	// bool a = true;
	// bool b = false;
	// bool c = true;
	//
	// int x = a + b + c;
	//
	// cout << (a>b) << endl;

	return 0;

}
