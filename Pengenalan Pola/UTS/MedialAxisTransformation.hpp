//filename	:	MedialAxisTransformation.hpp
//author	:	steaKK

#ifndef MedialAxisTransformation_HPP
#define MedialAxisTransformation_HPP

using namespace std;

class MedialAxisTransformation {

	//CONSTRUCTOR
	MedialAxisTransformation(MatrixImageBinary);
	~MedialAxisTransformation();

	//METHOD
	MatrixImageBinary transform();
	List<Point> get_border();
	bool is_simple(MatrixImageBinary,Point);
	Point count_local_object(MatrixImageBinary,Point);
	bool is_curve_end(MatrixImageBinary);
	bool is_border(MatrixImageBinary);

private:
	//ATTRIBUTE
	MatrixImageBinary m;
};

#endif
