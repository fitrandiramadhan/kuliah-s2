//filename	:	Point.cpp
//author	:	steaKK

#include "Point.hpp"

Point::Point() {
	x = 0;
	y = 0;
}

Point::Point(int _x, int _y) {
	x = _x;
	y = _y;
}

Point::Point(const Point& _Point) {
	x = _Point.x;
	y = _Point.y;
}

Point& Point::operator=(const Point& _Point) {
	x = _Point.x;
	y = _Point.y;

	return *this;
}

Point::~Point() {

}

//GETTERSETTER
int Point::get_x() {
	return x;
}

void Point::set_x(int _x) {
	x = _x;
}

int Point::get_y() {
	return y;
}

void Point::set_y(int _y) {
	y = _y;
}

//TESTER
void Point::print() {
	cout << "[" << x << "," << y << "]" << endl;
}

//METHOD
bool Point::is_equal(Point p) {
	if(x==p.x) {
		if(y==p.y) return true;
		else return false;
	}
	return false;
}

Point Point::left() {
	Point result(x-1,y);
	return result;
}

Point Point::top() {
	Point result(x,y-1);
	return result;
}

Point Point::right() {
	Point result(x+1,y);
	return result;
}

Point Point::bottom() {
	Point result(x,y+1);
	return result;
}

Point Point::top_left() {
	Point result(x-1,y-1);
	return result;
}

Point Point::top_right() {
	Point result(x+1,y-1);
	return result;
}

Point Point::bottom_left() {
	Point result(x-1,y+1);
	return result;
}

Point Point::bottom_right() {
	Point result(x+1,y+1);
	return result;
}

void Point::move_top() {
	y--;
}

void Point::move_bottom() {
	y++;
}

void Point::move_left() {
	x--;
}

void Point::move_right() {
	x++;
}

void Point::move_top_left() {
	y--;
	x--;
}

void Point::move_top_right() {
	y--;
	x++;
}

void Point::move_bottom_left() {
	y++;
	x--;
}

void Point::move_bottom_right() {
	y++;
	x++;
}
