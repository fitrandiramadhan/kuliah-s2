//filename	:	Point.hpp
//author	:	steaKK

#ifndef Point_HPP
#define Point_HPP

#include <iostream>

using namespace std;

class Point {
public:
	//CONSTRUCTORS
	Point();
	Point(int,int);
	Point(const Point&);
	Point& operator=(const Point&);
	~Point();

	//GETTERSETTER
	int get_x();
	void set_x(int);
	int get_y();
	void set_y(int);

	//TESTER
	void print();

	//METHOD
	bool is_equal(Point);

	Point top();
	Point bottom();
	Point left();
	Point right();
	Point top_left();
	Point top_right();
	Point bottom_left();
	Point bottom_right();

	void move_top();
	void move_bottom();
	void move_left();
	void move_right();
	void move_top_left();
	void move_top_right();
	void move_bottom_left();
	void move_bottom_right();

private:
	//ATTRIBUTE
	int x;
	int y;
};

#endif
