//filename	:	main.cpp
//author	:	steaKK

#include <iostream>

#include "CImg.h"

#include "Point.hpp"
#include "ImageMatrix.hpp"
#include "RGBGrayscaleTransformation.hpp"
#include "SegmentationTransformation.hpp"
#include "ImageMatrixBinary.hpp"
// #include "ZhangSuenTransformation.hpp"
#include "ChainCode.hpp"
#include "ZhangSuen.hpp"
// #include "MedialAxisTransformation.hpp"

using namespace std;
using namespace cimg_library;

int main() {

	//ImageMatrix
	// CImg<unsigned char> _image("number_1.jpg");
	// ImageMatrix image(_image);
	// CImg<unsigned char> image_(image.make_image());
	// image_.display();

	//RGBGrayscaleTransformation
	// CImg<unsigned char> _image("number_1.jpg");
	// ImageMatrix image(_image);
	// RGBGrayscaleTransformation transformation(image);
	// CImg<unsigned char> image_((transformation.transform()).make_image());
	// image_.display();

	//Segmentation
	//ImageMatrixBinary
	// CImg<unsigned char> _image("number_1.jpg");
	// ImageMatrix image(_image);
	// RGBGrayscaleTransformation transformation1(image);
	// SegmentationTransformation transformation2(transformation1.transform());
	// CImg<unsigned char> image_((transformation2.transform()).make_image());
	// image_.display();

	//ZhangSuenTransformation
	// CImg<unsigned char> _image("number_1.jpg");
	// ImageMatrix image(_image);
	// RGBGrayscaleTransformation transformation1(image);
	// SegmentationTransformation transformation2(transformation1.transform());
	// ZhangSuenTransformation transformation3(transformation2.transform());
	// CImg<unsigned char> image_((transformation3.transform()).make_image());
	// image_.display();

	//ZhangSuen
	// CImg<unsigned char> _image("number_7.jpg");
	// ImageMatrix image(_image);
	// RGBGrayscaleTransformation transformation1(image);
	// SegmentationTransformation transformation2(transformation1.transform());
	// CImg<unsigned char> image_(transformation2.transform().make_image());
	// ZhangSuen transformation3(image_);
	// transformation3.transform();
	// CImg<unsigned char> image__(transformation3.make_image());
	// (_image,image_,image__).display();

	//ChainCode
	// CImg<unsigned char> _image("number_7.jpg");
	// ImageMatrix image(_image);
	// RGBGrayscaleTransformation transformation1(image);
	// SegmentationTransformation transformation2(transformation1.transform());
	// CImg<unsigned char> image_(transformation2.transform().make_image());
	// ZhangSuen transformation3(image_);
	// transformation3.transform();
	// CImg<unsigned char> image__(transformation3.make_image());
	// ChainCode ccc(image__);
	// CImg<unsigned char> image___(ccc.make_image());
	// (_image,image_,image__,image___).display();
	// ccc.print();

	return 0;
}
