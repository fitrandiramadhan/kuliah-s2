//filename	:	ZhangSuen.hpp
//author	:	steaKK

#ifndef ZhangSuen_HPP
#define ZhangSuen_HPP

#include <iostream>
#include <vector>

#include "CImg.h"

#include "Point.hpp"

#define cimg_use_magick

using namespace std;
using namespace cimg_library;


class ZhangSuen {
public:
	//CONSTRUCTORS
	ZhangSuen(int _height, int _width);
	ZhangSuen(CImg<unsigned char> _image);
	~ZhangSuen();

	//METHODS
	void transform();
	vector<Point> get_step_one();
	vector<Point> get_step_two();
	int n_white_to_black(int i, int x);
	int n_black_neighbour(int i, int x);
	bool is_black_246(int i, int j);
	bool is_black_468(int i, int j);
	bool is_black_248(int i, int j);
	bool is_black_268(int i, int j);

	void clear_edge();

	CImg<unsigned char> make_image();
	void print();

private:
	//ATTRIBUTES
	int height;
	int width;
	bool* data;
};

#endif
