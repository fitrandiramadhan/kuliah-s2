//filename	:	ChainCode.hpp
//author	:	steaKK

#ifndef ChainCode_HPP
#define ChainCode_HPP

#include <iostream>
#include <vector>

#include "CImg.h"
#define cimg_use_magick

#include "Point.hpp"

using namespace std;
using namespace cimg_library;

class ChainCode {
public:
	//CONSTRUCTORS
	ChainCode(int _height, int _width);
	ChainCode(CImg<unsigned char> _image);
	~ChainCode();

	//METHODS
	void transform();


	void fetch_code();
	Point get_anchor();
	void transform_border();

	//TESTER UTILITY
	void clear();
	void clear_edge();
	CImg<unsigned char> make_image();
	void print();

private:
	//ATTRIBUTES
	int height;
	int width;
	bool* data;

	vector<Point> anchor;
	vector<vector<int> > code;
};

#endif
