//filename	:	ImageMatrix.hpp
//author	:	steaKK

#ifndef ImageMatrix_HPP
#define ImageMatrix_HPP

#include <iostream>

#include "CImg.h"

#define cimg_use_magick

using namespace std;
using namespace cimg_library;

class ImageMatrix {

public:
	//CONSTRUCTORS
	ImageMatrix();
	ImageMatrix(int _height, int _width, int _depth, int _channel, int _spectrum);
	ImageMatrix(CImg<unsigned char> _image);
	ImageMatrix(const ImageMatrix& _ImageMatrix);
	ImageMatrix& operator=(const ImageMatrix& _ImageMatrix);
	~ImageMatrix();

	//GETTERSETTER
	int get_height();
	void set_height(int _height);
	int get_width();
	void set_width(int _width);
	int get_depth();
	void set_depth(int _depth);
	int get_channel();
	void set_channel(int _channel);
	int get_spectrum();
	void set_spectrum(int _spectrum);
	int get_data(int i, int j, int k, int c);
	void set_data(int i, int j, int k, int c, int value);

	//TESTER
	void print();
	CImg<unsigned char> make_image();

	//METHOD
	ImageMatrix inverse();
	bool is_edge_pixel(int i, int j);
	bool is_out_of_bounds(int i, int j);
	ImageMatrix make_local(int i, int j);

private:
	//CONSTANT
	static const int DEFAULT_HEIGHT = 100;
	static const int DEFAULT_WIDTH = 100;
	static const int DEFAULT_DEPTH = 1;
	static const int DEFAULT_CHANNEL = 3;
	static const int DEFAULT_SPECTRUM = 256;

	//ATTRIBUTE
	int height;		//height of image (pixels)
	int width;		//width of image (pixels)
	int depth;		//depth of image (pixels)
	int channel;	//number of channel (default 1 = GRAYSCALE, 3 = RGB)
	int spectrum;	//number of color

	//DATA
	int* data;
};

#endif
