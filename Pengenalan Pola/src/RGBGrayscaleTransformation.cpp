//filename	:	RGBGrayscaleTransformation.cpp
//author	:	steaKK

#include "RGBGrayscaleTransformation.hpp"

RGBGrayscaleTransformation::RGBGrayscaleTransformation() {

}

RGBGrayscaleTransformation::RGBGrayscaleTransformation(ImageMatrix _image) : image(_image) {

}

RGBGrayscaleTransformation::~RGBGrayscaleTransformation() {

}

ImageMatrix RGBGrayscaleTransformation::transform() {
	ImageMatrix result(image.get_height(),image.get_width(),image.get_depth(),1,256);
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			for(int k=0;k<image.get_depth();k++) {
				float r = (float)image.get_data(i,j,k,0);
				float g = (float)image.get_data(i,j,k,0);
				float b = (float)image.get_data(i,j,k,0);
				result.set_data(i,j,k,0,(int)(0.299*r + 0.587*g + 0.114*b));
			}
		}
	}
	return result;
}
