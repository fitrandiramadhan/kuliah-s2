//filename	:	ZhangSuen.hpp
//author	:	steaKK

#include "ZhangSuen.hpp"

ZhangSuen::ZhangSuen(int _height, int _width) {
	height = _height;
	width = _width;
	data = new bool[height*width];
	for(int i=0;i<height*width;i++) data[i] = false;
}

ZhangSuen::ZhangSuen(CImg<unsigned char> _image) {
	height = _image.height();
	width = _image.width();
	data = new bool[height*width];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			if(_image(j,i,0,0)<128) data[i*width+j] = false;
			else data[i*width+j] = true;
		}
	}
}

ZhangSuen::~ZhangSuen() {
	delete[] data;
}

void ZhangSuen::transform() {
	clear_edge();
	vector<Point> v1 = get_step_one();
	vector<Point> v2 = get_step_two();
	while (!v1.empty() && !v2.empty()) {
		for(int i=0;i<v1.size();i++) data[v1[i].y*width+v1[i].x] = false;
		v2 = get_step_two();
		for(int i=0;i<v2.size();i++) data[v2[i].y*width+v2[i].x] = false;
		v1.clear(); v2.clear();
		v1 = get_step_one(); v2 = get_step_two();
	}
}

vector<Point> ZhangSuen::get_step_one() {
	vector<Point> result;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(data[i*width+j]) {
				if(is_black_246(i,j)) {
					if(is_black_468(i,j)) {
						int n_neigbour = n_black_neighbour(i,j);
						if(n_neigbour>1 && n_neigbour<7) {
							if(n_white_to_black(i,j)==1) {
								Point p; p.x = j; p.y=i;
								result.push_back(p);
							}
						}
					}
				}
			}
		}
	}
	return result;
}

vector<Point> ZhangSuen::get_step_two() {
	vector<Point> result;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(data[i*width+j]) {
				if(is_black_248(i,j)) {
					if(is_black_268(i,j)) {
						int n_neigbour = n_black_neighbour(i,j);
						if(n_neigbour>1 && n_neigbour<7) {
							if(n_white_to_black(i,j)==1) {
								Point p; p.x = j; p.y=i;
								result.push_back(p);
							}
						}
					}
				}
			}
		}
	}
	return result;
}

int ZhangSuen::n_white_to_black(int i, int j) {
	int result = 0;
	if(data[(i-1)*width+(j-1)]<data[(i-1)*width+j]) result++;
	if(data[(i-1)*width+j]<data[(i-1)*width+(j+1)]) result++;
	if(data[(i-1)*width+(j+1)]<data[i*width+(j+1)]) result++;
	if(data[i*width+(j+1)]<data[(i+1)*width+(j+1)]) result++;
	if(data[(i+1)*width+(j+1)]<data[(i+1)*width+j]) result++;
	if(data[(i+1)*width+j]<data[(i+1)*width+(j-1)]) result++;
	if(data[(i+1)*width+(j-1)]<data[i*width+(j-1)]) result++;
	if(data[i*width+(j-1)]<data[(i-1)*width+(j-1)]) result++;
	return result;
}

int ZhangSuen::n_black_neighbour(int i, int j) {
	return
		data[(i-1)*width+(j-1)] + data[(i-1)*width+j] + data[(i-1)*width+(j+1)] + data[i*width+(j-1)] +
		data[i*width+(j+1)] + data[(i+1)*width+(j-1)] + data[(i+1)*width+j] + data[(i+1)*width+(j+1)];
}

bool ZhangSuen::is_black_246(int i, int j) {
	return (data[(i-1)*width+j] || data[i*width+(j+1)] || data[(i+1)*width+j]);
}

bool ZhangSuen::is_black_468(int i, int j) {
	return (data[i*width+(j+1)] || data[(i+1)*width+j] || data[i*width+(j-1)]);
}

bool ZhangSuen::is_black_248(int i, int j) {
	return (data[(i-1)*width+j] || data[i*width+(j+1)] || data[i*width+(j-1)]);
}

bool ZhangSuen::is_black_268(int i, int j) {
	return (data[(i-1)*width+j] || data[(i+1)*width+j] || data[i*width+(j-1)]);
}

void ZhangSuen::clear_edge() {
	for(int i=0;i<height;i++) {
		data[i*width+0] = false;
		data[(i+1)*width-1] = false;
	}
	for(int i=0;i<width;i++) {
		data[i] = false;
		data[((height-1)*width)+i] = false;
	}
}

CImg<unsigned char> ZhangSuen::make_image() {
	CImg<unsigned char> result(width,height,1,1);
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			result(j,i,0,0) = (int)(data[i*width+j]*255);
		}
	}
	return result;
}

void ZhangSuen::print() {
	cout << endl;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			cout << data[i*width+j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(	data[i*width+j]
				&& is_black_246(i,j)
				&& is_black_468(i,j)
				&& n_black_neighbour(i,j)>1
				&& n_black_neighbour(i,j)<7
				&& n_white_to_black(i,j)==1
				)
			{
				cout << 1 << " ";
			}
			else {
				cout << 0 << " ";
			}
		}
		cout << endl;
	}
}
