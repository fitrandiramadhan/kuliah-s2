//filename	:	ZhangSuen.hpp
//author	:	steaKK

#ifndef ZhangSuenTransformation_HPP
#define ZhangSuenTransformation_HPP

#include <vector>

#include "Point.hpp"
#include "ImageMatrixBinary.hpp"

using namespace std;

class ZhangSuenTransformation {
public:
	//CONSTRUCTOR
	ZhangSuenTransformation(ImageMatrixBinary _image);
	~ZhangSuenTransformation();

	//METHOD
	ImageMatrixBinary transform();

	vector<Point> get_step_one();
	vector<Point> get_step_two();

	int n_white_to_black(ImageMatrixBinary);
	int n_black_neighbour(ImageMatrixBinary);
	bool is_black_246(ImageMatrixBinary);
	bool is_black_468(ImageMatrixBinary);
	bool is_black_248(ImageMatrixBinary);
	bool is_black_268(ImageMatrixBinary);

private:
	//ATTRIBUTE
	ImageMatrixBinary image;
};

#endif
