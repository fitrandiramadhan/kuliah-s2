//filename	:	ZhangSuenTransformation.cpp
//author	:	steaKK

#include "ZhangSuenTransformation.hpp"

ZhangSuenTransformation::ZhangSuenTransformation(ImageMatrixBinary _image) : image(_image) {

}

ZhangSuenTransformation::~ZhangSuenTransformation() {

}

ImageMatrixBinary ZhangSuenTransformation::transform() {
	vector<Point> v1 = get_step_one();
	vector<Point> v2 = get_step_two();

	while (!v1.empty() && !v2.empty()) {
		for(int i=0;i<v1.size();i++) image.set_data(v1[i].get_y(),v1[i].get_x(),false);
		v2 = get_step_two();
		for(int i=0;i<v2.size();i++) image.set_data(v2[i].get_y(),v2[i].get_x(),false);
		v1.clear(); v2.clear();
		v1 = get_step_one(); v2 = get_step_two();
	}

	return image;
}

vector<Point> ZhangSuenTransformation::get_step_one() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(image.get_data(i,j)) {
				ImageMatrixBinary m(image.make_local(i,j));
				if(n_black_neighbour(m)>1 && n_black_neighbour(m)<7) {
					if(n_white_to_black(m)==1) {
						if(is_black_246(m) && is_black_468(m)) {
							Point p(j,i);
							result.push_back(p);
						}
					}
				}
			}
		}
	}
	return result;
}

vector<Point> ZhangSuenTransformation::get_step_two() {
	vector<Point> result;
	for(int i=0;i<image.get_height();i++) {
		for(int j=0;j<image.get_width();j++) {
			if(image.get_data(i,j)) {
				ImageMatrixBinary m(image.make_local(i,j));
				if(n_black_neighbour(m)>1 && n_black_neighbour(m)<7) {
					if(n_white_to_black(m)==1) {
						if(is_black_248(m) && is_black_268(m)) {
							Point p(j,i);
							result.push_back(p);
						}
					}
				}
			}
		}
	}
	return result;
}

int ZhangSuenTransformation::n_white_to_black(ImageMatrixBinary m) {
	int result = 0;
	if(m.get_data(0,0)<m.get_data(0,1)) result++;
	if(m.get_data(0,1)<m.get_data(0,2)) result++;
	if(m.get_data(0,2)<m.get_data(1,2)) result++;
	if(m.get_data(1,2)<m.get_data(2,2)) result++;
	if(m.get_data(2,2)<m.get_data(2,1)) result++;
	if(m.get_data(2,1)<m.get_data(2,0)) result++;
	if(m.get_data(2,0)<m.get_data(1,0)) result++;
	if(m.get_data(1,0)<m.get_data(0,0)) result++;
	return result;
}

int ZhangSuenTransformation::n_black_neighbour(ImageMatrixBinary m) {
	return
		m.get_data(0,0) + m.get_data(0,1) + m.get_data(0,2) + m.get_data(1,0) +
		m.get_data(1,2) + m.get_data(2,0) + m.get_data(2,1) + m.get_data(2,2);
}

bool ZhangSuenTransformation::is_black_246(ImageMatrixBinary m) {
	return m.get_data(0,1) || m.get_data(1,2) || m.get_data(2,1);
}

bool ZhangSuenTransformation::is_black_468(ImageMatrixBinary m) {
	return m.get_data(1,2) || m.get_data(2,1) || m.get_data(1,0);
}

bool ZhangSuenTransformation::is_black_248(ImageMatrixBinary m) {
	return m.get_data(0,1) || m.get_data(1,2) || m.get_data(1,0);
}

bool ZhangSuenTransformation::is_black_268(ImageMatrixBinary m) {
	return m.get_data(0,1) || m.get_data(2,1) || m.get_data(1,0);
}
