//filename	:	ChainCode.hpp
//author	:	steaKK

#include "ChainCode.hpp"

ChainCode::ChainCode(int _height, int _width) {
	height = _height;
	width = _width;
	data = new bool[height*width];
	for(int i=0;i<height*width;i++) data[i] = false;
}

ChainCode::ChainCode(CImg<unsigned char> _image) {
	height = _image.height();
	width = _image.width();
	data = new bool[height*width];
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			if(_image(j,i,0,0)<128) data[i*width+j] = false;
			else data[i*width+j] = true;
		}
	}
}

ChainCode::~ChainCode() {
	delete[] data;
}

void ChainCode::fetch_code() {
	transform_border(get_border());
	vector<Point> v = get_anchor();
	while(!v.empty()) {
		code = trace_border(v[0]);
		v = get_anchor();
	}
}

vector<int> ChainCode::trace_border(Point start) {
	vector<int> result;
	Point p(start);
	do {
		data[p.y*width+p.x] = false;
		if(data[(p.y)*width+(p.x+1)]) {
			p.y; p.x++;
			result.push_back(0);
		}
		else if(data[(p.y-1)*width+(p.x+1)]) {
			p.y--; p.x++;
			result.push_back(1);
		}
		else if(data[(p.y-1)*width+(p.x)]) {
			p.y--; p.x;
			result.push_back(2);
		}
		else if(data[(p.y-1)*width+(p.x-1)]) {
			p.y--; p.x--;
			result.push_back(3);
		}
		else if(data[(p.y)*width+(p.x-1)]) {
			p.y; p.x--;
			result.push_back(4);
		}
		else if(data[(p.y+1)*width+(p.x-1)]) {
			p.y++; p.x--;
			result.push_back(5);
		}
		else if(data[(p.y+1)*width+(p.x)]) {
			p.y++; p.x;
			result.push_back(6);
		}
		// else if(data[(p.y+1)*width+(p.x+1)]) {
		// 	p.y++; p.x++;
		// 	result.push_back();
		// }
		else {
			p.y++; p.x++;
			result.push_back(7);
		}
	} (while p!=start);
	return result;
}

vector<Point> ChainCode::get_anchor() {
	vector<Point> result;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(data[i*width+j]) {
				Point p(j,i);
				result.push_back(p);
				return result;
			}
		}
	}
	return result;
}

void ChainCode::transform_border(vector<Point> v) {
	clear();
	for(int i=0;i<v.size();i++) data[(v[i].y)*width+(v[i].x)];
}

vector<Point> ChainCode::get_border() {
	vector<Point> result;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			if(	data[(i-1)*width+(j-1)] || data[(i-1)*width+(j)] ||
				data[(i-1)*width+(j+1)] || data[(i)*width+(j-1)] ||
				data[(i)*width+(j+1)] || data[(i+1)*width+(j-1)] ||
				data[(i+1)*width+(j)] || data[(i+1)*width+(j+1)] ) {
					Point p(j,i);
					result.push_back(p);
				}
		}
	}
	return result;
}

void ChainCode::clear_edge() {
	for(int i=0;i<height;i++) {
		data[i*width+0] = false;
		data[(i+1)*width-1] = false;
	}
	for(int i=0;i<width;i++) {
		data[i] = false;
		data[((height-1)*width)+i] = false;
	}
}

CImg<unsigned char> ChainCode::make_image() {
	CImg<unsigned char> result(width,height,1,1);
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			result(j,i,0,0) = (int)(data[i*width+j]*255);
		}
	}
	return result;
}

void ChainCode::clear() {
	for(int i=0;i<height*width;i++) data[i*width+j] = false;
}

void ChainCode::print() {
	cout << endl;
	for(int i=1;i<height-1;i++) {
		for(int j=1;j<width-1;j++) {
			cout << data[i*width+j] << " ";
		}
		cout << endl;
	}
}
