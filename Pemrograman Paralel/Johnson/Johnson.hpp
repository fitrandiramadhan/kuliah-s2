//file		:	Johnson.hpp
//author	:	steaKK

#ifndef Johnson_HPP
#define Johnson_HPP

#include <iostream>
#include <vector>
#include <omp.h>

#include "Djikstra.hpp"
#include "Matrix.hpp"

using namespace std;

class Johnson {
public:
	static vector<Matrix> solve_all(Matrix);
	static Matrix solve_single_source(Matrix,int);
	static Matrix transform_graph(Matrix);
	static vector<int> solve_s_node(Matrix);
	static Matrix create_s_node(Matrix);
	static bool is_all_positive(Matrix);

	static Matrix init_data();

private:
	static const int DEFAULT_SIZE = 16;
	static const int INFINITY = 999999;
	static const int UNDEFINED = -1;
};

#endif
