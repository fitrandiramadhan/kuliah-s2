//file		:	Johnson.cpp
//author	:	steaKK

#include "Johnson.hpp"

Matrix Johnson::create_s_node(Matrix data) {
	Matrix result(data.get_height()+1,data.get_width()+1);
	for(int i=0;i<result.get_height();i++) {
		for(int j=0;j<result.get_width();j++) {
			if(i==0) result.set_data(i,j,0);
			else if(j==0) result.set_data(i,j,999999);
			else result.set_data(i,j,data.get_data(i-1,j-1));
		}
	}
	return result;
}

vector<int> Johnson::solve_s_node(Matrix s_data) {
	Matrix temp(Djikstra::solve_single_source(s_data,0));
	vector<int> result;
	for(int i=0;i<s_data.get_height()-1;i++) result.push_back(999999);
	for(int i=0;i<result.size();i++) {
		int s_val = 999999;
		for(int j=0;j<result.size();j++) {
			s_val = Djikstra::min(s_val,temp.get_data(i+1,j+1));
		}
		result.push_back(s_val);
	}
	cout << "solving_s_node" << endl;
	return result;
}

Matrix Johnson::transform_graph(Matrix data) {
	Matrix result(data.get_height(),data.get_width());
	vector<int> temp = solve_s_node(create_s_node(data));
	for(int i=0;i<data.get_height();i++) {
		for(int j=0;j<data.get_width();j++) {
			result.set_data(i,j,data.get_data(i,j)+temp[i]-temp[j]);
		}
	}
	return result;
}

Matrix Johnson::solve_single_source(Matrix data, int k) {
	return Djikstra::solve_single_source(transform_graph(data),k);
}

vector<Matrix> Johnson::solve_all(Matrix data) {
	if(is_all_positive(data)) return Djikstra::solve_all(data);
	else return Djikstra::solve_all(transform_graph(data));
}

bool Johnson::is_all_positive(Matrix data) {
	for(int i=0;i<data.get_height();i++) {
		for(int j=0;j<data.get_width();j++) {
			if(data.get_data(i,j)<0) return false;
		}
	}
	return true;
}

Matrix Johnson::init_data() {
	Matrix result(7,7);

	result.set_data(0,0,0);
	result.set_data(0,1,999999);
	result.set_data(0,2,1);
	result.set_data(0,3,2);
	result.set_data(0,4,999999);
	result.set_data(0,5,999999);
	result.set_data(0,6,999999);

	result.set_data(1,0,999999);
	result.set_data(1,1,0);
	result.set_data(1,2,-1);
	result.set_data(1,3,999999);
	result.set_data(1,4,999999);
	result.set_data(1,5,3);
	result.set_data(1,6,999999);

	result.set_data(2,0,999999);
	result.set_data(2,1,2);
	result.set_data(2,2,0);
	result.set_data(2,3,1);
	result.set_data(2,4,3);
	result.set_data(2,5,999999);
	result.set_data(2,6,999999);

	result.set_data(3,0,2);
	result.set_data(3,1,999999);
	result.set_data(3,2,1);
	result.set_data(3,3,0);
	result.set_data(3,4,999999);
	result.set_data(3,5,999999);
	result.set_data(3,6,1);

	result.set_data(4,0,999999);
	result.set_data(4,1,999999);
	result.set_data(4,2,3);
	result.set_data(4,3,999999);
	result.set_data(4,4,0);
	result.set_data(4,5,2);
	result.set_data(4,6,999999);

	result.set_data(5,0,999999);
	result.set_data(5,1,3);
	result.set_data(5,2,999999);
	result.set_data(5,3,999999);
	result.set_data(5,4,999999);
	result.set_data(5,5,0);
	result.set_data(5,6,1);

	result.set_data(6,0,999999);
	result.set_data(6,1,999999);
	result.set_data(6,2,999999);
	result.set_data(6,3,1);
	result.set_data(6,4,999999);
	result.set_data(6,5,1);
	result.set_data(6,6,0);

	return result;
}
