//file		:	main.cpp
//author	:	steaKK

#include "Johnson.hpp"
#include <ctime>

int main() {
	Matrix m0(Johnson::init_data());
	cout << "printing " << endl;
	vector<Matrix> tab = Johnson::solve_all(m0);
	cout << "printing 2" << endl;
	for(int i=0;i<tab.size();i++) {
		cout << "printing " << i << endl;
		tab[i].print();
		cout << endl;
	}

	return 0;
}
