//file		:	Djikstra.hpp
//author	:	steaKK

#ifndef Djikstra_HPP
#define Djikstra_HPP

#include <iostream>
#include <vector>
#include <omp.h>

#include "Matrix.hpp"

using namespace std;

class Djikstra {
public:
	static int get_next_cluster(vector<bool>);
	static Matrix solve_single_source(Matrix,int);
	static vector<Matrix> solve_all(Matrix);

	static int min(int,int);

	static Matrix init_data();

private:
	static const int DEFAULT_SIZE = 16;
	static const int INFINITY = 99999;
	static const int UNDEFINED = -1;
};

#endif
