//file		: Hello.cpp
//author	: steaKK

#include <iostream>
#include <string>
#include <cstdlib>
#include <mpi.h>

using namespace std;

const int MAX_STRING = 1000;

int main() {
	char greeting[MAX_STRING];
	int comm_sz;
	int my_rank;

	MPI_Init(NULL,NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	if(my_rank!=0) {
		sprintf(greeting,"Greetings from proccess %d of %d!", my_rank, comm_sz);
		MPI_Send(&greeting, strlen(greeting)+1,MPI_CHAR,0,0,MPI_COMM_WORLD);
	}
	else {
		cout << greeting << endl;
		for(int i=1;i<comm_sz;i++) {
			MPI_Recv(greeting,MAX_STRING,MPI_CHAR,i,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			cout << greeting << endl;
		}
	}

	MPI_Finalize();

	return 0;
}
