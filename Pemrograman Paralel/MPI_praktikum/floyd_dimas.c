/*********************************************************************
* nama: Dimas Gilang
* nim:  23515027
* keterangan:
*     cara compile: mpicc floyd.c -o floyd -lm
*     cara run:     mpirun -np {banyaknya prosesor} -hostfile {path ke mpi hostfile} floyd
* struktur data:
*     blocks: adalah matrix utuh, berbentuk flat menjadi array berukuran n*n
*     block:  adalah matrix lokal yang dimiliki tiap processor,
*             bebentuk flat menjadi array dengan ukuran n/sqrt(p) * n/sqrt(p)
********************************************************************/

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef enum { false, true } bool;

const int Z = 999999; // infinity
const int TAG_SEND_ROW = 0; // tag untuk broadcast baris k
const int TAG_SEND_COL = 1; // tag untuk broadcase kolom k
const bool IS_SHOW_MATRIX = true; // set untuk menampilkan atau tidak

int min(int a, int b) {
    return a < b ? a : b;
}

// men-generate matrix input
void input_matrix(int rank, int p, int n, int width, int slice, int block[]);

// cek apakah prosesor rank memiliki baris k
int have_row_k(int rank, int k, int width, int slice);

// cek apakah prosesor rank memiliki kolom k
int have_col_k(int rank, int k, int width, int slice);

// algoritma floyd
void floyd(int rank, int p, int n, int width, int slice, int block[]);

// menampilkan matrix ke layar
void show_matrix(int rank, int p, int n, int width, int slice, int block[]);

// menampilkan vector jarak terdekat dari s ke vertex lain
void show_s_shortest(int rank, int p, int n, int width, int slice, int block[], int s);

// cek apakah output dari algoritma benar
bool check_output(int rank, int p, int n, int width, int slice, int block[]);

int main(int argc, char *argv[] ) {
    int p, rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // periksa nilai p apakah valid atau tidak
    if (ceilf(sqrt(p)) != sqrt(p)) {
        MPI_Finalize();
        if (rank == 0) {
            printf("Invalid input: p tidak bisa diakarkan\n");
        }
        return 0;
    }

    // terima nilai n dari user
    int n;
    if (rank == 0) {
        printf("Masukkan jumlah vertex\n");
        scanf("%d", &n);
    }
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD); // broadcast n ke prosesor lain
    int slice = sqrt(p); // hitung jumlah potongan dalam satu sisi
    int width = n/slice; // hitung jumlah item pada satu potongan

    // periksa nilai n apakah valid atau tidak
    if (n%slice != 0) {
        MPI_Finalize();
        if (rank == 0) {
            printf("Invalid input: n tidak bisa dibagi dengan akar p\n");
        }
        return 0;
    }

    // terima s dari user
    int s;
    if (rank == 0) {
        printf("Masukkan sorce vertex (0 <= s < %d)\n", n);
        scanf("%d", &s);
    }
    MPI_Bcast(&s, 1, MPI_INT, 0, MPI_COMM_WORLD); // broadcast s ke prosesor lain

    // membuat matrix input
    int *block = malloc(width*width*sizeof(int));
    input_matrix(rank, p, n, width, slice, block);

    // tampilkan matrix input
    if (IS_SHOW_MATRIX == true) {
        if (rank == 0) {
            printf("\nMatrix graf input:\n");
        }
        show_matrix(rank, p, n, width, slice, block);
    }

    // siap, laksanakan floyd
    floyd(rank, p, n, width, slice, block);

    // tampilkan matrix hasil
    if (IS_SHOW_MATRIX == true) {
        if (rank == 0) {
            printf("\nMatrix jarak terdekat:\n");
        }
        show_matrix(rank, p, n, width, slice, block);
    }

    // tampilkan vector jarak terdekat dari s ke vertex lain
    if (IS_SHOW_MATRIX == true) {
        if (rank == 0) {
            printf("\nJarak terdekat dari s=%d ke vertex lain:\n", s);
        }
        show_s_shortest(rank, p, n, width, slice, block, s);
    }

    // periksa testing
    if (n == 9) {
        bool is_correct = check_output(rank, p, n, width, slice, block);
        if (rank == 0) {
            printf("\nTest apakah algoritma benar:\n%s\n", is_correct == true ? "Benar" : "Tidak benar");
        }
    }

    // bersih-bersih
    free(block);
    MPI_Finalize();

    return 0;
}

void input_matrix(int rank, int p, int n, int width, int slice, int block[]) {
    int idx_p, i, j, val, random;
    int slide[81] = { // matrix dari slide, untuk test kebenaran algoritma
        0, 2, 3, Z, Z, Z, Z, Z, Z,
        Z, 0, Z, Z, Z, 1, Z, Z, Z,
        Z, Z, 0, 1, 2, Z, Z, Z, Z,
        Z, Z, Z, 0, Z, Z, 2, Z, Z,
        Z, Z, Z, Z, 0, Z, Z, Z, Z,
        Z, Z, Z, Z, Z, 0, 2, 3, 2,
        Z, Z, Z, Z, 1, Z, 0, 1, Z,
        Z, Z, Z, Z, Z, Z, Z, 0, Z,
        Z, Z, Z, Z, Z, Z, Z, 1, 0
    };

    // generate matrix
    int *matrix = malloc(n*n*sizeof(int));
    if (n == 9) { // berarti pakai matrix dari slide
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                matrix[(i*n)+j] = slide[(i*n)+j];
            }
        }
    } else { // berarti matrix random
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (i == j) {
                    val = 0;
                } else {
                    random = (rand()%12)+1;
                    if (random <= 3) {
                        val = random;
                    } else {
                        val = Z;
                    }
                }
                matrix[(i*n)+j] = val;
            }
        }
    }

    // konversi matrix (dalam bentuk array) tersebut ke bentuk yang bisa di-scatter
    // caranya adalah dengan membuat semua item untuk prosesor pertama berada di depan,
    //     item untuk prosesor kedua berada di setelahnya, dan seterusnya.
    // intinya adalah flatting matrix tidak berurutan berdasarkan baris,
    //     melainkan berdasarkan bagiannya prosesor
    int *blocks = NULL;
    if (rank == 0) {
        blocks = malloc(n*n*sizeof(int));
        int idx_item = 0;
        for (idx_p = 0; idx_p < p; idx_p++) {
            int row_start = (idx_p/slice)*width;
            for (i = row_start; i < row_start+width; i++) {
                int col_start = (idx_p%slice)*width;
                for (j = col_start; j < col_start+width; j++) {
                    blocks[idx_item] = matrix[(i*n)+j];
                    idx_item++;
                }
            }
        }
    }

    // bagi dan broadcast masing-masing blok ke setiap prosesor
    MPI_Scatter(blocks, width*width, MPI_INT, block, width*width, MPI_INT, 0, MPI_COMM_WORLD);

    // bersih-bersih
    free(matrix);
    free(blocks);
}

int have_row_k(int rank, int k, int width, int slice) {
    int pos = k/width;
    if (rank/slice == pos) {
        // jika ya, mengembalikan index item pada blok lokal prosesor tersebut
        return (k%width)*width;
    } else {
        // jika tidak, kembalikan -1
        return -1;
    }
}

int have_col_k(int rank, int k, int width, int slice) {
    int pos = k/width;
    if (rank%slice == pos) {
        // jika ya, mengembalikan index item pada blok lokal prosesor tersebut
        return (k%width);
    } else {
        // jika tidak, kembalikan -1
        return -1;
    }
}

void floyd(int rank, int p, int n, int width, int slice, int block[]) {
    int k, idx_item, dest, i, j;
    long long count_compute = 0, count_comm = 0; // jumlah dan waktu pemrosesan

    // timer start
    struct timeval time_start;
    gettimeofday(&time_start, NULL);

    for (k = 0; k < n; k++) { // for each k
        int *send_row = NULL, *send_col = NULL; // baris dan kolom yang akan dikirim
        int start_row = have_row_k(rank, k, width, slice);
        int start_col = have_col_k(rank, k, width, slice);

        // jika prosesor memiliki baris k,
        // broadcast baris tersebut ke semua processor di atas dan bawahnya
        if (start_row > -1) {
            send_row = malloc(width*sizeof(int));
            for (idx_item = 0; idx_item < width; idx_item++) {
                send_row[idx_item] = block[start_row+idx_item];
                count_comm++;
            }
            for (dest = rank%slice; dest < p; dest += slice) {
                MPI_Send(send_row, width, MPI_INT, dest, TAG_SEND_ROW, MPI_COMM_WORLD);
                count_comm++;
            }
        }

        // jika prosesor memiliki kolom k,
        // broadcast kolom tersebut ke semua processor di kiri dan kanannya
        if (start_col > -1) {
            send_col = malloc(width*sizeof(int));
            for (idx_item = 0; idx_item < width; idx_item++) {
                send_col[idx_item] = block[start_col+(idx_item*width)];
                count_comm++;
            }
            for (dest = (rank/slice)*slice; dest < ((rank/slice)*slice)+slice; dest++) {
                MPI_Send(send_col, width, MPI_INT, dest, TAG_SEND_COL, MPI_COMM_WORLD);
                count_comm++;
            }
        }

        // terima baris k dan kolom k dari prosesor yang memiliki
        int source_row = (rank%slice)+((k/width)*slice);
        int source_col = ((rank/slice)*slice)+(k/width);
        int *recv_row = malloc(width*sizeof(int)); // baris yang akan diterima
        int *recv_col = malloc(width*sizeof(int)); // kolom yang akan diterima
        MPI_Recv(recv_row, width, MPI_INT, source_row, TAG_SEND_ROW, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        count_comm++;
        MPI_Recv(recv_col, width, MPI_INT, source_col, TAG_SEND_COL, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        count_comm++;

        // hitung jarak terdekat
        for (i = 0; i < width; i++) {
            for (j = 0; j < width; j++) {
                block[(i*width)+j] = min(block[(i*width)+j], recv_row[j]+recv_col[i]);
                count_compute++;
            }
        }

        // bersih-bersih
        free(send_row);
        free(send_col);
        free(recv_row);
        free(recv_col);
    }

    // timer end
    struct timeval time_end;
    gettimeofday(&time_end, NULL);
    long long time_ms = (time_end.tv_sec*1000)+(time_end.tv_usec/1000.0)-
                        (time_start.tv_sec*1000)+(time_start.tv_usec/1000.0);
    long long sum_ms;

    // broadcast and show jumlah proses
    MPI_Bcast(&count_compute, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&count_comm, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Reduce(&time_ms, &sum_ms, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        printf("\nJumlah proses masing-masing prosesor\n");
        printf("Compute: %lli\nComm:    %lli\nTotal:   %lli\n",
               count_compute, count_comm, count_compute+count_comm);
        printf("\nWaktu rata-rata tiap prosesor:\n%lli ms\n", sum_ms/p);
    }
}

void show_matrix(int rank, int p, int n, int width, int slice, int block[]) {
    int *blocks = rank == 0 ? malloc(n*n*sizeof(int)) : NULL;
    int i, j, idx_p, start_p, idx_item, val;
    MPI_Gather(block, width*width, MPI_INT, blocks, width*width, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                idx_p = (j/width)+((i/width)*slice);
                start_p = idx_p*width*width;
                idx_item = ((i%width)*width)+(j%width);
                val = blocks[start_p+idx_item];
                if (val == Z) {
                    printf("- ");
                } else {
                    printf("%d ", val);
                }
            }
            printf("\n");
        }
    }
    free(blocks);
}

void show_s_shortest(int rank, int p, int n, int width, int slice, int block[], int s) {
    int *blocks = rank == 0 ? malloc(n*n*sizeof(int)) : NULL;
    int i, j, idx_p, start_p, idx_item, val;
    MPI_Gather(block, width*width, MPI_INT, blocks, width*width, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        for (j = 0; j < n; j++) {
            idx_p = (j/width)+((s/width)*slice);
            start_p = idx_p*width*width;
            idx_item = ((s%width)*width)+(j%width);
            val = blocks[start_p+idx_item];
            if (val == Z) {
                printf("- ");
            } else {
                printf("%d ", val);
            }
        }
        printf("\n");
    }
    free(blocks);
}

bool check_output(int rank, int p, int n, int width, int slice, int block[]) {
    int seharusnya[81] = { // matrix hasil seharusnya, dari slide
        0, 2, 3, 4, 5, 3, 5, 6, 5,
        Z, 0, Z, Z, 4, 1, 3, 4, 3,
        Z, Z, 0, 1, 2, Z, 3, 4, Z,
        Z, Z, Z, 0, 3, Z, 2, 3, Z,
        Z, Z, Z, Z, 0, Z, Z, Z, Z,
        Z, Z, Z, Z, 3, 0, 2, 3, 2,
        Z, Z, Z, Z, 1, Z, 0, 1, Z,
        Z, Z, Z, Z, Z, Z, Z, 0, Z,
        Z, Z, Z, Z, Z, Z, Z, 1, 0
    };

    bool is_correct = true;
    int *blocks = rank == 0 ? malloc(n*n*sizeof(int)) : NULL;
    int i, j, idx_p, start_p, idx_item, val;
    MPI_Gather(block, width*width, MPI_INT, blocks, width*width, MPI_INT, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                idx_p = (j/width)+((i/width)*slice);
                start_p = idx_p*width*width;
                idx_item = ((i%width)*width)+(j%width);
                val = blocks[start_p+idx_item];
                if (val != seharusnya[(i*n)+j]) {
                    is_correct = false;
                }
            }
        }
    }
    free(blocks);
    return is_correct;
}
