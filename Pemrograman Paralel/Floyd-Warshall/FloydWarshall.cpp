//file		:	FloydWarshall.cpp
//author	:	steaKK

#include "FloydWarshall.hpp"

FloydWarshall::FloydWarshall() : data(4,4) {

}

FloydWarshall::FloydWarshall(int height, int width) : data(height,width) {

}

FloydWarshall::FloydWarshall(const FloydWarshall& _FloydWarshall) : data(_FloydWarshall.data) {

}

FloydWarshall& FloydWarshall::operator=(const FloydWarshall& _FloydWarshall) {
	data = _FloydWarshall.data;
	return *this;
}

FloydWarshall::~FloydWarshall() {

}

Matrix FloydWarshall::get_data() {
	return data;
}

void FloydWarshall::set_data(Matrix _Matrix) {
	data = _Matrix;
}

void FloydWarshall::print() {
	cout << data.get_height() << endl;
	data.print();
}

Matrix FloydWarshall::solve_sequential() {
	Matrix result(data);

	for(int i=0;i<result.get_height();i++) {
		for(int j=0;j<result.get_width();j++) {
			result.set_data(i,j,999);
		}
	}

	for(int i=0;i<result.get_height();i++) {
		for(int j=0;j<result.get_width();j++) {
			if(i==j) result.set_data(i,j,0);
			else result.set_data(i,j,data.get_data(i,j));
		}
	}

	for(int k=0;k<result.get_height();k++) {
		for(int i=0;i<result.get_height();i++) {
			for(int j=0;j<result.get_width();j++) {
				int temp = result.get_data(i,k) + result.get_data(k,j);
				if(result.get_data(i,j)>temp) result.set_data(i,j,temp);
			}
		}
	}

	return result;
}

Matrix FloydWarshall::solve_openmp() {
	Matrix result(data);

	omp_set_num_threads(omp_get_num_procs());
	#pragma omp parallel for
	for(int i=0;i<result.get_height();i++) {
		#pragma omp parallel for
		for(int j=0;j<result.get_width();j++) {
			result.set_data(i,j,9999);
		}
	}

	#pragma omp parallel for
	for(int i=0;i<result.get_height();i++) {
		#pragma omp parallel for
		for(int j=0;j<result.get_width();j++) {
			if(i==j) result.set_data(i,j,0);
			else result.set_data(i,j,data.get_data(i,j));
		}
	}

	#pragma omp parallel for
	for(int k=0;k<result.get_height();k++) {
		#pragma omp parallel for
		for(int i=0;i<result.get_height();i++) {
			#pragma omp parallel for
			for(int j=0;j<result.get_width();j++) {
				int temp = result.get_data(i,k) + result.get_data(k,j);
				if(result.get_data(i,j)>temp) result.set_data(i,j,temp);
			}
		}
	}

	return result;
}

Matrix Floyd-Warshall::path_sequential() {
	for()
}





let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
let next be a |V| × |V| array of vertex indices initialized to null

procedure FloydWarshallWithPathReconstruction ()
   for each edge (u,v)
      dist[u][v] ← w(u,v)  // the weight of the edge (u,v)
      next[u][v] ← v
   for k from 1 to |V| // standard Floyd-Warshall implementation
      for i from 1 to |V|
         for j from 1 to |V|
            if dist[i][k] + dist[k][j] < dist[i][j] then
               dist[i][j] ← dist[i][k] + dist[k][j]
               next[i][j] ← next[i][k]

procedure Path(u, v)
   if next[u][v] = null then
       return []
   path = [u]
   while u ≠ v
       u ← next[u][v]
       path.append(u)
   return path
