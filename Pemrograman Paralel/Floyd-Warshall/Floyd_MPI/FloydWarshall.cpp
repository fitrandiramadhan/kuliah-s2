//file		:	FloydWarshall.cpp
//author	:	steaKK

#include "FloydWarshall.hpp"

void FloydWarshall::init(int size, int* data) {
	int* dist = new int[width*width];
	for(int i=0;i<width*width;i++) dist[i] = data[i];
	MPI_Scatter(block,width*width,MPI_INT,block,width,width,MPI_INT,0,MPI_COMM_WORLD);
	delete[] dist;
}

void FloydWarshall::run(int block_size, int* dist) {
	int* chunk = new int[width];

	for(int k=0;k<width;k++) {
		if(is_owner()) {
			for(int i=0;i<size;j++) chunk[((rank*width)+i)%size];
		}
	}
	//broadcast chunk
	MPI_Bcast(chunk,block_size,MPI_INT,root,MPI_COMM_WORLD);
	for(int i=0;i<chunk_size/size;i++) {
		for(int j=0;j<size;j++) {
			dist[get_index(size,i,j)] = min(dist[get_index(size,i,j)],dist[get_index(size,i,k)] + chunk[j]);
		}
	}
	delete[] chunk;
}

int FloydWarshall::get_index(int size, int i, int j) {
	return i*size+j;
}

int FloydWarshall::min(int a, int b) {
	if(a<b) return a;
	else return b;
}

int FloydWarshall::get_block_owner(int rank, int n_proc, int size) {
	return rank*n_proc/size;
}

int FloydWarshall::get_block_size(int n_proc, int size) {
	return size*size/n_proc;
}
