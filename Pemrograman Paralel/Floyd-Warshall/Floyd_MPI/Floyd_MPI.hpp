//file		:	Floyd_MPI.c
//author	:	steaKK

#ifndef Floyd_MPI_HPP
#define Floyd_MPI_HPP

#include <iostream>
#include <mpi.h>

class Floyd_MPI {
public:
	Floyd_MPI();
	Floyd_MPI(Matrix);
	Floyd_MPI(const Floyd_MPI&);
	Floyd_MPI& operator=(const Floyd_MPI&);
	~Floyd_MPI();

	static min(int,int);

	void input_matrix(int,int,int,int,int);
	bool have_row_k(int,int,int,int);
	bool have_col_k(int,int,int,int);

	void floyd(int,int,int,int,int,int,int);

	void print(int,int,int,int,int,int); //
	void show_path(int,int,int.int.int); //show_s_shortest
	bool check_output(int,int,int,int,int,int,int)

private:
	static const int INFINITY = 999999;
	static const int TAG_SEND_ROW = 0;
	static const int TAG_SEND_COL = 1;

	
};

#endif
