//file		:	FloydWarshall.hpp
//author	:	steaKK

#ifndef FloydWarshall_HPP
#define FloydWarshall_HPP

#include <iostream>

using namespace std;

class FloydWarshall {
public:
	static int[] run();

	static void print();

	static const int infinity = 999999;
};

#endif
