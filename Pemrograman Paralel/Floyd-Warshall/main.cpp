//file		:	main.cpp
//author	:	steaKK

#include "FloydWarshall.hpp"

int main() {
	FloydWarshall f0;

	Matrix m1(9,9);
	for(int i=0;i<m1.get_height();i++) {
		for(int j=0;j<m1.get_width();j++) {
			m1.set_data(i,j,99999);
		}
	}
	for(int i=0;i<9;i++) m1.set_data(i,i,0);
	m1.set_data(0,1,15);
	m1.set_data(0,2,13);
	m1.set_data(0,3,5);
	m1.set_data(1,7,11);
	m1.set_data(2,1,2);
	m1.set_data(2,5,6);
	m1.set_data(3,4,4);
	m1.set_data(3,8,99);
	m1.set_data(4,2,3);
	m1.set_data(4,5,1);
	m1.set_data(4,6,9);
	m1.set_data(4,8,14);
	m1.set_data(5,1,8);
	m1.set_data(5,7,17);
	m1.set_data(6,5,16);
	m1.set_data(6,7,7);
	m1.set_data(6,8,10);
	m1.set_data(8,6,12);

	f0.set_data(m1);

	cout << endl;
	f0.solve_sequential().print();
	cout << endl;
	f0.solve_openmp().print();
	cout << endl;
http://www.cs.princeton.edu/courses/archive/spr09/cos226/shortest-path.png
	return 0;
}
