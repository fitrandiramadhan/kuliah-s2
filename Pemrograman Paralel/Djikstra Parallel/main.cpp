//file		:	main.cpp
//author	:	steaKK

#include "Djikstra.hpp"
#include <ctime>

int main() {
	Matrix m0(Djikstra::init_data());

	vector<Matrix> tab = Djikstra::solve_all(m0);
	
	for(int i=0;i<tab.size();i++) {
		tab[i].print();
		cout << endl;
	}

	return 0;
}
