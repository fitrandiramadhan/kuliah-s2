//file		:	FloydWarshall.cpp
//author	:	steaKK

#include "Djikstra.hpp"

int Djikstra::min(int a, int b) {
	if(a<b) return a;
	else return b;
}

Matrix Djikstra::solve_single_source(Matrix data, int k) {
	Matrix result(data.get_height(),data.get_width());
	vector<bool> cluster;
	for(int i=0;i<data.get_height();i++) cluster.push_back(false);
	int i=get_next_cluster(cluster);
	// cout << "solve_single_source start " << k << endl;
	while(i!=UNDEFINED) {
		// cout << "solve_single_source loop i = " << i << endl;
		// cout << "cluster = ";
		for(int a=0;a<cluster.size();a++) {
			cout << cluster[a] << " ";
		}
		cout << endl;
		omp_set_num_threads(omp_get_num_procs());
		#pragma omp parallel for
		for(int j=0;j<data.get_height();j++) {
			if(!cluster[j]) result.set_data(i,j,data.get_data(i,j)+result.get_data(k,i));
		}
		cluster[i] = true;
		i=get_next_cluster(cluster);
	}
	return result;
}

vector<Matrix> Djikstra::solve_all(Matrix data) {
	vector<Matrix> result;
	omp_set_num_threads(omp_get_num_procs());
	#pragma omp parallel for
	for(int k=0;k<data.get_height();k++) {
		cout << "solve_all = " << k << endl;
		result.push_back(solve_single_source(data,k));
	}
	return result;
}

int Djikstra::get_next_cluster(vector<bool> v) {
	for(int i=0;i<v.size();i++) {
		if(!v[i]) return i;
	}
	return -1;
}

Matrix Djikstra::init_data() {
	Matrix result(7,7);

	result.set_data(0,0,0);
	result.set_data(0,1,INFINITY);
	result.set_data(0,2,1);
	result.set_data(0,3,2);
	result.set_data(0,4,INFINITY);
	result.set_data(0,5,INFINITY);
	result.set_data(0,6,INFINITY);

	result.set_data(1,0,INFINITY);
	result.set_data(1,1,0);
	result.set_data(1,2,2);
	result.set_data(1,3,INFINITY);
	result.set_data(1,4,INFINITY);
	result.set_data(1,5,3);
	result.set_data(1,6,INFINITY);

	result.set_data(2,0,INFINITY);
	result.set_data(2,1,2);
	result.set_data(2,2,0);
	result.set_data(2,3,1);
	result.set_data(2,4,3);
	result.set_data(2,5,INFINITY);
	result.set_data(2,6,INFINITY);

	result.set_data(3,0,2);
	result.set_data(3,1,INFINITY);
	result.set_data(3,2,1);
	result.set_data(3,3,0);
	result.set_data(3,4,INFINITY);
	result.set_data(3,5,INFINITY);
	result.set_data(3,6,1);

	result.set_data(4,0,INFINITY);
	result.set_data(4,1,INFINITY);
	result.set_data(4,2,3);
	result.set_data(4,3,INFINITY);
	result.set_data(4,4,0);
	result.set_data(4,5,2);
	result.set_data(4,6,INFINITY);

	result.set_data(5,0,INFINITY);
	result.set_data(5,1,3);
	result.set_data(5,2,INFINITY);
	result.set_data(5,3,INFINITY);
	result.set_data(5,4,INFINITY);
	result.set_data(5,5,0);
	result.set_data(5,6,1);

	result.set_data(6,0,INFINITY);
	result.set_data(6,1,INFINITY);
	result.set_data(6,2,INFINITY);
	result.set_data(6,3,1);
	result.set_data(6,4,INFINITY);
	result.set_data(6,5,1);
	result.set_data(6,6,0);

	return result;
}


// void Djikstra::solve() {
// 	vector<Matrix> result;
//
// 	for(int k=0;k<size;k++) {
// 		vector<bool> cluster;
// 		for(int i=0;i<size;i++) {
// 			if(i==k) cluster.push_back(true);
// 			else cluster.push_back(false);
// 			tab[k].set_data(i,k,0); //set matrix tab ke K elemen node tersebut 0 jarak
// 		}
// 		int i=get_next_cluster();
// 		while(i!=UNDEFINED) {
// 			for(int j=0;j<size;j++) {
// 				if(!cluster[j]) tab[k].set_data(i,j,data.get_data(i,j)+tab[k].get_data(k,i));
// 			}
// 			i=get_next_cluster();
// 		}
// 	}
// }


// void Djikstra::solve() {
// 	vector<Matrix> result;
// 	for(int i=0;i<size;i++) {
// 		Matrix temp(size,size);
// 		for(int x=0;x<size;x++) {
// 			for(int y=0;y<size;y++) if(x==y) temp.set_data(x,y,UNDEFINED);
// 		}
// 		tab.push_back(temp);
// 	}
//
// 	for(int k=0;k<size;k++) {
// 		vector<bool> cluster;
// 		for(int i=0;i<size;i++) {
// 			if(i==k) cluster.push_back(true);
// 			else cluster.push_back(false);
// 			tab[k].set_data(i,k,0); //set matrix tab ke K elemen node tersebut 0 jarak
// 		}
// 		int i=get_next_cluster();
// 		while(i!=UNDEFINED) {
// 			for(int j=0;j<size;j++) {
// 				if(!cluster[j]) tab[k].set_data(i,j,data.get_data(i,j)+tab[k].get_data(k,i));
// 			}
// 			i=get_next_cluster();
// 		}
// 	}
// }
