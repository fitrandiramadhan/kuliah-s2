//file		:	Sorting.cpp
//author	:	steaKK

#include "Sorting.hpp"

Sorting::Sorting() {
	size = DEFAULT_SIZE;
	data = new int[size];
	for(int i=0;i<size;i++) data[i] = 0;
}

Sorting::Sorting(int _size) {
	size = _size;
	data = new int[size];
	for(int i=0;i<size;i++) data[i] = 0;
}

Sorting::Sorting(const Sorting& _Sorting) {
	size = _Sorting.size;
	data = new int[size];
	for(int i=0;i<size;i++) data[i] = 0;
}

Sorting& Sorting::operator=(const Sorting& _Sorting) {
	size = _Sorting.size;
	for(int i=0;i<size;i++) data[i] = 0;
	return *this;
}

Sorting::~Sorting() {
	delete[] data;
}

int Sorting::get_size() {
	return size;
}

void Sorting::set_size(int _size) {
	size - _size;;
}

int Sorting::get_data(int pos) {
	return data[pos];
}

void Sorting::set_data(int pos, int val) {
	data[pos] = val;
}

bool Sorting::greater_than(int x, int y) {
	return x>y;
}

bool Sorting::less_than(int x, int y) {
	return x<y;
}

void Sorting::print() {
	std::cout << "size = " << size << std::endl;
	std::cout << "data = " << std::endl;
	for(int i=0;i<size;i++) {
		std::cout << data[i] << " ";
	}
	std::cout << endl;
}

void Sorting::init_data() {
	srand (time(NULL));
	for(int i=0;i<size;i++) {
		data[i] = rand() % 100;
	}
}

bool Sorting::is_odd(int x) {
	if(x%2==0) return false;
	else return true;
}

bool Sorting::oddeven_sequential() {
	for(int i=0;i<size;i++) {
		if(is_odd(i+1)) {
			for(int i=0;i<size/2;i+=2) {
				if(less_than(data[i],data[i+1])) swap(i,i+1);
			}
		}
		else{
			for(int i=1;i<size/2;i+=2) {
				if(less_than(data[i],data[i+1])) swap(i,i+1);
			}
		}
	}
}

bool Sorting::oddeven_openmp() {
	for(int i=0;i<size;i++) {
		if(is_odd(i+1)) {
			#pragma omp parallel for
			for(int i=0;i<size;i+=2) {
				if(less_than(data[i],data[i+1])) swap(i,i+1);
			}
		}
		else{
			#pragma omp parallel for
			for(int i=1;i<size;i+=2) {
				if(less_than(data[i],data[i+1])) swap(i,i+1);
			}
		}
	}
}

bool Sorting::quicksort(int lo, int hi) {
	if(lo<hi) {
		int p = partition(lo,hi);
		quicksort(lo,p-1);
		quicksort(p+1,hi);
	}
}

bool Sorting::quicksort_openmp(int lo, int hi) {
	if(lo<hi) {
		int p = partition(lo,hi);
		#pragma omp parallel sections
		{
			#pragma omp section
			{
				quicksort_openmp(lo,p-1);
			}
			#pragma omp section
			{
				quicksort_openmp(p+1,hi);
			}
		}
	}
}

int Sorting::partition(int lo, int hi) {
	int pivot = data[hi];
	int i = lo;
	for(int j=lo;j<hi;j++) {
		if(data[j]<=pivot) {
			swap(i,j);
			i++;
		}
	}
	swap(i,hi);
	return i;
}

void Sorting::swap(int a, int b) {
	int temp = data[a];
	data[a] = data[b];
	data[b] = temp;
}
