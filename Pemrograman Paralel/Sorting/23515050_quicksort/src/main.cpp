//file		:	Sorting.hpp
//author	:	steaKK

#include "Sorting.hpp"

int main() {

	cout << "sequential" << endl;
	for(int i=10;i<=1000000;i=i*10) {
		int sum = 0;
		for(int j=0;j<20;j++) {
			Sorting s0(i);
			s0.init_data();
			time_t t0 = time(NULL);
			s0.quicksort(0,s0.get_size());
			time_t t1 = time(NULL);
			sum += (t1-t0);
		}
		cout << i << " data = " << (float)(sum/20) << endl;
	}
	cout << "openmp" << endl;
	for(int i=10;i<=1000000;i=i*10) {
		int sum = 0;
		for(int j=0;j<20;j++) {
			Sorting s0(i);
			s0.init_data();
			time_t t0 = time(NULL);
			s0.quicksort_openmp(0,s0.get_size());
			time_t t1 = time(NULL);
			sum += (t1-t0);
		}
		cout << i << " data = " << (float)(sum/20) << endl;
	}

	return 0;
}
