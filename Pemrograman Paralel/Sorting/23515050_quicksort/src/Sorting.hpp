//file		:	Sorting.hpp
//author	:	steaKK

#ifndef Sorting_HPP
#define Sorting_HPP

#include <iostream>
#include <omp.h>
#include <ctime>

using namespace std;

class Sorting {

public:
	Sorting();
	Sorting(int);
	Sorting(const Sorting&);
	Sorting& operator=(const Sorting&);
	~Sorting();

	int get_size();
	void set_size(int);
	int get_data(int);
	void set_data(int,int);

	void print();
	void init_data();

	static bool greater_than(int,int);
	static bool less_than(int,int);
	static bool is_odd(int);

	bool oddeven_sequential();
	bool oddeven_openmp();

	bool quicksort(int,int);
	bool quicksort_openmp(int,int);
	int partition(int,int);
	void swap(int,int);

private:
	static const int DEFAULT_SIZE = 100;

	int size;
	int* data;
};

#endif
