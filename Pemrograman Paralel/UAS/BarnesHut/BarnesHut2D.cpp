//file		:	BarnesHut.cpp
//author	:	steaKK

#include "BarnesHut2D.hpp"

BarnesHut2D::BarnesHut2D() {

}

BarnesHut2D::BarnesHut2D(vector<BHTree> _data) {
	data = _data;
}

BarnesHut2D::BarnesHut2D(const BarnesHut2D& _BarnesHut2D) {
	data = _BarnesHut2D.data;
}

BarnesHut2D& BarnesHut2D::operator=(const BarnesHut2D& _BarnesHut2D) {
	data = _BarnesHut2D.data;
	return *this;
}

BarnesHut2D::~BarnesHut2D() {

}

void BarnesHut2D::print() {
	for(int i=0;i<data.size();i++) {
		cout << "data ke-" << i << endl;
		cout << "body   " << "pos_x= " << data[i].body.pos_x << "   pos_y= " << data[i].body.pos_y << "   mass= " << data[i].body.mass << endl;
		cout << "parent= " << data[i].parent << endl;
		cout << "child= " << data[i].child1 << " " << data[i].child2 << " " << data[i].child3 << " " << data[i].child4 << endl;
		cout << endl;
	}
}

float BarnesHut2D::get_xmin(vector<Particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].pos_x<result) result = p[i].pos_x;
	}
	return result;
}

float BarnesHut2D::get_xmax(vector<Particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].pos_x>result) result = p[i].pos_x;
	}
	return result;
}

float BarnesHut2D::get_ymin(vector<Particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].pos_y<result) result = p[i].pos_y;
	}
	return result;
}

float BarnesHut2D::get_ymax(vector<Particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].pos_y>result) result = p[i].pos_y;
	}
	return result;
}

BHTree BarnesHut2D::init_BHTree() {
	BHTree result;

	result.border.x_min = 0;
	result.border.x_max = 0;
	result.border.y_min = 0;
	result.border.y_max = 0;
	result.body.pos_x = 0;
	result.body.pos_y = 0;
	result.body.mass = 0;
	result.parent = -1;
	result.child1 = -1;
	result.child2 = -1;
	result.child3 = -1;
	result.child4 = -1;

	return result;
}

bool BarnesHut2D::has_child(BHTree BHT) {
	if(BHT.child1>-1 && BHT.child2>-1 && BHT.child3>-1 && BHT.child4>-1) return false;
	else return true;
}

Boundary BarnesHut2D::get_boundary_quad(int kuadran, Boundary _border) {
	Boundary result;

	if(kuadran==0) {
		result.x_min = (_border.x_min + _border.x_max)/2;
		result.x_max = _border.x_max;
		result.y_min = (_border.y_min + _border.y_max)/2;
		result.y_max = _border.y_max;
	}
	else if(kuadran==1) {;
		result.x_min = _border.x_min;
		result.x_max = (_border.x_min + _border.x_max)/2;
		result.y_min = (_border.y_min + _border.y_max)/2;
		result.y_max = _border.y_max;
	}
	else if(kuadran==2) {
		result.x_min = _border.x_min;
		result.x_max = (_border.x_min + _border.x_max)/2;
		result.y_min = _border.y_min;
		result.y_max = (_border.y_min + _border.y_max)/2;
	}
	else if(kuadran==3) {
		result.x_min = (_border.x_min + _border.x_max)/2;
		result.x_max = _border.x_max;
		result.y_min = _border.y_min;
		result.y_max = (_border.y_min + _border.y_max)/2;
	}

	return result;
}

void BarnesHut2D::generate_tree(vector<Particle> particle_set, Boundary _border, int _parent, int current) {
	cout << "andiganteng1" << endl;
	print();
	if(particle_set.size()>0) {
		cout << "andiganteng11" << endl;
		if(particle_set.size()==1) {
			cout << "andiganteng111" << endl;
			data[current].border = _border;
			data[current].body = particle_set[0];
			data[current].parent = _parent;
			data[current].child1 = -1;
			data[current].child2 = -1;
			data[current].child3 = -1;
			data[current].child4 = -1;
		}
		else {
			cout << "andiganteng112" << endl;
			BHTree temp = init_BHTree();
			data.push_back(temp);
			data[current].child1 = data.size()-1;
			data.push_back(temp);
			data[current].child2 = data.size()-1;
			data.push_back(temp);
			data[current].child3 = data.size()-1;
			data.push_back(temp);
			data[current].child4 = data.size()-1;
			vector<vector<Particle> > temp_vector(4);
			for(int i=0;i<particle_set.size();i++) {
				if(particle_set[i].pos_x>=(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y>=(_border.y_min+_border.y_max)/2)
					temp_vector[0].push_back(particle_set[i]);
				else if(particle_set[i].pos_x<(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y>=(_border.y_min+_border.y_max)/2)
					temp_vector[1].push_back(particle_set[i]);
				else if(particle_set[i].pos_x<(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y<(_border.y_min+_border.y_max)/2)
					temp_vector[2].push_back(particle_set[i]);
				else if(particle_set[i].pos_x>=(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y<(_border.y_min+_border.y_max)/2)
					temp_vector[3].push_back(particle_set[i]);
			}
			generate_tree(temp_vector[0],get_boundary_quad(0,_border),data[current].parent,data[current].child1);
			generate_tree(temp_vector[1],get_boundary_quad(1,_border),data[current].parent,data[current].child2);
			generate_tree(temp_vector[2],get_boundary_quad(2,_border),data[current].parent,data[current].child3);
			generate_tree(temp_vector[3],get_boundary_quad(3,_border),data[current].parent,data[current].child4);
		}
		cout << "andiganteng12" << endl;
	}
	cout << "andiganteng2" << endl;
}

void BarnesHut2D::generate_tree(vector<Particle> particle_set, Boundary _border, int _parent, int current) {
	if(particle_set.size()>0) {
		if(particle_set.size()==1) {
			data[current] = init_BHTree();
			data[current].border = _border;
			data[current].body = particle_set[0];
			data[current].parent = _parent;
		}
		else {
			vector<vector<Particle> > temp_vector;
			vector<Particle> temp_temp;
			for(int i=0;i<4;i++) temp_vector.push_back(temp_temp);
			for(int i=0;i<particle_set.size();i++) {
				if(particle_set[i].pos_x>=(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y>=(_border.y_min+_border.y_max)/2)
					temp_vector[0].push_back(particle_set[i]);
				else if(particle_set[i].pos_x<(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y>=(_border.y_min+_border.y_max)/2)
					temp_vector[1].push_back(particle_set[i]);
				else if(particle_set[i].pos_x<(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y<(_border.y_min+_border.y_max)/2)
					temp_vector[2].push_back(particle_set[i]);
				else if(particle_set[i].pos_x>=(_border.x_min+_border.x_max)/2 && particle_set[i].pos_y<(_border.y_min+_border.y_max)/2)
					temp_vector[3].push_back(particle_set[i]);
			}
			for(int i=0;i<4;i++) {

			}
		}
	}
}
