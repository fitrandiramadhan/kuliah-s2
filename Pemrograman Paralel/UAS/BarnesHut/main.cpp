//file		:	BarnesHut2D.hpp
//author	:	steaKK

#include "BarnesHut2D.hpp"

int main() {
	int n = 3;

	vector<Particle> galaxy;
	srand (time(NULL));

	for(int i=0;i<n;i++) {
		Particle p;
		p.pos_x = rand() % 100;
		p.pos_y = rand() % 100;
		p.mass = rand() % 100;
		galaxy.push_back(p);
	}
	Boundary B;
	B.x_min = BarnesHut2D::get_xmin(galaxy);
	B.x_max = BarnesHut2D::get_xmax(galaxy);
	B.y_min = BarnesHut2D::get_ymin(galaxy);
	B.y_max = BarnesHut2D::get_ymax(galaxy);

	BarnesHut2D BH;
	BH.generate_tree(galaxy,B,-1,-1);

	cout << "success" << endl;

	return 0;
}
