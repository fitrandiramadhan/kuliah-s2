//file		:	BarnesHut2D.hpp
//author	:	steaKK

#ifndef BarnesHut2D_HPP
#define BarnesHut2D_HPP

#include <iostream>
#include <vector>

using namespace std;

struct Boundary {
	float x_min;
	float x_max;
	float y_min;
	float y_max;
};

struct Particle {
	float pos_x;
	float pos_y;
	float mass;
};

struct BHTree {
	Boundary border;
	Particle body;
	int parent;
	int child1;
	int child2;
	int child3;
	int child4;
};

class BarnesHut2D {
public:
	BarnesHut2D();
	BarnesHut2D(vector<BHTree>);
	BarnesHut2D(const BarnesHut2D&);
	BarnesHut2D& operator=(const BarnesHut2D&);
	~BarnesHut2D();

	void print();
	static float get_xmin(vector<Particle>);
	static float get_xmax(vector<Particle>);
	static float get_ymin(vector<Particle>);
	static float get_ymax(vector<Particle>);
	static BHTree init_BHTree();
	static bool has_child(BHTree);
	Boundary get_boundary_quad(int,Boundary);

	void generate_tree(vector<Particle>,Boundary,int,int);

private:
	vector<BHTree> data;
};

#endif
