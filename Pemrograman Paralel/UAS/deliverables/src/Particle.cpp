//file		:	Particle.cpp
//author	:	steaKK

#include "Particle.hpp"

Particle::Particle() {
	position_x = 0;
	position_y = 0;
	mass = 0;
}

Particle::Particle(float _position_x, float _position_y, float _mass) {
	position_x = _position_x;
	position_y = _position_y;
	mass = _mass;
}

Particle::Particle(const Particle& _Particle) {
	position_x = _Particle.position_x;
	position_y = _Particle.position_y;
	mass = _Particle.mass;
}

Particle& Particle::operator=(const Particle& _Particle) {
	position_x = _Particle.position_x;
	position_y = _Particle.position_y;
	mass = _Particle.mass;
	return *this;
}

Particle::~Particle() {

}

float Particle::get_position_x() {
	return position_x;
}

float Particle::get_position_y() {
	return position_y;
}

float Particle::get_mass() {
	return mass;
}

void Particle::set_position_x(float _position_x) {
	position_x = _position_x;
}

void Particle::set_position_y(float _position_y) {
	position_y = _position_y;
}

void Particle::set_mass(float _mass) {
	mass = _mass;
}

void Particle::print() {
	cout << "position_x = " << position_x << endl;
	cout << "position_y = " << position_y << endl;
	cout << "mass = " << mass << endl;
}
