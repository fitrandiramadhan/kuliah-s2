//file		:	main.cpp
//author	:	steaKK

#include "BarnesHut.hpp"

int main() {
	int n_particle = 100;

	srand (time(NULL));

	vector<Particle> galaxy;
	for(int i=0;i<n_particle;i++) {
		float x = rand() % 100;
		float y = rand() % 100;
		float m = rand() % 100;
		Particle p(x,y,m);
		galaxy.push_back(p);
	}

	Boundary B(Boundary::get_xmin(galaxy),Boundary::get_xmax(galaxy),Boundary::get_ymin(galaxy),Boundary::get_ymax(galaxy));
	BarnesHut BT(BarnesHut::generate_tree(B,galaxy));

	BT.print();

	return 0;
}
