//file		:	BarnesHut.cpp
//author	:	steaKK

#include "BarnesHut.hpp"

BarnesHut::BarnesHut() {

}

BarnesHut::BarnesHut(Boundary _border, Particle _body, vector<BarnesHut> _child) {
	border = _border;
	body = _body;
	child = _child;
}

BarnesHut::BarnesHut(const BarnesHut& _BarnesHut) {
	border = _BarnesHut.border;
	body = _BarnesHut.body;
	child = _BarnesHut.child;
}

BarnesHut& BarnesHut::operator=(const BarnesHut& _BarnesHut) {
	border = _BarnesHut.border;
	body = _BarnesHut.body;
	child = _BarnesHut.child;
	return *this;
}

BarnesHut::~BarnesHut() {

}

Boundary BarnesHut::get_border() {
	return border;
}

Particle BarnesHut::get_body() {
	return body;
}

vector<BarnesHut> BarnesHut::get_child() {
	return child;
}

void BarnesHut::set_border(Boundary _border) {
	border = _border;
}

void BarnesHut::set_body(Particle _body) {
	body = _body;
}

void BarnesHut::set_child(vector<BarnesHut> _child) {
	child = _child;
}

void BarnesHut::print() {
	border.print();
	body.print();
	if(has_child()) {
		cout << "has_child" << endl;
		for(int i=0;i<child.size();i++) {
			child[i].print();
		}
	}
	else cout << "no_child" << endl;
}

bool BarnesHut::has_child() {
	if(child.size()>0) return true;
	else return false;
}

int BarnesHut::is_empty3(vector<vector<Particle> > v) {
	if(!v[0].empty() && v[1].empty() && v[2].empty() && v[3].empty()) return 0;
	if(v[0].empty() && !v[1].empty() && v[2].empty() && v[3].empty()) return 1;
	if(v[0].empty() && v[1].empty() && !v[2].empty() && v[3].empty()) return 2;
	if(v[0].empty() && v[1].empty() && v[2].empty() && !v[3].empty()) return 3;
	else return -1;

}

BarnesHut BarnesHut::generate_tree(Boundary _border, vector<Particle> particle_set) {
	BarnesHut result;
	if(particle_set.size()>0) {
		if(particle_set.size()==1) {
			result.border = _border;
			result.body = particle_set[0];
		}
		else {
			vector<vector<Particle> > temp(4);
			for(int i=0;i<particle_set.size();i++) {
				if(particle_set[i].get_position_x()>=(_border.get_x_min()+_border.get_x_max())/2 && particle_set[i].get_position_y()>=(_border.get_y_min()+_border.get_y_max())/2)
					temp[0].push_back(particle_set[i]);
				else if(particle_set[i].get_position_x()<(_border.get_x_min()+_border.get_x_max())/2 && particle_set[i].get_position_y()>=(_border.get_y_min()+_border.get_y_max())/2)
					temp[1].push_back(particle_set[i]);
				else if(particle_set[i].get_position_x()<(_border.get_x_min()+_border.get_x_max())/2 && particle_set[i].get_position_y()<(_border.get_y_min()+_border.get_y_max())/2)
					temp[2].push_back(particle_set[i]);
				else if(particle_set[i].get_position_x()>=(_border.get_x_min()+_border.get_x_max())/2 && particle_set[i].get_position_y()<(_border.get_y_min()+_border.get_y_max())/2)
					temp[3].push_back(particle_set[i]);
			}
			int fail_safe = is_empty3(temp);
			if(fail_safe>-1) {
				if(fail_safe>0) {
					temp[0].push_back(temp[fail_safe].back());
					temp[fail_safe].pop_back();
				}
				else {
					temp[1].push_back(temp[fail_safe].back());
					temp[fail_safe].pop_back();
				}
			}
			for(int i=0;i<4;i++) {
				result.child.push_back(generate_tree(Boundary::get_boundary_quad(i,_border),temp[i]));
			}
		}
	}
	return result;
}
