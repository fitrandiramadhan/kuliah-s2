//file		:	Particle.hpp
//author	:	steaKK

#ifndef Particle_HPP
#define Particle_HPP

#include <iostream>

using namespace std;

class Particle {
public:
	Particle();
	Particle(float,float,float);
	Particle(const Particle&);
	Particle& operator=(const Particle&);
	~Particle();

	float get_position_x();
	float get_position_y();
	float get_mass();
	void set_position_x(float);
	void set_position_y(float);
	void set_mass(float);

	void print();

private:
	float position_x;
	float position_y;
	float mass;
};

#endif
