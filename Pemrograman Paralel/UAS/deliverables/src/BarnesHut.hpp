//file		:	BarnesHut.hpp
//author	:	steaKK

#ifndef BarnesHut_HPP
#define BarnesHut_HPP

#include <iostream>
#include <vector>

#include "Boundary.hpp"
#include "Particle.hpp"

using namespace std;

class BarnesHut {
public:
	BarnesHut();
	BarnesHut(Boundary,Particle,vector<BarnesHut>);
	BarnesHut(const BarnesHut&);
	BarnesHut& operator=(const BarnesHut&);
	~BarnesHut();

	Boundary get_border();
	Particle get_body();
	vector<BarnesHut> get_child();
	void set_border(Boundary);
	void set_body(Particle);
	void set_child(vector<BarnesHut>);

	void print();

	bool has_child();

	static int is_empty3(vector<vector<Particle> >);
	static BarnesHut generate_tree(Boundary,vector<Particle>);

private:
	Boundary border;
	Particle body;
	vector<BarnesHut> child;
};

#endif
