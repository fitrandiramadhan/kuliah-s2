//file		:	QuadTree.cpp
//author	:	steaKK

#include "QuadTree.hpp"

QuadTree::QuadTree() {
	data.position_x = 0;
	data.position_y = 0;
	data.mass = 0;
	border.x_min = 0;
	border.x_max = 0;
	border.y_min = 0;
	border.y_max = 0;
}

// QuadTree::QuadTree(vector<QuadTree> _QuadTree) {
// 	data.position_x = 0;
// 	data.position_y = 0;
// 	data.mass = 0;
//
// 	child = _QuadTree;
// }

QuadTree::QuadTree(const QuadTree& _QuadTree) {
	data.position_x = _QuadTree.data.position_x;
	data.position_y = _QuadTree.data.position_y;
	data.mass = _QuadTree.data.mass;
	border.x_min = _QuadTree.border.x_min;
	border.x_max = _QuadTree.border.x_min;
	border.y_min = _QuadTree.border.y_min;
	border.y_max = _QuadTree.border.y_min;
	child = _QuadTree.child;
}

QuadTree& QuadTree::operator=(const QuadTree& _QuadTree) {
	data.position_x = _QuadTree.data.position_x;
	data.position_y = _QuadTree.data.position_y;
	data.mass = _QuadTree.data.mass;
	border.x_min = _QuadTree.border.x_min;
	border.x_max = _QuadTree.border.x_min;
	border.y_min = _QuadTree.border.y_min;
	border.y_max = _QuadTree.border.y_min;
	child = _QuadTree.child;

	return *this;
}

QuadTree::~QuadTree() {

}

boundary QuadTree::get_border() {
	return border;
}

void QuadTree::set_border(boundary _border) {
	border.x_min = _border.x_min;
	border.x_max = _border.x_max;
	border.y_min = _border.y_min;
	border.y_max = _border.y_max;
}

bool QuadTree::has_child() {
	if(child.size()>0) return true;
	else return false;
}

boundary QuadTree::get_boundary_quad(int kuadran) {
	boundary result;

	if(kuadran==0) {
		result.x_min = (border.x_min + border.x_max)/2;
		result.x_max = border.x_max;
		result.y_min = (border.y_min + border.y_max)/2;
		result.y_max = border.y_max;
	}
	else if(kuadran==1) {;
		result.x_min = border.x_min;
		result.x_max = (border.x_min + border.x_max)/2;
		result.y_min = (border.y_min + border.y_max)/2;
		result.y_max = border.y_max;
	}
	else if(kuadran==2) {
		result.x_min = border.x_min;
		result.x_max = (border.x_min + border.x_max)/2;
		result.y_min = border.y_min;
		result.y_max = (border.y_min + border.y_max)/2;
	}
	else if(kuadran==3) {
		result.x_min = (border.x_min + border.x_max)/2;
		result.x_max = border.x_max;
		result.y_min = border.y_min;
		result.y_max = (border.y_min + border.y_max)/2;
	}

	return result;
}

int QuadTree::square(int x, int times) {
	int result = 1;
	for(int i=0;i<times;i++) result = result*x;
	return result;
}

QuadTree QuadTree::generate_tree(int level, int rank2, vector<particle> particle_set) {
	vector<particle> result;
	vector<vector<particle> > temp(4);
	for(int i=0;i<particle_set.size();i++) {
		if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[0].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[1].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[2].push_back(particle_set[i]);
		else if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[3].push_back(particle_set[i]);
	}

	if(temp[0].size()>0 || temp[1].size()>0 || temp[2].size()>0 || temp[3].size()>0) {
		n_anak = square(4,level)
		for(int i=0;n_anak;i++) {

		}
	}
}

int QuadTree::get_tree_max_level(int rank, vector<particle> particle_set) {
	vector<particle> result;
	vector<vector<particle> > temp(4);
	for(int i=0;i<particle_set.size();i++) {
		if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[0].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[1].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[2].push_back(particle_set[i]);
		else if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[3].push_back(particle_set[i]);
	}

	if(temp[0].size()>0 || temp[1].size()>0 || temp[2].size()>0 || temp[3].size()>0) {
		n_anak = square(4,level)
		for(int i=0;n_anak;i++) {

		}
	}
}

float QuadTree::get_xmin(vector<particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_x<result) result = p[i].position_x;
	}
	return result;
}

float QuadTree::get_xmax(vector<particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_x>result) result = p[i].position_x;
	}
	return result;
}

float QuadTree::get_ymin(vector<particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_y<result) result = p[i].position_y;
	}
	return result;
}

float QuadTree::get_ymax(vector<particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_y>result) result = p[i].position_y;
	}
	return result;
}
