//file		:	QuadTree.cpp
//author	:	steaKK

#include "QuadTree.hpp"

QuadTree::QuadTree() {
	data.position_x = 0;
	data.position_y = 0;
	data.mass = 0;
	border.x_min = 0;
	border.x_max = 0;
	border.y_min = 0;
	border.y_max = 0;
}

// QuadTree::QuadTree(vector<QuadTree> _QuadTree) {
// 	data.position_x = 0;
// 	data.position_y = 0;
// 	data.mass = 0;
//
// 	child = _QuadTree;
// }

QuadTree::QuadTree(const QuadTree& _QuadTree) {
	data.position_x = _QuadTree.data.position_x;
	data.position_y = _QuadTree.data.position_y;
	data.mass = _QuadTree.data.mass;
	border.x_min = _QuadTree.border.x_min;
	border.x_max = _QuadTree.border.x_min;
	border.y_min = _QuadTree.border.y_min;
	border.y_max = _QuadTree.border.y_min;
	child = _QuadTree.child;
}

QuadTree& QuadTree::operator=(const QuadTree& _QuadTree) {
	data.position_x = _QuadTree.data.position_x;
	data.position_y = _QuadTree.data.position_y;
	data.mass = _QuadTree.data.mass;
	border.x_min = _QuadTree.border.x_min;
	border.x_max = _QuadTree.border.x_min;
	border.y_min = _QuadTree.border.y_min;
	border.y_max = _QuadTree.border.y_min;
	child = _QuadTree.child;

	return *this;
}

QuadTree::~QuadTree() {

}

boundary QuadTree::get_border() {
	return border;
}

void QuadTree::set_border(boundary _border) {
	border.x_min = _border.x_min;
	border.x_max = _border.x_max;
	border.y_min = _border.y_min;
	border.y_max = _border.y_max;
}

bool QuadTree::has_child() {
	if(child.size()>0) return true;
	else return false;
}

boundary QuadTree::get_boundary_quad(int kuadran) {
	boundary result;

	if(kuadran==0) {
		result.x_min = (border.x_min + border.x_max)/2;
		result.x_max = border.x_max;
		result.y_min = (border.y_min + border.y_max)/2;
		result.y_max = border.y_max;
	}
	else if(kuadran==1) {;
		result.x_min = border.x_min;
		result.x_max = (border.x_min + border.x_max)/2;
		result.y_min = (border.y_min + border.y_max)/2;
		result.y_max = border.y_max;
	}
	else if(kuadran==2) {
		result.x_min = border.x_min;
		result.x_max = (border.x_min + border.x_max)/2;
		result.y_min = border.y_min;
		result.y_max = (border.y_min + border.y_max)/2;
	}
	else if(kuadran==3) {
		result.x_min = (border.x_min + border.x_max)/2;
		result.x_max = border.x_max;
		result.y_min = border.y_min;
		result.y_max = (border.y_min + border.y_max)/2;
	}

	return result;
}

QuadTree QuadTree::generate_tree(vector<particle> particle_set) {
	cout << "andinganteng1" << endl;

	QuadTree result;
	result.border = border;
	result.data = data;
	result.child = child;

	vector<vector<particle> > temp(4);
	vector<particle> temp_temp;

	cout << "andinganteng2" << endl;

	for(int i=0;i<particle_set.size();i++) {
		if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[0].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y>=(border.y_min+border.y_max)/2)
			temp[1].push_back(particle_set[i]);
		else if(particle_set[i].position_x<(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[2].push_back(particle_set[i]);
		else if(particle_set[i].position_x>=(border.x_min+border.x_max)/2 && particle_set[i].position_y<(border.y_min+border.y_max)/2)
			temp[3].push_back(particle_set[i]);
	}

	cout << "andinganteng3" << endl;

	if(temp[0].size()>0 || temp[1].size()>0 || temp[2].size()>0 || temp[3].size()>0) {
		for(int i=0;i<4;i++) {
			QuadTree temp_QT;
			child.push_back(temp_QT);
		}
		for(int i=0;i<4;i++) {
			QuadTree QT;
			QT.border = get_boundary_quad(i);
			if(temp[i].size()>0) {
				if(temp[i].size()==1) {
					QT.data.position_x = temp[i][0].position_x;
					QT.data.position_y = temp[i][0].position_y;
					QT.data.mass = temp[i][0].mass;
				}
				else {
					QT = generate_tree(temp[i]);
				}
			}
			result.child[i] = QT;
		}
	}
	return result;
}

float QuadTree::get_xmin(vector<particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_x<result) result = p[i].position_x;
	}
	return result;
}

float QuadTree::get_xmax(vector<particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_x>result) result = p[i].position_x;
	}
	return result;
}

float QuadTree::get_ymin(vector<particle> p) {
	float result = 9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_y<result) result = p[i].position_y;
	}
	return result;
}

float QuadTree::get_ymax(vector<particle> p) {
	float result = -9999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].position_y>result) result = p[i].position_y;
	}
	return result;
}
