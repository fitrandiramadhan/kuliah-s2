//file		:	QuadTree.hpp
//author	:	steaKK

#ifndef QuadTree_HPP
#define QuadTree_HPP

#include <iostream>
#include <vector>

using namespace std;

struct particle {
	float position_x;
	float position_y;
	float mass;
};

struct boundary {
	float x_min;
	float x_max;
	float y_min;
	float y_max;
};

class QuadTree {
public:
	QuadTree();
	// QuadTree(vector<particle>);
	QuadTree(const QuadTree&);
	QuadTree& operator=(const QuadTree&);
	~QuadTree();

	boundary get_border();
	void set_border(boundary);

	bool has_child();
	boundary get_boundary_quad(int);

	QuadTree generate_tree(vector<particle>);

	static float get_xmin(vector<particle>);
	static float get_xmax(vector<particle>);
	static float get_ymin(vector<particle>);
	static float get_ymax(vector<particle>);

private:
	boundary border;
	particle data;
	vector<QuadTree> child;
};

#endif
