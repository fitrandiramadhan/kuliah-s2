//file		:	SolarSystemTree.cpp
//author	:	steaKK

#include "SolarSystemTree.hpp"

SolarSystemTree::SolarSystemTree() {
	size = DEFAULT_SIZE;
	data = new Planet[size];
	Planet temp;
	for(int i=0;i<size;i++) {
		data[i] = temp;
	}
}

SolarSystemTree::SolarSystemTree(int _size) {
	size = _size;
	data = new Planet[size];
	Planet temp;
	for(int i=0;i<size;i++) {
		data[i] = temp;
	}
}

SolarSystemTree::SolarSystemTree(const SolarSystemTree& _SolarSystemTree) {
	size = _SolarSystemTree.size;
	data = new Planet[size];
	Planet temp;
	for(int i=0;i<size;i++) {
		data[i] = _SolarSystemTree.data[i];
	}
}

SolarSystemTree& SolarSystemTree::operator=(const SolarSystemTree& _SolarSystemTree) {
	size = _SolarSystemTree.size;
	Planet temp;
	for(int i=0;i<size;i++) {
		data[i] = _SolarSystemTree.data[i];
	}
	return *this;
}

SolarSystemTree::~SolarSystemTree() {
	delete[] data;
}

float SolarSystemTree::get_size() {
	return size;
}

Planet SolarSystemTree::get_data(int pos) {
	return data[pos];
}

void SolarSystemTree::set_size(int _size) {
	size = _size;
}

void SolarSystemTree::set_data(int pos, Planet _Planet) {
	data[pos] = _Planet;
}

void SolarSystemTree::print() {
	for(int i=0;i<size;i++) {
		cout << "data ke-" << i << endl;
		cout << "position_x = " <<data[i].get_position_x() << endl;
		cout << "position_y = " <<data[i].get_position_y() << endl;
		cout << "mass = " <<data[i].get_mass() << endl;
	}
}

float SolarSystemTree::get_distance_squared(int p0, int p1) {
	return
		(data[p0].get_position_x()-data[p1].get_position_x()) * (data[p0].get_position_x()-data[p1].get_position_x())
		+ (data[p0].get_position_y()-data[p1].get_position_y()) * (data[p0].get_position_y()-data[p1].get_position_y());
}

float SolarSystemTree::calculate_once(int p0, int p1) {
	return
		GRAVITATIONAL_CONSTANT * data[p0].get_mass() * data[p1].get_mass()
		/ get_distance_squared(p0,p1);
}
