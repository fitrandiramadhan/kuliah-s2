//file		:	SolarSystemTree.hpp
//author	:	steaKK

#ifndef SolarSystemTree_HPP
#define SolarSystemTree_HPP

#include <iostream>
#include "Planet.hpp"

using namespace std;

class SolarSystemTree {
public:
	SolarSystemTree();
	SolarSystemTree(vector<Planet>);
	SolarSystemTree(const SolarSystemTree&);
	SolarSystemTree& operator=(const SolarSystemTree&);
	~SolarSystemTree();

	float get_size();
	Planet get_data(int);
	void set_size(int);
	void set_data(int,Planet);

	void print();

	float get_distance_squared(int,int);
	float calculate_once(int,int);

private:
	static const int DEFAULT_SIZE = 10;
	static const int GRAVITATIONAL_CONSTANT = 1;

	Planet data0;
	vector<Planet> data1;
};

#endif
