//file		:	Planet.hpp
//author	:	steaKK

#ifndef Planet_HPP
#define Planet_HPP

#include <iostream>

using namespace std;

class Planet {
public:
	Planet();
	Planet(float,float,float);
	Planet(const Planet&);
	Planet& operator=(const Planet&);
	~Planet();

	float get_position_x();
	float get_position_y();
	float get_mass();
	void set_position_x(float);
	void set_position_y(float);
	void set_mass(float);

private:
	#define DEFAULT_POSITION_X 0;
	#define DEFAULT_POSITION_Y 0;
	#define DEFAULT_MASS 0;

	float position_x;
	float position_y;
	float mass;
};

#endif
