//file		:	testtree.hpp
//author	:	steaKK

#ifndef testtree_HPP
#define testtree_HPP

#include <iostream>
#include <vector>

using namespace std;

class testtree {
public:
	testtree();
	testtree(const testtree&);
	testtree& operator=(const testtree&);
	~testtree();

	void print();
	testtree generate_tree(vector<int>);
	int get_max(vector<int>);
	int get_min(vector<int>);
	static bool is_all_same(vector<int>);
	static void print_vector(vector<int>);

private:
	int data0;
	vector<testtree> data1;
	// testtree* t1;
	// testtree* t2;

};

#endif
