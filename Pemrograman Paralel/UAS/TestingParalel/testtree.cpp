//file		:	testtree.cpp
//author	:	steaKK

#include "testtree.hpp"

testtree::testtree() {
	data0 = -1;
}

testtree::testtree(const testtree& _testtree) {
	data0 = _testtree.data0;
	data1 = _testtree.data1;
}

testtree& testtree::operator=(const testtree& _testtree) {
	data0 = _testtree.data0;
	data1 = _testtree.data1;
	return *this;
}

testtree::~testtree() {

}

void testtree::print() {
	cout << "tree start" << endl;
	cout << "data0 = " << data0 << endl;
	for(int i=0;i<data1.size();i++) data1[i].print();
	cout << "tree end" << endl;
}

testtree testtree::generate_tree(vector<int> v) {
	cout << "andiganteng1" << endl;
	print_vector(v);
	testtree result;
	if(v.size()>0) {
		cout << "size = " << v.size() << endl;
		if(v.size()==1) {
			cout << "andipaling ganteng" << endl;
			result.data0 = v[0];
		}
		else {
			vector<vector<int> > temp(2);
			int pivot = (get_min(v) + get_max(v)) / 2;
			for(int i=0;i<v.size();i++) {
				if(v[i]<pivot) temp[0].push_back(v[i]);
				else temp[1].push_back(v[i]);
			}
			if(temp[0].empty()) {
				temp[0].push_back(temp[1].back());
				temp[1].pop_back();
			}
			if(temp[1].empty()) {
				temp[1].push_back(temp[0].back());
				temp[0].pop_back();
			}
			result.data1.push_back(generate_tree(temp[0]));
			result.data1.push_back(generate_tree(temp[1]));
		}
	}
	return result;
}

int testtree::get_max(vector<int> v) {
	int result = 0;
	for(int i=0;i<v.size();i++) if(v[i]>result) result = v[i];
	return result;
}

int testtree::get_min(vector<int> v) {
	int result = 999;
	for(int i=0;i<v.size();i++) if(v[i]<result) result = v[i];
	return result;
}

bool testtree::is_all_same(vector<int> v) {
	int temp = v[0];
	for(int i=1;i<v.size();i++) {
		if(v[i]!=temp) return false;
	}
	return true;
}

void testtree::print_vector(vector<int> v) {
	cout << "size = " << v.size() << endl;
	for(int i=0;i<v.size();i++) {
		cout << "[" << v[i] << "]" << " ";
	}
	cout << endl;
}
