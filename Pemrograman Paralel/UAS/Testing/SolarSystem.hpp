//file		:	SolarSystem.hpp
//author	:	steaKK

#ifndef SolarSystem_HPP
#define SolarSystem_HPP

#include <iostream>
#include "Planet.hpp"

using namespace std;

class SolarSystem {
public:
	SolarSystem();
	SolarSystem(int);
	SolarSystem(const SolarSystem&);
	SolarSystem& operator=(const SolarSystem&);
	~SolarSystem();

	float get_size();
	Planet get_data(int);
	void set_size(int);
	void set_data(int,Planet);

	void print();

	float get_distance_squared(int,int);
	float calculate_once(int,int);

private:
	static const int DEFAULT_SIZE = 10;
	static const int GRAVITATIONAL_CONSTANT = 1;

	int size;
	Planet* data;
};

#endif
