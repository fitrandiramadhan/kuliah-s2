//file		:	Simulation.hpp
//author	:	steaKK

#ifndef Simulation_HPP
#define Simulation_HPP

#include <iostream>
#include "SolarSystem.hpp"

using namespace std;

class Simulation {
public:
	static SolarSystem simulate_once(SolarSystem);
	static SolarSystem simulate_ntimes(int,SolarSystem);
};

#endif
