//file		:	Simulation.cpp
//author	:	steaKK

#include "Simulation.hpp"

SolarSystem Simulation::simulate_once(SolarSystem _SolarSystem) {
	SolarSystem result(_SolarSystem);
	for(int i=0;i<_SolarSystem.get_size();i++) {
		Planet temp(result.get_data(i));
		for(int j=0;j<_SolarSystem.get_size();j++) {
			if(i!=j) {
				float force = _SolarSystem.calculate_once(i,j);
				temp.set_position_x(force*(_SolarSystem.get_data(j).get_position_x()-_SolarSystem.get_data(i).get_position_x()));
				temp.set_position_y(force*(_SolarSystem.get_data(j).get_position_y()-_SolarSystem.get_data(i).get_position_y()));
			}
		}
		result.set_data(i,temp);
	}
	return result;
}

SolarSystem Simulation::simulate_ntimes(int n, SolarSystem _SolarSystem) {
	SolarSystem result(_SolarSystem);
	for(int i=0;i<n;i++) result = simulate_once(result);
	return result;
}
