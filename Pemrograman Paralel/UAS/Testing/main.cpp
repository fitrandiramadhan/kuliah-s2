//file		:	main.cpp
//author	:	steaKK

#include "Simulation.hpp"

int main() {
	int size = 10000;
	int n = 5;
	SolarSystem SS0(size);
	srand (time(NULL));
	for(int i=0;i<SS0.get_size();i++) {
		Planet temp;
		temp.set_position_x(rand() % 100);
		temp.set_position_y(rand() % 100);
		temp.set_mass(rand() % 100);
		SS0.set_data(i,temp);
	}

	SS0.print();

	SolarSystem SS1(SS0);
	cout << "----- GOING ONCE -----" << endl;
	SS1 = Simulation::simulate_once(SS1);
	SS1.print();
	cout << endl;

	SS1 = SS0;
	cout << "----- GOING N-TIMES -----" << endl;
	SS1 = Simulation::simulate_ntimes(n,SS1);
	SS1.print();
	cout << endl;

	return 0;
}
