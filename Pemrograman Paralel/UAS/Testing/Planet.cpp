//file		:	Planet.cpp
//author	:	steaKK

#include "Planet.hpp"

Planet::Planet() {
	position_x = DEFAULT_POSITION_X;
	position_y = DEFAULT_POSITION_Y;
	mass = DEFAULT_MASS;
}

Planet::Planet(float _position_x, float _position_y, float _mass) {
	position_x = _position_x;
	position_y = _position_y;
	mass = _mass;
}

Planet::Planet(const Planet& _Planet) {
	position_x = _Planet.position_x;
	position_y = _Planet.position_y;
	mass = _Planet.mass;
}

Planet& Planet::operator=(const Planet& _Planet) {
	position_x = _Planet.position_x;
	position_y = _Planet.position_y;
	mass = _Planet.mass;
	return *this;
}

Planet::~Planet() {

}

float Planet::get_position_x() {
	return position_x;
}

float Planet::get_position_y() {
	return position_y;
}

float Planet::get_mass() {
	return mass;
}

void Planet::set_position_x(float _position_x) {
	position_x = _position_x;
}

void Planet::set_position_y(float _position_y) {
	position_y = _position_y;
}

void Planet::set_mass(float _mass) {
	mass = _mass;
}
