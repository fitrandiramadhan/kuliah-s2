//file		:	Boundary.hpp
//author	:	steaKK

#ifndef Boundary_HPP
#define Boundary_HPP

#include <iostream>
#include <vector>

#include "Particle.hpp"

using namespace std;

class Boundary {
public:
	Boundary();
	Boundary(float,float,float,float);
	Boundary(const Boundary&);
	Boundary& operator=(const Boundary&);
	~Boundary();

	float get_x_min();
	float get_x_max();
	float get_y_min();
	float get_y_max();

	void set_x_min(float);
	void set_x_max(float);
	void set_y_min(float);
	void set_y_max(float);

	void print();

	static float get_xmin(vector<Particle>);
	static float get_xmax(vector<Particle>);
	static float get_ymin(vector<Particle>);
	static float get_ymax(vector<Particle>);

	static Boundary get_boundary_quad(int,Boundary);



private:
	float x_min;
	float x_max;
	float y_min;
	float y_max;
};

#endif
