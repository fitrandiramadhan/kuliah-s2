//file		:	Boundary.cpp
//author	:	steaKK

#include "Boundary.hpp"

Boundary::Boundary() {
	x_min = 0;
	x_max = 0;
	y_min = 0;
	y_max = 0;
}

Boundary::Boundary(float _x_min, float _x_max, float _y_min, float _y_max) {
	x_min = _x_min;
	x_max = _x_max;
	y_min = _y_min;
	y_max = _y_max;
}

Boundary::Boundary(const Boundary& _Boundary) {
	x_min = _Boundary.x_min;
	x_max = _Boundary.x_max;
	y_min = _Boundary.y_min;
	y_max = _Boundary.y_max;
}

Boundary& Boundary::operator=(const Boundary& _Boundary) {
	x_min = _Boundary.x_min;
	x_max = _Boundary.x_max;
	y_min = _Boundary.y_min;
	y_max = _Boundary.y_max;
	return *this;
}

Boundary::~Boundary() {

}

float Boundary::get_x_min() {
	return x_min;
}

float Boundary::get_x_max() {
	return x_max;
}

float Boundary::get_y_min() {
	return y_min;
}

float Boundary::get_y_max() {
	return y_max;
}

void Boundary::set_x_min(float _x_min) {
	x_min = _x_min;
}

void Boundary::set_x_max(float _x_max) {
	x_max = _x_max;
}

void Boundary::set_y_min(float _y_min) {
	y_min = _y_min;
}

void Boundary::set_y_max(float _y_max) {
	y_max = _y_max;
}

void Boundary::print() {
	cout << "x_min = " << x_min << endl;
	cout << "x_max = " << x_max << endl;
	cout << "y_min = " << y_min << endl;
	cout << "y_max = " << y_max << endl;
}

float Boundary::get_xmin(vector<Particle> p) {
	float result = 999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].get_position_x()<result) result = p[i].get_position_x();
	}
	return result;
}

float Boundary::get_xmax(vector<Particle> p) {
	float result = -999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].get_position_x()>result) result = p[i].get_position_x();
	}
	return result;
}

float Boundary::get_ymin(vector<Particle> p) {
	float result = 999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].get_position_y()<result) result = p[i].get_position_y();
	}
	return result;
}

float Boundary::get_ymax(vector<Particle> p) {
	float result = -999999;
	for(int i=0;i<p.size();i++) {
		if(p[i].get_position_y()>result) result = p[i].get_position_y();
	}
	return result;
}

Boundary Boundary::get_boundary_quad(int kuadran, Boundary _border) {
	Boundary result;

	if(kuadran==0) {
		result.x_min = (_border.x_min + _border.x_max)/2;
		result.x_max = _border.x_max;
		result.y_min = (_border.y_min + _border.y_max)/2;
		result.y_max = _border.y_max;
	}
	else if(kuadran==1) {;
		result.x_min = _border.x_min;
		result.x_max = (_border.x_min + _border.x_max)/2;
		result.y_min = (_border.y_min + _border.y_max)/2;
		result.y_max = _border.y_max;
	}
	else if(kuadran==2) {
		result.x_min = _border.x_min;
		result.x_max = (_border.x_min + _border.x_max)/2;
		result.y_min = _border.y_min;
		result.y_max = (_border.y_min + _border.y_max)/2;
	}
	else if(kuadran==3) {
		result.x_min = (_border.x_min + _border.x_max)/2;
		result.x_max = _border.x_max;
		result.y_min = _border.y_min;
		result.y_max = (_border.y_min + _border.y_max)/2;
	}

	return result;
}
