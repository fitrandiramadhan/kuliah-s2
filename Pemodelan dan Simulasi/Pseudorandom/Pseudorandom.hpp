//file		:	Pseudorandom.hpp
//author	:	steaKK

#ifndef Pseudorandom_HPP
#define Pseudorandom_HPP

#include <iostream>
#include <cstdlib>

using namespace std;

class Pseudorandom {
public:
	Pseudorandom();
	~Pseudorandom();

private:
	long seed;
};

#endif
