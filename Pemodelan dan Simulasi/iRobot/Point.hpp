//file		:	Point.hpp
//author	:	steaKK

#ifndef Point_HPP
#define Point_HPP

#include <iostream>

using namespace std;

class Point {
public:
	Point();
	Point(int,int);
	Point(const Point&);
	Point& operator=(const Point&);
	~Point();

	int get_x();
	void set_x(int);
	int get_y();
	void set_y(int);

	void print();
	void init();

private:
	int x;
	int y;
};

#endif
