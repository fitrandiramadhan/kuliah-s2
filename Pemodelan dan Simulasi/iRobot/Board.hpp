//file		:	Board.hpp
//author	:	steaKK

#ifndef Board_HPP
#define Board_HPP

#include <iostream>

using namespace std;

class Board {

public:
	Board();
	Board(int,int);
	Board(const Board&);
	Board& operator=(const Board&);
	~Board();

	int get_height();
	void set_height(int);
	int get_width();
	void set_width(int);
	int get_data(int,int);
	void set_data(int,int,int);

	void print();
	void init_data();

private:
	static const int DEFAULT_HEIGHT = 100;
	static const int DEFAULT_WIDTH = 100;

	int height;
	int width;
	vector<vector<Thing> > data;
};

#endif
