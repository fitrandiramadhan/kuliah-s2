//file		:	Board.cpp
//author	:	steaKK

#include "Board.hpp"

Board::Board() {
	height = DEFAULT_HEIGHT;
	width = DEFAULT_WIDTH;
	for(int i=0;i<height;i++) {
		vector<Thing> vt;
		for(int j=0;j<width;j++) {
			Thing t(0,i,j);
			temp.push_back(t);
		}
		data.push_back(vt);
	}
}

Board::Board(int _height, int _width) {
	height = _height;
	width = _width;
	for(int i=0;i<height;i++) {
		vector<Thing> vt;
		for(int j=0;j<width;j++) {
			Thing t(0,i,j);
			temp.push_back(t);
		}
		data.push_back(vt);
	}
}

Board::Board(const Board& _Board) {
	height = _Board.height;
	width = _Board.width;
	data = _Board.data;
}

Board& Board::operator=(const Board& _Board) {
	height = _Board.height;
	width = _Board.width;
	data = _Board.data;
	return *this;
}

Board::~Board() {

}

int Board::get_height() {
	return height;
}

void Board::set_height(int _height) {
	height = _height;
}

int Board::get_width() {
	return width;
}

void Board::set_width(int _width) {
	width = _width;
}

Thing Board::get_data(int x, int y) {
	return data[x][y];
}

void Board::set_data(int x, int y, Thing t) {
	data[x][y] = t;
}

void Board::print() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			cout << "[" << data[i][j].get_value() << "]";
		}
		cout << endl;
	}
}

void Board::init_data() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			Thing t(0,i,j);
			data[i][j] = t;
		}
	}
}
