//file		:	Point.hpp
//author	:	steaKK

#include "Point.hpp"

Point();
Point(int,int);
Point(const Point&);
Point& operator=(const Point&);
~Point();

int get_x();
void set_x(int);
int get_y();
void set_y(int);

void print();
void init();
