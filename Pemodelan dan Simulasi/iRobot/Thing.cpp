//file		:	Thing.cpp
//author	:	steaKK

#include "Thing.hpp"

Thing::Thing() {
	value = DEFAULT_VALUE;
	posx = DEFAULT_POSX;
	posy = DEFAULT_POSY;
	collision = DEFAULT_COLLISION;
}

Thing::Thing(int _posx, int _posy, int _value, _collision) {
	posx = _posx;
	posy = _posy;
	value = _value;
	collision = _collision;
}

Thing::Thing(const Thing& _Thing) {
	value = _Thing.value;
	posx = _Thing.posx;
	posy = _Thing.posy;
	collision = _Thing.collision;
}

Thing& Thing::operator=(const Thing& _Thing) {
	value = _Thing.value;
	posx = _Thing.posx;
	posy = _Thing.posy;
	collision = _Thing.collision;
	return *this;
}

Thing::~Thing() {

}

int Thing::get_posx() {
	return posx;
}

int Thing::get_posy() {
	return posy;
}

int Thing::set_pos(int _posx, int _posy) {
	posx = _posx;
	posy = _posy;
}

int Thing::get_value() {
	return value;
}

void Thing::set_value(int _value) {
	value = _value;
}

int Thing::get_collision() {
	return collision;
}

void Thing::set_collision(int _collision) {
	collision = _collision;
}

void Thing::print() {
	cout << "Thing = " << posx << "," << posy << "," << value << "," << collision << endl;
}

void Thing::init() {
	posx = 0;
	posy = 0;
	value = 0;
	collision = false;
}

void Thing::step_to_target() {

}
