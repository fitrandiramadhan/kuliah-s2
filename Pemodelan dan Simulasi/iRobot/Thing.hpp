//file		:	Thing.hpp
//author	:	steaKK

#ifndef Thing_HPP
#define Thing_HPP

#include <iostream>

using namespace std;

class Thing {

public:
	Thing();
	Thing(int,int,int);
	Thing(const Thing&);
	Thing& operator=(const Thing&);
	~Thing();

	int get_value();
	void set_value(int);
	int get_posx();
	int get_posy();
	int set_pos(int,int);

	void print();
	void init();



	void step_to_target();

private:
	static const int DEFAULT_VALUE = 0;
	static const int DEFAULT_POS_X = 0;
	static const int DEFAULT_POS_Y = 0;
	static const int DEFAULT_COLLISION = 0;

	//val
	//0 = blank
	//1 = wall
	//2 = added wall
	//3 = iRobot
	//4 = target

	int value;
	bool collision;
	int posx;
	int posy;
};

#endif
