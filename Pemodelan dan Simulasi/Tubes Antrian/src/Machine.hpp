//file		:	Machine.hpp
//author	:	steaKK

#ifndef Machine_HPP
#define Machine_HPP

#include <iostream>
#include <vector>

#include "Job.hpp"

using namespace std;

class Machine {
public:
	Machine();
	Machine(vector<Job>);
	Machine(const Machine&);
	Machine& operator=(const Machine&);
	~Machine();

	vector<Job> get_data();
	void set_data(vector<Job>);

	void print();

private:
	vector<Job> data;
};

#endif
