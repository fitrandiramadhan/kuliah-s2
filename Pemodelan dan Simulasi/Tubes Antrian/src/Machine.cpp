//file		:	Machine.cpp
//author	:	steaKK

#include "Machine.hpp"

Machine::Machine() {

}

Machine::Machine(vector<Job> _data) {
	data = _data;
}

Machine::Machine(const Machine& _Machine) {
	data = _Machine.data;
}

Machine& Machine::operator=(const Machine& _Machine) {
	data = _Machine.data;
	return *this;
}

Machine::~Machine() {

}

vector<Job> Machine::get_data() {
	return data;
}

void Machine::set_data(vector<Job> _data) {
	data = _data;
}

void Machine::print() {
	for(int i=0;i<data.size();i++) {
		cout << "Job ke-" << i << " ";
		data[i].print();
	}
}
