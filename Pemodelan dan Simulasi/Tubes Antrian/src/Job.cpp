//file		:	Job.cpp
//author	:	steaKK

#include "Job.hpp"

Job::Job() {
	duration = -1;
}

Job::Job(int _duration) {
	duration = _duration;
}

Job::Job(const Job& _Job) {
	duration = _Job.duration;
}

Job& Job::operator=(const Job& _Job) {
	duration = _Job.duration;
	return *this;
}

Job::~Job() {

}

int Job::get_duration() {
	return duration;
}

void Job::set_duration(int _duration) {
	duration = _duration;
}

void Job::print() {
	cout << "duration = " << duration << endl;
}
