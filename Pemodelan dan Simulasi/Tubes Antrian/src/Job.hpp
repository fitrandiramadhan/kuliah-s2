//file		:	Job.hpp
//author	:	steaKK

#ifndef Job_HPP
#define Job_HPP

#include <iostream>

using namespace std;

class Job {
public:
	Job();
	Job(int);
	Job(const Job&);
	Job& operator=(const Job&);
	~Job();

	int get_duration();
	void set_duration(int);

	void print();

private:
	int duration;
};

#endif
