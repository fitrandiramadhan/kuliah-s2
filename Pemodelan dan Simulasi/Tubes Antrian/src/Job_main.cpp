//file		:	job_main.cpp
//author	:	steaKK

#include "Job.hpp"

int main() {
	Job j0;
	Job j1(10);
	Job j2(j1);
	Job j3 = j2;

	j2.set_duration(5);
	cout << j2.get_duration() << endl;

	j2.print();
	j1.print();

	return 0;
}
