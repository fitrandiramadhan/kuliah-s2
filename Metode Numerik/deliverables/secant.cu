//file		:	secant.cu
//author	:	steaKK

#include <iostream>
#include <cuda.h>

using namespace std;

__device__ float cuda_get_relativeerror(float x_old, float x_new) {
	float result = (x_new-x_old)/x_new*100;
	if(result<0) result = -result;
	return result;
}

__device__ float cuda_f0function(float x) {
	return 3*x*x*x + 2*x*x - x + 1;
}

__global__ void cuda_secant(float* result, float* xr, float* es, float* perturbation, int* maxit) {
	float ea = 999999.0;
	float x_old = xr[0];
	int iter = 0;
	while(ea>es[0] && iter<=maxit[0]) {
		float x_new = x_old - (perturbation[0]*x_old*cuda_f0function(x_old)/(cuda_f0function(x_old+perturbation[0]*x_old)-cuda_f0function(x_old)));
		ea = cuda_get_relativeerror(x_old,x_new);
		x_old = x_new;
		iter++;
	}
	result[0] = x_old;
}

int main() {
	float xr = -10.0;
	float es = 0.5;
	float perturbation = 0.00001;
	int maxit = 50;
	float result = 0;

	//user interface
	cout << "masukkan xr = "; cin >> xr;
	cout << "masukkan es = "; cin >> es;
	cout << "masukkan perturbation = "; cin >> perturbation;
	cout << "masukkan maxit = "; cin >> maxit;

	float* d_xr;
	float* d_es;
	float* d_perturbation;
	int* d_maxit;
	float* d_float_result;

	cudaMalloc(&d_xr, sizeof(float));
	cudaMalloc(&d_es, sizeof(float));
	cudaMalloc(&d_perturbation, sizeof(float));
	cudaMalloc(&d_maxit, sizeof(int));
	cudaMalloc(&d_float_result, sizeof(float));

	cudaMemcpy(d_xr, &xr, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_es, &es, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_perturbation, &perturbation, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_maxit, &maxit, sizeof(int), cudaMemcpyHostToDevice);

	cuda_secant<<<1,1>>>(d_float_result,d_xr,d_es,d_perturbation,d_maxit);

	cudaMemcpy(&result, d_float_result, sizeof(float), cudaMemcpyDeviceToHost);

	cout << "ans = " << result << endl;

	cudaFree(d_xr);
	cudaFree(d_es);
	cudaFree(d_perturbation);
	cudaFree(d_maxit);
	cudaFree(d_float_result);

	return 0;
}
