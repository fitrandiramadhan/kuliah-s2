//file		:	unit_test_1.hpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;

void print_result(vector<float> result) {
	cout << "ans = ";
	for(int i=0;i<result.size();i++) {
		cout << result[i] << " ";
	}
	cout << endl;
}

int main() {

	vector<float> result;
	float xmin = -10.0;
	float xmax = 10.0;
	int ns = 50;
	float es = 0.5;
	int maxit = 50;
	float xr = 5.0;
	float perturbation = 0.00001;

	cout << endl;
	cout << "inc_search" << endl;
	result = MetodeNumerik::inc_search(xmin,xmax,ns);
	print_result(result); cout << endl;

	cout << "bisection" << endl;
	result = MetodeNumerik::bisection(xmin,xmax,es,maxit);
	print_result(result); cout << endl;

	cout << "false_position" << endl;
	result = MetodeNumerik::false_position(xmin,xmax,es,maxit);
	print_result(result); cout << endl;

	cout << "newton_raphson" << endl;
	result = MetodeNumerik::newton_raphson(xr,es,maxit);
	print_result(result); cout << endl;

	cout << "secant" << endl;
	result = MetodeNumerik::secant(xr,es,perturbation,maxit);
	print_result(result); cout << endl;

	return 0;
}
