//file		:	unit_test_matrix.hpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;

int main() {

	Matrix m(3,4);
	// init
	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27);// m.set_data(0,4,1.02);
	m.set_data(1,0,0.26); m.set_data(1,1,0.32); m.set_data(1,2,0.18); m.set_data(1,3,0.24);// m.set_data(1,4,1.00);
	m.set_data(2,0,0.61); m.set_data(2,1,0.22); m.set_data(2,2,0.20); m.set_data(2,3,0.31);// m.set_data(2,4,1.34);
	m.set_data(3,0,0.40); m.set_data(3,1,0.34); m.set_data(3,2,0.36); m.set_data(3,3,0.17);// m.set_data(3,4,1.27);
	m.print();

	return 0;
}
