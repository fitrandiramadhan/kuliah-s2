//file		:	Matrix.cpp
//author	:	steaKK

#include "Matrix.hpp"

Matrix::Matrix() {
	height = DEFAULT_HEIGHT;
	width = DEFAULT_WIDTH;
	data = new float[height*width];
	for(int i=0;i<height*width;i++) data[i] = 0;
}

Matrix::Matrix(int _height, int _width) {
	height = _height;
	width = _width;
	data = new float[height*width];
	for(int i=0;i<height*width;i++) data[i] = 0;
}

Matrix::Matrix(const Matrix& _Matrix) {
	height = _Matrix.height;
	width = _Matrix.width;
	data = new float[height*width];
	for(int i=0;i<height*width;i++) data[i] = _Matrix.data[i];
}

Matrix& Matrix::operator=(const Matrix& _Matrix) {
	height = _Matrix.height;
	width = _Matrix.width;
	for(int i=0;i<height*width;i++) data[i] = _Matrix.data[i];
	return *this;
}

Matrix::~Matrix() {
	delete[] data;
}

int Matrix::get_height() {
	return height;
}

void Matrix::set_height(int _height) {
	height = _height;
}

int Matrix::get_width() {
	return width;
}

void Matrix::set_width(int _width) {
	width = _width;
}

float Matrix::get_data(int i, int j) {
	return data[i*width+j];
}

void Matrix::set_data(int i, int j, float x) {
	data[i*width+j] = x;
}

void Matrix::print() {
	for(int i=0;i<height;i++) {
		for(int j=0;j<width;j++) {
			cout << get_data(i,j) << " ";
		}
		cout << endl;
	}
}
