//file		:	unit_test_2.hpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;

int main() {

	vector<float> result;

	Matrix m(3,4);
	// init
	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
	m.set_data(1,0,0.26); m.set_data(1,1,0.32); m.set_data(1,2,0.18); m.set_data(1,3,0.24); m.set_data(1,4,1.00);
	m.set_data(2,0,0.61); m.set_data(2,1,0.22); m.set_data(2,2,0.20); m.set_data(2,3,0.31); m.set_data(2,4,1.34);
	m.set_data(3,0,0.40); m.set_data(3,1,0.34); m.set_data(3,2,0.36); m.set_data(3,3,0.17); m.set_data(3,4,1.27);
	m.print();

	int maxit = 10;
	vector<float> initial_guess;
	initial_guess.push_back(0); initial_guess.push_back(0); initial_guess.push_back(0);

	// Matrix m1(9,10);
	// m1.set_data(0,0,0.7071);m1.set_data(0,1,0.0); 	m1.set_data(0,2,0.0); 	m1.set_data(0,3,-1); 	m1.set_data(0,4,-0.8660);	m1.set_data(0,5,0.0); 	m1.set_data(0,6,0.0); 	m1.set_data(0,7,0.0); 	m1.set_data(0,8,0.0);		m1.set_data(0,9,0.0);
	// m1.set_data(1,0,0.7071);m1.set_data(1,1,0.0); 	m1.set_data(1,2,1.0); 	m1.set_data(1,3,0.0); 	m1.set_data(1,4,0.5);		m1.set_data(1,5,0.0); 	m1.set_data(1,6,0.0); 	m1.set_data(1,7,0.0); 	m1.set_data(1,8,0.0);		m1.set_data(1,9,-1000.0);
	// m1.set_data(2,0,0.0);	m1.set_data(2,1,1.0); 	m1.set_data(2,2,0); 	m1.set_data(2,3,0.0); 	m1.set_data(2,4,0.0);		m1.set_data(2,5,-1.0); 	m1.set_data(2,6,0.0); 	m1.set_data(2,7,0.0); 	m1.set_data(2,8,0.0);		m1.set_data(2,9,0.0);
	// m1.set_data(3,0,0.0); 	m1.set_data(3,1,0.0); 	m1.set_data(3,2,-1.0);	m1.set_data(3,3,0.0); 	m1.set_data(3,4,0.0);		m1.set_data(3,5,0.0); 	m1.set_data(3,6,0.0); 	m1.set_data(3,7,0.0); 	m1.set_data(3,8,0.0);		m1.set_data(3,9,0.0);
	// m1.set_data(4,0,0.0); 	m1.set_data(4,1,0.0); 	m1.set_data(4,2,0.0); 	m1.set_data(4,3,0.0); 	m1.set_data(4,4,0.0);		m1.set_data(4,5,0.0); 	m1.set_data(4,6,1.0); 	m1.set_data(4,7,0.0); 	m1.set_data(4,8,0.7071);	m1.set_data(4,9,500.0);
	// m1.set_data(5,0,0.0); 	m1.set_data(5,1,0.0); 	m1.set_data(5,2,0.0); 	m1.set_data(5,3,1);	 	m1.set_data(5,4,0.0);		m1.set_data(5,5,0.0); 	m1.set_data(5,6,0.0); 	m1.set_data(5,7,0.0); 	m1.set_data(5,8,-0.7071);	m1.set_data(5,9,0.0);
	// m1.set_data(6,0,0.0); 	m1.set_data(6,1,0.0); 	m1.set_data(6,2,0.0); 	m1.set_data(6,3,0.0); 	m1.set_data(6,4,0.8660);	m1.set_data(6,5,1.0); 	m1.set_data(6,6,0.0); 	m1.set_data(6,7,-1.0); 	m1.set_data(6,8,0.0);		m1.set_data(6,9,0.0);
	// m1.set_data(7,0,0.0); 	m1.set_data(7,1,0.0); 	m1.set_data(7,2,0.0); 	m1.set_data(7,3,0.0); 	m1.set_data(7,4,-0.5);		m1.set_data(7,5,0.0); 	m1.set_data(7,6,-1.0); 	m1.set_data(7,7,0.0); 	m1.set_data(7,8,0.0);		m1.set_data(7,9,-500.0);
	// m1.set_data(8,0,0.0); 	m1.set_data(8,1,0.0); 	m1.set_data(8,2,0.0); 	m1.set_data(8,3,0.0); 	m1.set_data(8,4,0.00);		m1.set_data(8,5,0.0); 	m1.set_data(8,6,0.0); 	m1.set_data(8,7,0.0); 	m1.set_data(8,8,0.7071);	m1.set_data(8,9,0.0);
	// m1.print();

	result = MetodeNumerik::jacobi_iteration2(m,initial_guess,maxit);
	// cout << "--- gauss_naive ---" << endl;
	// for(int i=0;i<result.size();i++) {
	// 	cout << result[0] << endl;
	// }
	// cout << "-------------------" << endl;

	return 0;
}
