// dekompLU.cpp : Defines the entry point for the console application.
//

#include<iostream>

using namespace std;

int main()
{

    int n,i,k,j,p;
    double a[10][10],l[10][10]={0},u[10][10]={0},sum,b[10],z[10]={0},x[10]={0};
    cout<<"Jumlah orde matriks = ";
    n = 3;
	a[1][1] = 2;
	a[1][2] = -4;
	a[1][3] = 1;
	a[2][1] = 6;
	a[2][2] = 2;
	a[2][3] = -1;
	a[3][1] = -2;
	a[3][2] = 6;
	a[3][3] = -2;
	
	b[1] = 4;
	b[2] = 10;
	b[3] = -6;
	
	
   

	//Dekomposisi LU
    for(k=1;k<=n;k++)
    {
        u[k][k]=1;
        for(i=k;i<=n;i++)
        {
            sum=0;
            for(p=1;p<=k-1;p++)
                sum+=l[i][p]*u[p][k];
            l[i][k]=a[i][k]-sum;
        }
		
        for(j=k+1;j<=n;j++)
        {
            sum=0;
            for(p=1;p<=k-1;p++)
                sum+=l[k][p]*u[p][j];
            u[k][j]=(a[k][j]-sum)/l[k][k];
        }
    }
    
	//Matriks LU
	cout<<endl<<endl<<"Matriks LU :"<<endl;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<l[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<u[i][j]<<"  ";
        cout<<endl;
    }

    //Mencari nilai Y; LY=b
    for(i=1;i<=n;i++)
    {                                        //forward subtitution 
        sum=0;
        for(p=1;p<i;p++)
        sum+=l[i][p]*z[p];
        z[i]=(b[i]-sum)/l[i][i];
    }
    
	//Mencari nilai X; UX=Y
    for(i=n;i>0;i--)
    {
        sum=0;
        for(p=n;p>i;p--)
            sum+=u[i][p]*x[p];
        x[i]=(z[i]-sum)/u[i][i];
    }
    
	//Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i=1;i<=n;i++)
        cout<<endl<<x[i];

    return 0;
}
