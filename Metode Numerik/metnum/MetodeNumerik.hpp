//file		:	MetodeNumerik.hpp
//author	:	steaKK

#ifndef MetodeNumerik_HPP
#define MetodeNumerik_HPP

#include <iostream>
#include <vector>

using namespace std;

class MetodeNumerik {
public:
	static bool is_positive(float);

	static vector<float> inc_search(float,float,int);

	static float ffunction(float);

private:

};

#endif
