//file		:	main.hpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;



int main() {
	vector<float> result;
	float xmin = -10.0;
	float xmax = 10.0;
	int ns = 50;

	// result = MetodeNumerik::ff(xmin,xmax,ns);

	// for(int i=0;i<result.size();i++) {
	// 	cout << result[i] << endl;
	// }

	float x = MetodeNumerik::ffunction(xmin);
	cout << x << endl;

	return 0;
}
