//file		:	MetodeNumerik.cpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

//checking sign
bool MetodeNumerik::is_positive(float x) {
	if(x<0) return false;
	else return true;
}

float MetodeNumerik::ffunction(float x) {
	return 3*x*x*x + 2*x*x - x + 1;
}

//Incremental Search
//fu = function f(x)
//xmin = lowest x space to be tested
//xmax = highest x space to be tested
//ns = number of interval in the x space tested
//return value = root prediction
vector<float> MetodeNumerik::inc_search(float xmin, float xmax, int ns) {
	vector<float> linspace;
	float delta = (xmax-xmin)/ns;
	linspace.push_back(xmin);
	vector<float> space_y;
	for(int i=0;i<ns;i++) {
		linspace.push_back(xmin+(i*delta));
		space_y.push_back(ffunction(linspace[i]));
	}
	bool sign = is_positive(space_y[0]);
	vector<float> result;
	for(int i=1;i<space_y.size();i++) {
		if(sign!=is_positive(space_y[i])) {
			sign = is_positive(space_y[i]);
			result.push_back((space_y[i-1]+space_y[i])/2);
		}
	}
	return result;
}
