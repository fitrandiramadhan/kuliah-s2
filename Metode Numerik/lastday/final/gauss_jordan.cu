// gaussJordan.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <cmath>

using namespace std;

int get_index(int i, int j, int orde) {
	return i*orde+j;
}

__device__ int cuda_get_index(int i, int j, int orde) {
	return i*orde+j;
}

__global__ void cuda_gauss(double* a, double* b, double* x, int* orde) {

	int n = orde[0];
	double temp,faktor;

	//Gauss Jordan
    for(int k=1;k<=n;k++)
    {
		faktor = a[cuda_get_index(k,k,n)];
        for(int j=1;j<=n;j++)
        {
			a[cuda_get_index(k,j,n)] = a[cuda_get_index(k,j,n)]/faktor;
        }
		b[k] = b[k]/faktor;

		for(int i=1;i<=n;i++)
		{
			if (i != k)
			{
				temp = a[cuda_get_index(i,k,n)];
				for (int j=1;j<=n;j++)
				{
					a[cuda_get_index(i,j,n)] = a[cuda_get_index(i,j,n)]-temp*a[cuda_get_index(k,j,n)];
				}
				b[i] = b[i]-temp*b[k];
			}
		}

    }

    //Mencari X; IX=b'
    for(int i=1;i<=n;i++)
    {
        x[i] = b[i];
    }
}

int main()
{

    int n;
	double* a = new double[100];
	for(int i=0;i<100;i++) a[i] = 0;
	double* b = new double[10];
	for(int i=0;i<10;i++) b[i] = 0;
	double* x = new double[10];
	for(int i=0;i<10;i++) x[i] = 0;

	cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(int i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(int j=1;j<=n;j++)
            cin>>a[get_index(i,j,n)];
    }
    cout<<"\nElemen matriks B = "<<endl;
    for(int i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[get_index(i,j,n)]<<"  ";
        cout<<endl;
    }
    cout<<endl;

	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

	double* d_a;
	double* d_b;
	double* d_x;
	int* d_n;

	cudaMalloc(&d_a, 100*sizeof(double));
	cudaMalloc(&d_b, 10*sizeof(double));
	cudaMalloc(&d_x, 10*sizeof(double));
	cudaMalloc(&d_n, sizeof(int));

	cudaMemcpy(d_a, a, 100*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

	cuda_gauss<<<1,1>>>(d_a,d_b,d_x,d_n);

	cudaMemcpy(x, d_x, 10*sizeof(double), cudaMemcpyDeviceToHost);

    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(int i=1;i<=n;i++)
        cout<<endl<<"x"<<i<<" = "<<x[i];
	cout << endl;

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_x);
	cudaFree(d_n);
	delete[] a;
	delete[] b;
	delete[] x;

    return 0;
}
