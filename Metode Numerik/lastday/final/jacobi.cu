
#include <iostream>
#include <cmath>
#include <cuda.h>

using namespace std;

int get_index(int i, int j, int orde) {
	return i*orde+j;
}

__device__ int cuda_get_index(int i, int j, int orde) {
	return i*orde+j;
}

__global__ void cuda_jacobi(double* a, double* b, double* x, int* orde) {
	//Iterasi Jacobi
	int n = orde[0];

	double* err = new double[10];
	for(int i=0;i<10;i++) err[i] = 0;
	double* xold = new double[10];
	for(int i=0;i<10;i++) xold[i] = 0;
	double* c = new double[10];
	for(int i=0;i<10;i++) c[i] = 0;

	int k = 1;
	do
	{
		// cout<<endl<<"i-"<<k<<"\n";
		for (int i = 1; i <= n; i++)
		{
			xold[i] = x[i];
			c[i] = b[i];
			for (int j = 1; j <= n; j++)
			{
				if (i != j)
				{
					c[i] = c[i] - a[cuda_get_index(i,j,n)] * x[j];
				}
			}
		}

		int ea = 0;
		for (int i = 1; i <= n; i++)
		{
			x[i] = c[i]/a[cuda_get_index(i,i,n)];
			err[i] = fabs((x[i] - xold[i])/x[i]);
			ea = ea + err[i];
			// cout<<"x"<<i<<" = "<<x[i]<<"\terr-"<<i<<" = "<<err[i]<<endl;
		}
		// int eps = ea/n;
		// cout<<"ea = "<<ea<<"\t\teps = "<<eps<<endl;
		k = k + 1;

		if (ea <= 0.000001)
		{
			break;
		}
	} while (k <= 20);

	delete[] err;
	delete[] xold;
	delete[] c;
}

int main()
{

	int n;
	double* a = new double[100];
	for(int i=0;i<100;i++) a[i] = 0;
	double* b = new double[10];
	for(int i=0;i<100;i++) a[i] = 0;
	double* x = new double[10];
	for(int i=0;i<10;i++) x[i] = 0;

	cout<<"Jumlah orde matriks A = ";
    cin>>n;

	cout<<"Koefisien Matriks A : ";
    for(int i = 1; i <= n; i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(int j = 1; j <= n; j++)
		{
            cin>>a[get_index(i,j,n)];
		}
    }

	cout<<"\nElemen matriks B = ";
    for(int i = 1; i <= n; i++)
    {
		cin>>b[i];
	}

	cout<<"\nTebakan awal x = ";
    for(int i = 1; i <= n; i++)
	{
		cin>>x[i];
	}

	//Menampilkan matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= n; j++)
		{
			cout<<a[get_index(i,j,n)]<<"  ";
		}
		cout<<endl;
    }
    cout<<endl;

	//Menampilkan matriks b
	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}
	cout<<endl;

	//Menampilkan tebakan awal x
	cout<<"\nTebakan awal x : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"x["<<i<<"] = "<<x[i]<<"\n";
	}

	double* d_a;
	double* d_b;
	double* d_x;
	int* d_n;

	cudaMalloc(&d_a, 100*sizeof(double));
	cudaMalloc(&d_b, 10*sizeof(double));
	cudaMalloc(&d_x, 10*sizeof(double));
	cudaMalloc(&d_n, sizeof(int));

	cudaMemcpy(d_a, a, 100*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

	cuda_jacobi<<<1,1>>>(d_a,d_b,d_x,d_n);

	cudaMemcpy(x, d_x, 10*sizeof(double), cudaMemcpyDeviceToHost);

    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(int i = 1; i <= n; i++)
	{
        cout<<endl<<"x"<<i<<" = "<<x[i];
	}
	cout << endl;

	//bersih2
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_x);
	cudaFree(d_n);
	delete[] a;
	delete[] b;
	delete[] x;

    return 0;
}
