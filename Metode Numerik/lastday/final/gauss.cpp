// gauss.cpp : Defines the entry point for the console application.
//

#include <iostream>

using namespace std;

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);

    int n,i,k,j,p;
    double a[10][10],l[10][10]={0},u[10][10]={0},faktor,b[10],x[10]={0};
    cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(j=1;j<=n;j++)
            cin>>a[i][j];
    }
    cout<<"\nElemen matriks B = "<<endl;
    for(i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;

	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

	//Gauss
    for(k=1;k<n;k++)
    {
        for(int i=k+1;i<=n;i++)
        {
			faktor=a[i][k]/a[k][k];
			b[i] = b[i]-faktor*b[k];
			for(p=k;p<=n;p++)
			{
				u[1][p] = a[1][p];
				u[i][p] = a[i][p]-faktor*a[k][p];
				a[i][p] = u[i][p];
			}
        }

    }

	//Menampilkan Matriks
	cout<<endl<<"Matriks A' : \n";
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<u[i][j]<<"  ";
        cout<<endl;
    }

	cout<<"\nMatriks b' : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

    //Mencari X; AX=b'
    for(i=n;i>0;i--)
    {
        faktor=0;
        for(p=n;p>i;p--)
            faktor+=u[i][p]*x[p];
        x[i]=(b[i]-faktor)/u[i][i];
    }
    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i=1;i<=n;i++)
        cout<<endl<<"x"<<i<<" = "<<x[i];


    return 0;
}
