// croutLU.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<conio.h>

using namespace std;

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);

    int n,i,k,j,p,r;
	double a[10][10],d[10][10]={0},sum,faktor,b[10],z[10]={0},x[10]={0};
    cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(j=1;j<=n;j++)
            cin>>a[i][j];
    }
    cout<<"\nElemen matriks B = ";
    for(i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;
	
	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}
    
	//Dekomposisi LU metode Crout
    for(p=1;p<=n;p++)
    {
        for(int j=p;j<=n;j++)
        {
			sum = 0;
			for(k=1;k<p;k++)
			{
				sum += d[p][k]*d[k][j];
			}
			d[p][j] = a[p][j] - sum;
        }
		for(i=p+1;i<=n;i++)
		{
			sum = 0;
			for(r=1;r<p;r++)
			{						
				sum += d[i][r]*d[r][p];
			}
			d[i][p] = (a[i][p] - sum)/d[p][p];
		}
    }
    
	//Menampilkan Matriks LU
	cout<<endl<<"Matriks LU : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<d[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;

    //Mencari Y, dimana LY=b
    for(i=1;i<=n;i++)
    {                                        //forward subtitution
        faktor = 0;
        for(p=1;p<i;p++)
		{
			faktor+=d[i][p]*z[p];
		}
        z[i]=(b[i]-faktor);
    }

    //Mencari X; UX=Y
    for(i=n;i>0;i--)
    {
		faktor = 0;
        for(p=n;p>i;p--)
		{
			faktor+=d[i][p]*x[p];
		}
		x[i]=(z[i]-faktor)/d[i][i];
    }

    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i=1;i<=n;i++)
        cout<<endl<<"x"<<i<<" = "<<x[i];

    _getch();
    return 0;
}
