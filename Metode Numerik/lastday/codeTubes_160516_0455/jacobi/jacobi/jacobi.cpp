// jacobi.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	cout.precision(8);
	cout.setf(ios::fixed);

	int n, i, j, k;
	double a[10][10], b[10], err[10], x[10], xold[10], c[10], ea, eps;

	cout<<"Jumlah orde matriks A = ";
    cin>>n;

	cout<<"Koefisien Matriks A : ";
    for(i = 1; i <= n; i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(j = 1; j <= n; j++)
		{
            cin>>a[i][j];
		}
    }

	cout<<"\nElemen matriks B = ";
    for(i = 1; i <= n; i++)
    {
		cin>>b[i];
	}

	cout<<"\nTebakan awal x = ";
    for(i = 1; i <= n; i++)
	{
		cin>>x[i];
	}

	//Menampilkan matriks A
	cout<<endl<<"Matriks A : \n";
	for(i = 1; i <= n; i++)
    {
        for(j = 1; j <= n; j++)
		{
			cout<<a[i][j]<<"  ";
		}
		cout<<endl;
    }
    cout<<endl;

	//Menampilkan matriks b
	cout<<"\nMatriks b : \n";
	for (i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}
	cout<<endl;

	//Menampilkan tebakan awal x
	cout<<"\nTebakan awal x : \n";
	for (i = 1; i <= n; i++)
	{
		cout<<"x["<<i<<"] = "<<x[i]<<"\n";
	}

	//Iterasi Jacobi
	k = 1;
	do
	{
		cout<<endl<<"i-"<<k<<"\n";
		for (i = 1; i <= n; i++)
		{
			xold[i] = x[i];
			c[i] = b[i];
			for (j = 1; j <= n; j++)
			{
				if (i != j)
				{
					c[i] = c[i] - a[i][j] * x[j];
				}
			}
		}

		ea = 0;
		for (i = 1; i <= n; i++)
		{
			x[i] = c[i]/a[i][i];
			err[i] = fabs((x[i] - xold[i])/x[i]);
			ea = ea + err[i];
			cout<<"x"<<i<<" = "<<x[i]<<"\terr-"<<i<<" = "<<err[i]<<endl;
		}
		eps = ea/n;
		cout<<"ea = "<<ea<<"\t\teps = "<<eps<<endl;
		k = k + 1;

		if (ea <= 0.000001)
		{
			break;
		}
	} while (k <= 20);

    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i = 1; i <= n; i++)
	{
        cout<<endl<<"x"<<i<<" = "<<x[i];
	}
	
    return 0;
}
