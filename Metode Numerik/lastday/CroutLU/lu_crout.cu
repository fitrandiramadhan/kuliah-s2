//file		:	crout.cu
//author	:	steaKK

#include <iostream>

using namespace std;

int get_index(int i, int j, int orde) {
	return i*orde+j;
}

__device__ int cuda_get_index(int i, int j, int orde) {
	return i*orde+j;
}

__global__ void cuda_bisection(double data, float* xmin, float* xmax, float* es, int* maxit) {
	float x_lo = xmin[0];
	float x_hi = xmax[0];
	float croot = (x_lo+x_hi)/2;

	if(xmin[0]*xmax[0]<0) {
		float ea = 99999.0;
		int iter = 0;

		while(ea>es[0] && iter<=maxit[0]) {
			croot = (x_lo+x_hi)/2;
			if(!cuda_is_positive(x_lo*croot)) {
				ea = cuda_get_relativeerror(x_lo,croot);
				x_hi = croot;
			}
			else {
				ea = cuda_get_relativeerror(croot,x_hi);
				x_lo = croot;
			}
			iter++;
		}
	}
	float_result[0] = croot;
}

int main() {
	int orde;

	cout << "Jumlah orde matriks (A) = ";
    cin >> orde;
	cout << endl;

	double* a = new double[orde*orde];
	for(int i=0;i<orde*orde;i++) a[i] = 0;
	double* d = new double[orde*orde];
	for(int i=0;i<orde*orde;i++) d[i] = 0;
	double* b = new double[orde];
	for(int i=0;i<orde;i++) b[i] = 0;
	double* z = new double[orde];
	for(int i=0;i<orde;i++) z[i] = 0;
	double* x = new double[orde];
	for(int i=0;i<orde;i++) x[i] = 0;

	cout << "Koefisien Matriks : " << endl;
    for(int i=0;i<orde;i++) {
        cout << "Baris ke-" << i << "  ";
        for(int j=0;j<orde;j++) cin >> a[get_index(i,j,orde)];
    }
	cout << endl;

	cout << "Elemen matriks B = ";
    for(int i=0;i<orde;i++) cin >> b[i];
	cout << endl;

	//Matriks A
	cout << "Matriks A :" << endl;
	for(int i=0;i<orde;i++) {
        for(int j=0;j<orde;j++) cout << a[get_index(i,j,orde)] << "  ";
        cout << endl;
    }
    cout << endl << endl;

	cout << " Matriks b :" << endl;
	for (int i=0;i<orde;i++) cout << b[i] << endl;

	//Dekomposisi LU metode Crout
    for(int i=0;i<orde;i++) {
        for(int j=i;j<i;j++) {
			double sum = 0;
			for(int k=0;k<i;k++) {
				sum += d[get_index(i,k,orde)]*d[get_index(k,j,orde)];
			}
			d[get_index(i,j,orde)] = a[get_index(i,j,orde)] - sum;
        }
		for(int j=i+1;j<orde;j++) {
			double sum = 0;
			for(int k=0;k<i;k++) {
				sum += d[get_index(j,k,orde)]*d[get_index(k,i,orde)];
			}
			d[get_index(j,i,orde)] = (a[get_index(j,i,orde)] - sum)/d[get_index(i,i,orde)];
		}
    }

	//Menampilkan Matriks LU
	cout << endl << "Matriks LU : " << endl;
	for(int i=0;i<orde;i++) {
        for(int j=0;j<orde;j++) cout << d[get_index(i,j,orde)] << "  ";
        cout<<endl;
    }
    cout << endl;

	//Mencari Y, dimana LY=b
    for(int i=0;i<orde;i++) {                                        //forward subtitution
        double faktor = 0;
        for(int j=0;j<i;j++) faktor += d[get_index(i,j,orde)]*z[j];
        z[i] = b[i] - faktor;
    }

	//Mencari X; UX=Y
    for(int i=orde-1;i>=0;i--) {
		double faktor = 0;
        for(int j=orde-1;j>i;j--) {
			faktor += d[get_index(i,j,orde)]*x[j];
		}
		x[i]=(z[i]-faktor)/d[get_index(i,i,orde)];
    }

	//Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(int i=0;i<orde;i++) cout << endl << "x" << i << " = " << x[i] << endl;

	return 0;
}
