// croutLU.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <cuda.h>
#include <stdio.h>

using namespace std;

int get_index(int i, int j, int orde) {
	return i*orde+j;
}

__device__ int cuda_get_index(int i, int j, int orde) {
	return i*orde+j;
}

__global__ void cuda_lu_decomp(double* a, double* b, double* x, int* orde) {
	printf("andiganteng1\n");

	int n = orde[0];

	double* d = new double[100];
	for(int i=0;i<100;i++) d[i] = 0;
	double* z = new double[10];
	for(int i=0;i<10;i++) z[i] = 0;

	printf("andiganteng2\n");

	//Dekomposisi LU metode Crout
    for(int p=1;p<=n;p++) {
        for(int j=p;j<=n;j++) {
			double sum = 0;
			for(int k=1;k<p;k++) {
				sum += d[cuda_get_index(p,k,n)]*d[cuda_get_index(k,j,n)];
			}
			d[cuda_get_index(p,j,n)] = a[cuda_get_index(p,j,n)] - sum;
        }
		for(int i=p+1;i<=n;i++) {
			double sum = 0;
			for(int r=1;r<p;r++) {
				sum += d[cuda_get_index(i,r,n)]*d[cuda_get_index(r,p,n)];
			}
			d[cuda_get_index(i,p,n)] = (a[cuda_get_index(i,p,n)] - sum)/d[cuda_get_index(p,p,n)];
		}
    }

	printf("andiganteng3\n");

    //Mencari Y, dimana LY=b
    for(int i=1;i<=n;i++) {                                        //forward subtitution
        double faktor = 0;
        for(int p=1;p<i;p++) {
			faktor+=d[cuda_get_index(i,p,n)]*z[p];
		}
        z[i]=(b[i]-faktor);
    }

	printf("andiganteng4\n");

    //Mencari X; UX=Y
    for(int i=n;i>0;i--) {
		double faktor = 0;
        for(int p=n;p>i;p--) {
			faktor+=d[cuda_get_index(i,p,n)]*x[p];
		}
		x[i]=(z[i]-faktor)/d[cuda_get_index(i,i,n)];
    }

	printf("andiganteng5\n");

	delete[] d;
	delete[] z;

	printf("andiganteng6\n");
}

int main() {

	int n;
	double* a = new double[100];
	for(int i=0;i<100;i++) a[i] = 0;
	double* b = new double[10];
	for(int i=0;i<100;i++) a[i] = 0;
	double* x = new double[10];
	for(int i=0;i<10;i++) x[i] = 0;

    cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(int i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(int j=1;j<=n;j++)
            cin>>a[get_index(i,j,n)];
    }
    cout<<"\nElemen matriks B = ";
    for(int i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[get_index(i,j,n)]<<"  ";
        cout<<endl;
    }
    cout<<endl;

	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

	double* d_a;
	double* d_b;
	double* d_x;
	int* d_n;

	cudaMalloc(&d_a, 100*sizeof(double));
	cudaMalloc(&d_b, 10*sizeof(double));
	cudaMalloc(&d_x, 10*sizeof(double));
	cudaMalloc(&d_n, sizeof(double));

	cudaMemcpy(d_a, a, 100*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_n, &n, sizeof(double), cudaMemcpyHostToDevice);

	cuda_lu_decomp<<<1,1>>>(d_a,d_b,d_x,d_n);

	cout << "ANDIGANTENG1" << endl;

	cudaMemcpy(x, d_x, 10*sizeof(double), cudaMemcpyDeviceToHost);

	cout << "ANDIGANTENG2" << endl;

    // Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(int i=1;i<=n;i++) {
		cout<<endl<<"x"<<i<<" = "<<x[i] << endl;
	}
	cout << x[0] << endl;

	//bersih2
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_x);
	cudaFree(d_n);
	delete[] a;
	delete[] b;
	delete[] x;

    return 0;
}
