// secant.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<conio.h>
using namespace std;

double f(double x)
{
	double y;
	y=x*sqrt(abs(2.1-0.5*x))/(1.0-x)*sqrt(abs(1.1-0.5*x))-3.69; //soal B.1.a
	//y=tan(x)-x+1; //soal B.1.b
	//y=0.5*exp(x/3)-sin(x); //soal B.1.c fungsi ini tidak (pernah) memotong sumbu x, artinya tidak mempunyai akar
	return(y);
}

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);
	double x0,x1,x2,err,er,eps;
	int maxiter;
	
	cout<<"Nilai awal 1 (X0) = ";
	cin>>x0;
	cout<<"Nilai awal 2 (X1) = ";
	cin>>x1;
	cout<<"Toleransi error (e) = ";
	cin>>err;
	cout<<"Maximal iterasi = ";
	cin>>maxiter;
		
	//int i = 0;
	//x2 = x1-f(x1)*(x1-x0)/(f(x1)-f(x0));
	for (int i = 0; i < maxiter; i++)
	{
		x2 = x1-f(x1)*(x1-x0)/(f(x1)-f(x0));
		
		er = fabs(x2-x1);
		eps = 2*er/(abs(x2)+err);
		
		cout<<"\n";
		cout<<"i = "<< i <<"\t";
		cout<<"x0 = "<< x0 <<"\t";
		cout<<"f(x0) = "<< f(x0) <<"\t";
		cout<<"x1 = "<< x1 <<"\t";
		cout<<"f(x1) = "<< f(x1) <<"\t";
		cout<<"x2 = "<< x2 <<"\t";
		cout<<"f(x2) = "<< f(x2) <<"\t";

		x0 = x1;
		x1 = x2;
		//x3 = x2-f(x2)*(x2-x1)/(f(x2)-f(x1));

		
		
		//i = i+1;

		if ((er <= err) | (eps <= err)) 
		{
			break;
		}
	}
	cout<<"\n\n";
	cout<<"Akar = "<<x1<<"\n";
	_getch();
	return 0;
}