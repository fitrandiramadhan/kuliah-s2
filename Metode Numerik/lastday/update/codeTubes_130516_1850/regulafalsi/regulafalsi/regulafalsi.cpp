// regulafalsi.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<conio.h>
using namespace std;

double f(double x)
{
	double y;
	//y=x*sqrt(2.1-0.5*x)/(1.0-x)*sqrt(1.1-0.5*x)-3.69; //soal B.1.a
	//y=tan(x)-x+1; //soal B.1.b
	y=0.5*exp(x/3)-sin(x); //soal B.1.c fungsi ini tidak (pernah) memotong sumbu x, artinya tidak mempunyai akar
	return(y);
}

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);
	double xl,xu,xr,err,er;
	int maxiter;

	cout<<"Batas bawah (xl) = ";
	cin>>xl;
	cout<<"Batas atas (xu) = ";
	cin>>xu;
	cout<<"Toleransi error (e) = ";
	cin>>err;
	cout<<"Maximal iterasi = ";
	cin>>maxiter;
	
	int i = 0;
	cout<<"\n";
	if(f(xl)*f(xu)<0)
	{
		xr = xu - (f(xu)*(xl-xu)/(f(xl)-f(xu)));
		while (i <= maxiter)
		{
			cout<<"\n";
			cout<<"i = "<< i <<"\t";
			cout<<"xl = "<< xl <<"\t";
			cout<<"xu = "<< xu <<"\t";
			cout<<"xr = "<< xr <<"\t";
			cout<<"f(xr) = "<< f(xr);
			
			er = abs((xr - xl)/xr)*100; 
			if(er <= err)
			{
				break;
			}

			i=i+1;
			if(f(xl)*f(xr)<0)
				xu = xr;
			else
				xl = xr;
			xr = xu - (f(xu)*(xl-xu)/(f(xl)-f(xu)));
		}
		cout<<"\n\n";
		cout<<"Akar = "<< xr <<"\n";
	}
	else
		cout<<"Tidak ada akar"<<"\n";
	_getch();
	return 0;
}

