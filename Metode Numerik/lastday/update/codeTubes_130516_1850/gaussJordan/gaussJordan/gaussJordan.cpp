// gaussJordan.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<conio.h>

using namespace std;

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);

    int n,i,k,j,p;
    double a[10][10],temp,faktor,b[10],x[10]={0};

	cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(j=1;j<=n;j++)
            cin>>a[i][j];
    }
    cout<<"\nElemen matriks B = "<<endl;
    for(i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;
	
	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}
    
	//Gauss Jordan
    for(k=1;k<=n;k++)
    {
		faktor = a[k][k];
        for(j=1;j<=n;j++)
        {
			a[k][j] = a[k][j]/faktor;
        }
		b[k] = b[k]/faktor;

		for(i=1;i<=n;i++)
		{
			if (i != k) 
			{
				temp = a[i][k];
				for (j=1;j<=n;j++)
				{
					a[i][j] = a[i][j]-temp*a[k][j];
				}
				b[i] = b[i]-temp*b[k];
			}
		}
		
    }
    
	//Menampilkan Matriks
	cout<<endl<<"Matriks A' : \n";
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<a[i][j]<<"  ";
        cout<<endl;
    }

	cout<<"\nMatriks b' : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

    //Mencari X; IX=b'
    for(i=1;i<=n;i++)
    {
        x[i] = b[i];
    }
    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i=1;i<=n;i++)
        cout<<endl<<"x"<<i<<" = "<<x[i];

    getch();
    return 0;
}