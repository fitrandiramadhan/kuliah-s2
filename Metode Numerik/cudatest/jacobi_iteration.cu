//file		:	jacobi_iteration.cu
//author	:	steaKK

#include <iostream>
#include <cuda.h>
#include <stdio.h>

using namespace std;

__device__ float cuda_get_data(int i, int j, float* m, int _width) {
	return m[i*_width+j];
}

__device__ void cuda_set_data(int i, int j, float* m, int _width, float x) {
	m[i*_width+j] = x ;
}

__device__ float* cuda_jacobi_subroutine(float* result, float* A, float* x, float* c, int _height, int _width) {
	for(int i=0;i<_height;i++) {
		float mx = 0;
		for(int j=0;j<_width;j++) mx += cuda_get_data(i,j,A,_width)*x[j];
		result[i] = mx+c[i];
	}
	return result;
}

__global__ void cuda_jacobi_iteration(float* A, float* v, float* initial_guess, int* maxit, int* height, int* width) {
	printf("height = %d\n", height[0]);
	printf("width = %d\n", width[0]);
	//init
	float* temp1;
	float* temp3;
	for(int i=0;i<height[0];i++) {
		for(int j=0;j<width[0];j++) {
			if(i==j) cuda_set_data(i,j,temp1,width[0],0.0);
			else cuda_set_data(i,j,temp1,width[0],-1*cuda_get_data(i,j,A,width[0])/cuda_get_data(i,i,A,width[0]));
		}
	}
	for(int i=0;i<height[0];i++) temp3[i] = v[i]/cuda_get_data(i,i,A,width[0]);
	//iteration
	for(int i=0;i<maxit[0];i++) {
		cuda_jacobi_subroutine(A,initial_guess,temp3,height[0],width[0]);
	}
	printf("%d\n", height[0]);
	for(int i=0;i<height[0];i++) printf("%f\n", initial_guess[i]);
}

int main() {
	int height = 3;
	int width = 3;
	float* m = new float[height*width];
	float* v = new float[height];
	float* initial_guess = new float[height];
	int maxit = 10;

	// cout << "andiganteng1" << endl;

	m[0] = 1.0; m[1] = 1.0; m[2] = 1.0;
	m[3] = 5.0; m[4] = 7.0; m[5] = 1.0;
	m[6] = 2.0; m[7] = 3.0; m[8] = -4.0;

	v[0] = 7.0;
	v[1] = -4.0;
	v[2] = 9.0;

	initial_guess[0] = 0;
	initial_guess[1] = 0;
	initial_guess[2] = 0;

	// cout << "andiganteng2" << endl;

	int* d_height;
	int* d_width;
	float* d_m;
	float* d_v;
	float* d_initial_guess;
	int* d_maxit;

	// cout << "andiganteng3" << endl;

	cudaMalloc(&d_height, sizeof(int));
	cudaMalloc(&d_width, sizeof(int));
	cudaMalloc(&d_m, height*width*sizeof(float));
	cudaMalloc(&d_v, height*sizeof(float));
	cudaMalloc(&d_initial_guess, height*sizeof(float));
	cudaMalloc(&d_maxit, sizeof(int));

	// cout << "andiganteng4" << endl;

	cudaMemcpy(d_height, &height, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_width, &width, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_m, m, height*width*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_v, v, height*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_initial_guess, initial_guess, height*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_maxit, &maxit, sizeof(int), cudaMemcpyHostToDevice);

	// cout << "andiganteng5" << endl;

	cuda_jacobi_iteration<<<1,1>>>(d_m,d_v,d_initial_guess,d_maxit,d_height,d_width);

	// cout << "andiganteng6" << endl;

	cudaMemcpy(initial_guess, d_initial_guess, height*sizeof(float), cudaMemcpyDeviceToHost);

	// cout << "andiganteng7" << endl;

	cout << "ans = " << initial_guess[0] << endl;
	cout << "ans = " << initial_guess[1] << endl;
	cout << "ans = " << initial_guess[2] << endl;

	// cout << "andiganteng8" << endl;

	cudaFree(d_height);
	cudaFree(d_width);
	cudaFree(d_m);
	cudaFree(d_v);
	cudaFree(d_initial_guess);
	cudaFree(d_maxit);

	// cout << "andiganteng9" << endl;

	return 0;
}
