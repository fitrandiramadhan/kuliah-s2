//file		:	cuda_test.cu
//author	:	steaKK

#include <iostream>
#include <cuda.h>

using namespace std;

__global__ void add_int_cuda(int* a, int* b, int* c) {
	c[0] = a[0] + b[0];
}

int main() {
	int a = 5;
	int b = 9;
	int c = 0;

	int* d_a;
	int* d_b;
	int* d_c;

	cudaMalloc(&d_a, sizeof(int));
	cudaMalloc(&d_b, sizeof(int));
	cudaMalloc(&d_c, sizeof(int));

	cudaMemcpy(d_a, &a, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, &b, sizeof(int), cudaMemcpyHostToDevice);

	add_int_cuda<<<1,1>>>(d_a,d_b,d_c);

	cudaMemcpy(&c, d_c, sizeof(int), cudaMemcpyDeviceToHost);

	cout << "ans = " << c << endl;

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);

	return 0;
}
