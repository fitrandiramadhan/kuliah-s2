//file		:	main.hpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;

int main() {
	vector<float> result;
	float xmin = -10.0;
	float xmax = 10.0;
	int ns = 50;
	float es = 0.5;
	int maxit = 50;
	float xr = 5.0;
	float perturbation = 0.00001;

	// Matrix m(3,3);
	vector<float> v;
	// // init
	// cout << "andiganteng 1" << endl;
	// m.set_data(0,0,1.0); m.set_data(0,1,1.0); m.set_data(0,2,1.0);
	// m.set_data(1,0,5.0); m.set_data(1,1,7.0); m.set_data(1,2,1.0);
	// m.set_data(2,0,2.0); m.set_data(2,1,3.0); m.set_data(2,2,-4.0);
	// m.print();
	// v.push_back(10.0); v.push_back(21.0); v.push_back(-2.0);
	// for(int i=0;i<v.size();i++) cout << v[i] << endl;

	Matrix m(3,4);
	// init
	cout << "andiganteng 1" << endl;
	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
	m.set_data(1,0,0.26); m.set_data(1,1,0.32); m.set_data(1,2,0.18); m.set_data(1,3,0.24); m.set_data(1,4,1.00);
	m.set_data(2,0,0.61); m.set_data(2,1,0.22); m.set_data(2,2,0.20); m.set_data(2,3,0.31); m.set_data(2,4,1.34);
	m.set_data(3,0,0.40); m.set_data(3,1,0.34); m.set_data(3,2,0.36); m.set_data(3,3,0.17); m.set_data(3,4,1.27);
	m.print();

// 	Matrix m1(9,10);
// 	m.set_data(0,0,0.7071); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
// 	m.set_data(0,0,0.31); m.set_data(0,1,0.14); m.set_data(0,2,0.30); m.set_data(0,3,0.27); m.set_data(0,4,1.02);
//
//
// 	0.7071	0	0 	-1	-0.8660	0	0	0	0		0
// 0.7071 0	1	0	0.5		0	0	0	0	          -1000
// 0	1	0	0	0		-1	0	0	0		0
// 0	0	-1	0	0		0	0	0	0		0
// 0	0	0	0	0		0	1	0     0.7071      x  =	500
// 0	0	0	1	0		0	0	0    -0.7071		0
// 0	0	0	0	0.8660		1	0	-1	0		0
// 0	0	0	0	-0.5		0	-1	0	0		-500
// 0	0	0	0	0		0	0	0     0.7071		0

	//init jacobi_iteration
	// Matrix m(3,3);
	// vector<float> v;
	vector<float> initial_guess;
	// m.set_data(0,0,10.0); m.set_data(0,1,2.0); m.set_data(0,2,-1.0);
	// m.set_data(1,0,1.0); m.set_data(1,1,8.0); m.set_data(1,2,3.0);
	// m.set_data(2,0,-2.0); m.set_data(2,1,-1.0); m.set_data(2,2,10.0);
	v.push_back(1.02); v.push_back(1.00); v.push_back(1.34); v.push_back(1.27); ;
	initial_guess.push_back(0.0); initial_guess.push_back(0.0); initial_guess.push_back(0.0); initial_guess.push_back(0.0);

	//TESTING
	// result = MetodeNumerik::inc_search(xmin,xmax,ns);
	// result = MetodeNumerik::bisection(xmin,xmax,es,maxit);
	// result = MetodeNumerik::false_position(xmin,xmax,es,maxit);
	// result = MetodeNumerik::newton_raphson(xr,es,maxit);
	// result = MetodeNumerik::secant(xr,es,perturbation,maxit);
	// result = MetodeNumerik::gauss_naive(m,v);
	result = MetodeNumerik::gauss_naive2(m);
	cout << "---gauss_naive---" << endl;
	for(int i=0;i<result.size();i++) {
		cout << result[i] << endl;
	}
	cout << "-----------------" << endl;
	result = MetodeNumerik::gauss_jordan(m);
	cout << "---gauss_jordan---" << endl;
	for(int i=0;i<result.size();i++) {
		cout << result[i] << endl;
	}
	cout << "-----------------" << endl;
	result = MetodeNumerik::jacobi_iteration(m,v,initial_guess,maxit);
	cout << "---iterasi_jacobi---" << endl;
	for(int i=0;i<result.size();i++) {
		cout << result[i] << endl;
	}
	cout << "-----------------" << endl;

	return 0;
}
