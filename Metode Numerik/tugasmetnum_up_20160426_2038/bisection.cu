//file		:	bisection.cu
//author	:	steaKK

#include <iostream>
#include <cuda.h>

using namespace std;

__device__ bool cuda_is_positive(float x) {
	if(x<0) return false;
	else return true;
}

__device__ float cuda_get_relativeerror(float x_old, float x_new) {
	float result = (x_new-x_old)/x_new*100;
	if(result<0) result = -result;
	return result;
}

__global__ void cuda_bisection(float* float_result, float* xmin, float* xmax, float* es, int* maxit) {
	float x_lo = xmin[0];
	float x_hi = xmax[0];
	float croot = (x_lo+x_hi)/2;

	if(xmin[0]*xmax[0]<0) {
		float ea = 99999.0;
		int iter = 0;

		while(ea>es[0] && iter<=maxit[0]) {
			croot = (x_lo+x_hi)/2;
			if(!cuda_is_positive(x_lo*croot)) {
				ea = cuda_get_relativeerror(x_lo,croot);
				x_hi = croot;
			}
			else {
				ea = cuda_get_relativeerror(croot,x_hi);
				x_lo = croot;
			}
			iter++;
		}
	}
	float_result[0] = croot;
}

int main() {
	float xmin = -10.0;
	float xmax = 10.0;
	float es = 0.5;
	int maxit = 50;
	float result = 0;

	float* d_xmin;
	float* d_xmax;
	float* d_es;
	int* d_maxit;
	float* d_float_result;

	cudaMalloc(&d_xmin, sizeof(float));
	cudaMalloc(&d_xmax, sizeof(float));
	cudaMalloc(&d_es, sizeof(float));
	cudaMalloc(&d_maxit, sizeof(int));
	cudaMalloc(&d_float_result, sizeof(float));

	cudaMemcpy(d_xmin, &xmin, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_xmax, &xmax, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_es, &es, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_maxit, &maxit, sizeof(int), cudaMemcpyHostToDevice);

	cuda_bisection<<<1,1>>>(d_float_result,d_xmin,d_xmax,d_es,d_maxit);

	cudaMemcpy(&result, d_float_result, sizeof(float), cudaMemcpyDeviceToHost);

	cout << "ans = " << result << endl;

	cudaFree(d_xmin);
	cudaFree(d_xmax);
	cudaFree(d_es);
	cudaFree(d_maxit);
	cudaFree(d_float_result);

	return 0;
}
