//file		:	MetodeNumerik.hpp
//author	:	steaKK

#ifndef MetodeNumerik_HPP
#define MetodeNumerik_HPP

#include <iostream>
#include <vector>
#include <limits>

#include "Matrix.hpp"

using namespace std;

class MetodeNumerik {
public:
	static float f0function(float);
	static float f1function(float);

	static bool is_positive(float);
	static float get_relativeerror(float,float);
	static Matrix swap_row(int,int,Matrix);
	static int get_swappable(int,Matrix);
	static Matrix pivoting(int,Matrix);
	static vector<float> jacobi_subroutine(Matrix,vector<float>,vector<float>);

	static vector<float> inc_search(float,float,int);
	static vector<float> bisection(float,float,float,int);
	static vector<float> false_position(float,float,float,int);

	static vector<float> newton_raphson(float,float,int);
	static vector<float> secant(float,float,float,int);

	static vector<float> gauss_naive(Matrix,vector<float>);
	static vector<float> gauss_naive2(Matrix);
	static vector<float> gauss_pivot(Matrix);
	static vector<float> gauss_jordan(Matrix);

	static vector<float> lu_decomposition(Matrix);

	static vector<float> jacobi_iteration(Matrix,vector<float>,vector<float>,int);

private:

};

#endif
