//file		:	MetodeNumerik.cpp
//author	:	steaKK

#include "MetodeNumerik.hpp"

using namespace std;

bool MetodeNumerik::is_positive(float x) {
	if(x<0) return false;
	else return true;
}

float MetodeNumerik::f0function(float x) {
	return 3*x*x*x + 2*x*x - x + 1;
}

float MetodeNumerik::f1function(float x) {
	return 6*x*x + 4*x - 1;
}

float MetodeNumerik::get_relativeerror(float x_old, float x_new) {
	float result = (x_new-x_old)/x_new*100;
	if(is_positive(result)) return result;
	else return -(result);
}

Matrix MetodeNumerik::swap_row(int rowa, int rowb, Matrix m) {
	Matrix result(m);
	for(int i=0;i<m.get_width();i++) result.set_data(rowa,i,m.get_data(rowb,i));
	for(int i=0;i<m.get_width();i++) result.set_data(rowb,i,m.get_data(rowa,i));
	return result;
}

int MetodeNumerik::get_swappable(int row, Matrix m) {
	for(int i=row+1;i<m.get_height();i++) {
		if(m.get_data(i,row)!=0 && m.get_data(row,i)!=0) return i;
	}
	return -1;
}

Matrix MetodeNumerik::pivoting(int rank, Matrix m) {
	Matrix result(m);
	for(int i=rank;i<m.get_height();i++) if(result.get_data(i,i)==0) result = swap_row(i,get_swappable(i,result),result);
	return result;
}


//Incremental Search
//fu = function f(x)
//xmin = lowest x space to be tested
//xmax = highest x space to be tested
//ns = number of interval in the x space tested
//return value = root prediction
vector<float> MetodeNumerik::inc_search(float xmin, float xmax, int ns) {
	vector<float> linspace;
	float delta = (xmax-xmin)/ns;
	linspace.push_back(xmin);
	vector<float> space_y;
	for(int i=0;i<ns;i++) {
		linspace.push_back(xmin+(i*delta));
		space_y.push_back(f0function(linspace[i]));
	}
	bool sign = is_positive(space_y[0]);
	vector<float> result;
	for(int i=1;i<space_y.size();i++) {
		if(sign!=is_positive(space_y[i])) {
			sign = is_positive(space_y[i]);
			result.push_back((space_y[i-1]+space_y[i])/2);
		}
	}
	return result;
}

vector<float> MetodeNumerik::bisection(float xmin, float xmax, float es, int maxit) {
	vector<float> result;
	if(!is_positive(xmin*xmax)) {
		float ea = std::numeric_limits<float>::infinity();
		int iter = 0;
		float x_lo = xmin; float x_hi = xmax;
		float croot = (x_lo+x_hi)/2;
		while(ea>es && iter<=maxit) {
			croot = (x_lo+x_hi)/2;
			if(!is_positive(x_lo*croot)) {
				ea = get_relativeerror(x_lo,croot);
				x_hi = croot;
			}
			else {
				ea = get_relativeerror(croot,x_hi);
				x_lo = croot;
			}
			iter++;
		}
		result.push_back(croot);
	}
	// else cout << "pilih yg laen aja intervalnya" << endl;
	return result;
}

vector<float> MetodeNumerik::false_position(float xmin, float xmax, float es, int maxit) {
	vector<float> result;
	if(!is_positive(xmin*xmax)) {
		float ea = std::numeric_limits<float>::infinity();
		int iter = 0;
		float x_lo = xmin; float x_hi = xmax;
		float croot = x_hi - ((f0function(x_hi)*(x_lo-x_hi))/(f0function(x_lo)-f0function(x_hi)));
		while(ea>es && iter<=maxit) {
			croot = x_hi - ((f0function(x_hi)*(x_lo-x_hi))/(f0function(x_lo)-f0function(x_hi)));
			if(!is_positive(x_lo*croot)) {
				ea = get_relativeerror(x_lo,croot);
				x_hi = croot;
			}
			else {
				ea = get_relativeerror(croot,x_hi);
				x_lo = croot;
			}
			iter++;
		}
		result.push_back(croot);
	}
	// else cout << "pilih yg laen aja intervalnya" << endl;
	return result;
}


vector<float> MetodeNumerik::newton_raphson(float xr, float es, int maxit) {
	vector<float> result;
	float ea = std::numeric_limits<float>::infinity();
	float x_old = xr;
	int iter = 0;
	while(ea>es && iter<=maxit) {
		float x_new = x_old - (f0function(x_old)/f1function(x_old));
		ea = get_relativeerror(x_old,x_new);
		x_old = x_new;
		iter++;
	}
	result.push_back(x_old);
	return result;
}

vector<float> MetodeNumerik::secant(float xr, float es, float perturbation, int maxit) {
	vector<float> result;
	float ea = std::numeric_limits<float>::infinity();
	float x_old = xr;
	int iter = 0;
	while(ea>es && iter<=maxit) {
		float x_new = x_old - (perturbation*x_old*f0function(x_old)/(f0function(x_old+perturbation*x_old)-f0function(x_old)));
		ea = get_relativeerror(x_old,x_new);
		x_old = x_new;
		iter++;
	}
	result.push_back(x_old);
	return result;
}

// Matrix MetodeNumerik::gauss_naive(Matrix A, Matrix b) {
// 	//forward elimination
// 	Matrix temp(A);
// 	for(int i=0;i<A.height();i++) {
// 		for(int j=i+1;<A.height();j++) {
// 			for(int k=0;k<j;k++) temp.set_data(j,k,0);
// 			for(int k=j;k<A.width();k++) {
// 				A.set_data(j,k,(A.get_data(j,k)-(A.get_data(i,k)*A.get_data(j,i)/A.get_data(i,i))));
// 			}
// 		}
// 	}
// }

vector<float> MetodeNumerik::gauss_naive(Matrix A, vector<float> b) {
	//forward elimination
	Matrix temp(A);
	vector<float> tempb = b;
	for(int k=0;k<temp.get_height()-1;k++) {
		for(int i=k+1;i<temp.get_height();i++) {
			tempb[i] = tempb[i] - tempb[k]*temp.get_data(i,k)/temp.get_data(k,k);
			for(int j=k+1;j<temp.get_width();j++) {
				// cout << "[i][j] = " << temp.get_data(i,j) << endl;
				// cout << "[k][j] = " << temp.get_data(k,j) << endl;
				// cout << "[i][k] = " << temp.get_data(i,k) << endl;
				// cout << "[k][i] = " << temp.get_data(k,i) << endl;
				// cout << temp.get_data(k,j) << " * " << temp.get_data(i,k) << " / " << temp.get_data(k,k) << " = " << temp.get_data(k,j)*temp.get_data(i,k)/temp.get_data(k,k) << endl;
				// cout << "[i][j] baru = " << temp.get_data(i,j)-(temp.get_data(k,j)*temp.get_data(i,k)/temp.get_data(k,k)) << endl;
				temp.set_data(i,j,(temp.get_data(i,j)-(temp.get_data(k,j)*temp.get_data(i,k)/temp.get_data(k,k))));
				// cout << "i=" << i << " j=" << j << " data =" << temp.get_data(i,j) << endl;
			}
			for(int j=0;j<k+1;j++) temp.set_data(i,j,0);
		}
		// cout << "------------------------" << endl;
		// cout << "abis di gauss ke k-" << k << endl;
		// temp.print();
		// cout << "------- vector -----------" << endl;
		// for(int i=0;i<tempb.size();i++) {
		// 	cout << tempb[i] << endl;
		// }
		// cout << "-----------" << endl;
	}

	//backward substitution
	vector<float> result;
	for(int i=0;i<temp.get_width();i++) result.push_back(0);
	for(int i=temp.get_height()-1;i>=0;i--) {
		float sum = 0;
		for(int j=temp.get_width()-1;j>i;j--) {
			sum += result[j]*temp.get_data(i,j);
		}
		result[i] = (tempb[i]-sum)/temp.get_data(i,i);
	}

	return result;
}

vector<float> MetodeNumerik::gauss_naive2(Matrix A) {
	//forward elimination
	Matrix temp(A);
	for(int k=0;k<temp.get_height()-1;k++) {
		for(int i=k+1;i<temp.get_height();i++) {
			for(int j=k+1;j<temp.get_width()-1;j++) {
				temp.set_data(i,j,(temp.get_data(i,j)-(temp.get_data(k,j)*temp.get_data(i,k)/temp.get_data(k,k))));
			}
			for(int j=0;j<k+1;j++) temp.set_data(i,j,0);
		}
	}

	//backward substitution
	vector<float> result;
	for(int i=0;i<temp.get_width()-1;i++) result.push_back(0);
	for(int i=temp.get_height()-1;i>=0;i--) {
		float sum = 0;
		for(int j=temp.get_width()-2;j>i;j--) {
			sum += result[j]*temp.get_data(i,j);
		}
		result[i] = (temp.get_data(i,temp.get_width()-1)-sum)/temp.get_data(i,i);
	}

	return result;
}

vector<float> MetodeNumerik::gauss_pivot(Matrix A) {
	//forward elimination
	Matrix temp(pivoting(0,A));
	for(int k=0;k<temp.get_height()-1;k++) {
		temp = pivoting(k,temp);
		for(int i=k+1;i<temp.get_height();i++) {
			for(int j=k+1;j<temp.get_width()-1;j++) {
				temp.set_data(i,j,(temp.get_data(i,j)-(temp.get_data(k,j)*temp.get_data(i,k)/temp.get_data(k,k))));
			}
			for(int j=0;j<k+1;j++) temp.set_data(i,j,0);
		}
	}

	//backward substitution
	vector<float> result;
	for(int i=0;i<temp.get_width()-1;i++) result.push_back(0);
	for(int i=temp.get_height()-1;i>=0;i--) {
		float sum = 0;
		for(int j=temp.get_width()-2;j>i;j--) {
			sum += result[j]*temp.get_data(i,j);
		}
		result[i] = (temp.get_data(i,temp.get_width()-1)-sum)/temp.get_data(i,i);
	}

	return result;
}

vector<float> MetodeNumerik::gauss_jordan(Matrix A) {
	vector<float> result = gauss_pivot(A);

	return result;
}

vector<float> MetodeNumerik::lu_decomposition(Matrix A) {
	//init LU Matrix
	// Matrix L(A.get_height(),A.get_width()-1);
	// Matrix U(A.get_height(),A.get_width()-1);
	// for(int i=0;i<L.get_height();i++) {
	// 	for(int j=0;j<L.get_width();j++) {
	// 		if(j<i) {
	// 			L.set_data(i,j,1);
	// 			U.set_data(i,j,0);
	// 		}
	// 		else if(j==i) {
	// 			L.set_data(i,j,1);
	// 			U.set_data(i,j,1);
	// 		}
	// 		else {
	// 			L.set_data(i,j,0);
	// 			U.set_data(i,j,1);
	// 		}
	// 	}
	// }
	//
	// //get LU data
	// for(int i=0;i<L.get_height();i++) {
	// 	for(int j=0;j<L.get_width();j++) {
	// 		float c = A.get_data(i,j);
	// 		float sum = 0;
	// 		for(int p=0;p<L.get_height();p++) {
	// 			for(int q=0;q<L.get_width();q++) {
	// 				sum += L.get_data(i,j)*L.get_data(j,i);
	// 			}
	// 		}
	// 		if(j<=i) L.set_data(A.get_data()/sum);
	// 		else U.set_data(A.get_data(i,j)/sum);
	// 	}
	// }


}

vector<float> MetodeNumerik::jacobi_subroutine(Matrix m, vector<float> x, vector<float> c) {
	vector<float> result;
	for(int i=0;i<m.get_height();i++) {
		float mx = 0;
		for(int j=0;j<m.get_width();j++) mx += m.get_data(i,j)*x[j];
		result.push_back(mx+c[i]);
	}
	// cout << "result = " << result.size() << endl;
	return result;
}

vector<float> MetodeNumerik::jacobi_iteration(Matrix m, vector<float> v, vector<float> initial_guess, int maxit) {
	//init
	Matrix temp1(m.get_height(),m.get_width());
	for(int i=0;i<temp1.get_height();i++) {
		for(int j=0;j<temp1.get_width();j++) {
			if(i==j) temp1.set_data(i,j,0.0);
			else temp1.set_data(i,j,-1*m.get_data(i,j)/m.get_data(i,i));
		}
	}
	vector<float> temp2 = initial_guess;
	vector<float> temp3;
	for(int i=0;i<v.size();i++)  temp3.push_back(v[i]/m.get_data(i,i));
	//iteration
	for(int i=0;i<maxit;i++) temp2 = jacobi_subroutine(temp1,temp2,temp3);

	return temp2;
}
