//file		:	gauss_jordan.cu
//author	:	steaKK

#include <iostream>
#include <cuda.h>
#include <stdio.h>

using namespace std;

__device__ float cuda_get_data(int i, int j, float* m, int _width) {
	return m[i*_width+j];
}

__device__ void cuda_set_data(int i, int j, float* m, int _width, float x) {
	m[i*_width+j] = x ;
}

__device__ void cuda_swap_row(int rowa, int rowb, float* m, int _height, int _width) {
	float* temp;
	for(int i=0;i<_width;i++) temp[i] = cuda_get_data(rowa,i,m,_width);

	for(int i=0;i<_width;i++) cuda_set_data(rowa,i,m,_width,cuda_get_data(rowb,i,m,_width));
	for(int i=0;i<_width;i++) cuda_set_data(rowb,i,m,_width,temp[i]);
}

__device__ int cuda_get_swappable(int row, float* m, int _height, int _width) {
	for(int i=row+1;i<_height;i++) {
		if(cuda_get_data(i,row,m,_width)!=0 && cuda_get_data(row,i,m,_width)!=0) return i;
	}
	return -1;
}

__device__ void cuda_pivoting(int rank, float* m, int _height, int _width) {
	for(int i=rank;i<_height;i++) {
		if(cuda_get_data(i,i,m,_width)==0) {
			cuda_swap_row(i,cuda_get_swappable(i,m,_height,_width),m,_height,_width);
		}
	}
}

__global__ void cuda_gauss_jordan(float* result, float* A, int* height, int* width) {
	//forward elimination
	printf("andiganteng11\n");
	printf("height = %d\n",height[0]);
	printf("width = %d\n",width[0]);
	for(int k=0;k<height[0]-1;k++) {
		cuda_pivoting(k,A,height[0],width[0]);
		for(int i=k+1;i<height[0];i++) {
			for(int j=k+1;j<width[0];j++) {
				cuda_set_data(i,j,A,width[0],(cuda_get_data(i,j,A,width[0])-(cuda_get_data(k,j,A,width[0])*cuda_get_data(i,k,A,width[0])/cuda_get_data(k,k,A,width[0]))));
			}
			for(int j=0;j<k+1;j++) cuda_set_data(i,j,A,width[0],0);
		}
		// for(int i=0;i<height[0];i++) {
		// 	for(int j=0;j<width[0];j++) {
		// 		printf("[%f]", cuda_get_data(i,j,A,width[0]));
		// 	}
		// 	printf("\n");
		// }
		// printf("-----------------\n");
	}

	//backward substitution
	for(int i=0;i<width[0]-1;i++) result[i] = 0;
	for(int i=height[0]-1;i>=0;i--) {
		float sum = 0;
		for(int j=width[0]-2;j>i;j--) {
			sum += result[j]*cuda_get_data(i,j,A,width[0]);
		}
		result[i] = (cuda_get_data(i,width[0]-1,A,width[0])-sum)/cuda_get_data(i,i,A,width[0]);
		printf("hasil x%d = %f\n",i,result[i]);
	}
}

int main() {
	int height = 4;
	int width = 5;
	float* m = new float[height*width];
	float* result = 0;

	// cout << "andiganteng1" << endl;

	m[0] = 0.31; m[1] = 0.14; m[2] = 0.30; m[3] = 0.27; m[4] = 1.02;
	m[5] = 0.26; m[6] = 0.32; m[7] = 0.18; m[8] = 0.24; m[9] = 1.00;
	m[10] = 0.61; m[11] = 0.22; m[8] = 0.20; m[9] = 0.31; m[10] = 1.34;
	m[11] = 0.40; m[12] = 0.34; m[13] = 0.36; m[14] = 0.17; m[15] = 1.27;

	// cout << "andiganteng2" << endl;

	int* d_height;
	int* d_width;
	float* d_m;
	float* d_result;

	// cout << "andiganteng3" << endl;

	cudaMalloc(&d_height, sizeof(int));
	cudaMalloc(&d_width, sizeof(int));
	cudaMalloc(&d_m, height*width*sizeof(float));
	cudaMalloc(&d_result, height*sizeof(float));

	// cout << "andiganteng4" << endl;

	cudaMemcpy(d_height, &height, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_width, &width, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_m, m, height*width*sizeof(float), cudaMemcpyHostToDevice);

	// cout << "andiganteng5" << endl;

	cuda_gauss_jordan<<<1,1>>>(d_result,d_m,d_height,d_width);

	// cout << "andiganteng6" << endl;

	cudaMemcpy(result, d_result, height*sizeof(float), cudaMemcpyDeviceToHost);

	// cout << "andiganteng7" << endl;

	// cout << "ans = " << result[0] << endl;
	// cout << "ans = " << result[1] << endl;
	// cout << "ans = " << result[2] << endl;

	// cout << "andiganteng8" << endl;

	cudaFree(d_height);
	cudaFree(d_width);
	cudaFree(d_m);
	cudaFree(d_result);

	// cout << "andiganteng9" << endl;

	return 0;
}
