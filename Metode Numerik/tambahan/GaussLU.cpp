// GaussLU.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<conio.h>

using namespace std;

int main()
{
	cout.precision(6);
	cout.setf(ios::fixed);

    int n,i,k,j,p;
    double a[10][10],l[10][10]={0},u[10][10]={0},faktor,b[10],z[10]={0},x[10]={0};
    cout<<"Jumlah orde matriks = ";
    cin>>n;
    cout<<"Koefisien Matriks : ";
    for(i=1;i<=n;i++)
    {
        cout<<"\nBaris ke-"<<i<<"  ";
        for(j=1;j<=n;j++)
            cin>>a[i][j];
    }
    cout<<"\nElemen matriks B = "<<endl;
    for(i=1;i<=n;i++)
        cin>>b[i];

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;
	
	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		//b[i] = 1;
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}
    
	//Dekomposisi LU
    for(k=1;k<n;k++)
    {
        for(int i=k+1;i<=n;i++)
        {
			for(p=k;p<=n;p++)
			{
				l[p][p]=1;		//pivot matriks L = 1 
			}
			faktor=a[i][k]/a[k][k];
			for(p=k;p<i;p++)
			{
				l[i][p] = faktor;	//matrik L
			}
			for(p=k;p<=n;p++)
			{
				u[1][p] = a[1][p];
				u[i][p]=a[i][p]-faktor*a[k][p];
				a[i][p]=u[i][p];
			}
        }
		
    }
    
	//Menampilkan Matriks LU
	cout<<endl<<endl<<"Matriks LU :"<<endl;
	cout<<"Matriks L : \n";
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<l[i][j]<<"  ";
        cout<<endl;
    }
    cout<<endl;
	cout<<"Matriks U : \n";
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
            cout<<u[i][j]<<"  ";
        cout<<endl;
    }

    //Mencari Y, dimana LY=b
    for(i=1;i<=n;i++)
    {                                        //forward subtitution
        faktor=0;
        for(p=1;p<i;p++)
        faktor+=l[i][p]*z[p];
        z[i]=(b[i]-faktor)/l[i][i];
    }
    //Mencari X; UX=Y
    for(i=n;i>0;i--)
    {
        faktor=0;
        for(p=n;p>i;p--)
            faktor+=u[i][p]*x[p];
        x[i]=(z[i]-faktor)/u[i][i];
    }
    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(i=1;i<=n;i++)
        cout<<endl<<"x"<<i<<" = "<<x[i];

    getch();
    return 0;
}

