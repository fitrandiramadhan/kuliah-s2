// gauss.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <cmath>
#include <cuda.h>

using namespace std;

int get_index(int i, int j, int orde) {
	return i*orde+j;
}

__device__ int cuda_get_index(int i, int j, int orde) {
	return i*orde+j;
}

__global__ void cuda_gauss(double* a, double* b, double* x, int* orde) {

	int n = orde[0];

	float faktor;

	double* l = new double[100];
	for(int i=0;i<100;i++) l[i] = 0;
	double* u = new double[100];
	for(int i=0;i<100;i++) u[i] = 0;
	double* z = new double[10];
	for(int i=0;i<10;i++) z[i] = 0;

	//Gauss
    for(int k=1;k<n;k++)
    {
        for(int i=k+1;i<=n;i++)
        {
			faktor=a[cuda_get_index(i,k,n)]/a[cuda_get_index(k,k,n)];
			b[i] = b[i]-faktor*b[k];
			for(int p=k;p<=n;p++)
			{
				u[cuda_get_index(1,p,n)] = a[cuda_get_index(1,p,n)];
				u[cuda_get_index(i,p,n)] = a[cuda_get_index(i,p,n)]-faktor*a[cuda_get_index(k,p,n)];
				a[cuda_get_index(i,p,n)] = u[cuda_get_index(i,p,n)];
			}
        }

    }

    //Mencari X; AX=b'
    for(int i=n;i>0;i--)
    {
        faktor=0;
        for(int p=n;p>i;p--)
            faktor+=u[cuda_get_index(i,p,n)]*x[p];
        x[i]=(b[i]-faktor)/u[cuda_get_index(i,i,n)];
    }

	delete[] l;
	delete[] u;
	delete[] z;

}

int main()
{

    int n;

	double* a = new double[100];
	for(int i=0;i<100;i++) a[i] = 0;
	double* b = new double[10];
	for(int i=0;i<10;i++) b[i] = 0;
	double* x = new double[10];
	for(int i=0;i<10;i++) x[i] = 0;

	cout<<"Jumlah orde matriks = ";
    cin>>n;

	for (int i=1; i<=n; i++) {
		for (int j=1; j<=n; j++) {
			a[get_index(i,j,n)] = 1/(double)(i+j-1);
		}
		b[i] = 1;
	}

	//Matriks A
	cout<<endl<<"Matriks A : \n";
	for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
			cout<<a[get_index(i,j,n)]<<"  ";
        cout<<endl;
    }
    cout<<endl;

	cout<<"\nMatriks b : \n";
	for (int i = 1; i <= n; i++)
	{
		cout<<"b["<<i<<"] = "<<b[i]<<"\n";
	}

	double* d_a;
	double* d_b;
	double* d_x;
	int* d_n;

	cudaMalloc(&d_a, 100*sizeof(double));
	cudaMalloc(&d_b, 10*sizeof(double));
	cudaMalloc(&d_x, 10*sizeof(double));
	cudaMalloc(&d_n, sizeof(int));

	cudaMemcpy(d_a, a, 100*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, 10*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

	cuda_gauss<<<1,1>>>(d_a,d_b,d_x,d_n);

	cudaMemcpy(x, d_x, 10*sizeof(double), cudaMemcpyDeviceToHost);

    //Menampilkan hasil
	cout<<endl<<"Nilai x :"<<endl;
    for(int i=1;i<=n;i++) cout<<endl<<"x"<<i<<" = "<<x[i];
	cout << endl;

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_x);
	cudaFree(d_n);
	delete[] a;
	delete[] b;
	delete[] x;

    return 0;
}
