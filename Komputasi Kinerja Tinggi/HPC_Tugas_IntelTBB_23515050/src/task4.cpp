//file		:	fourth_task.cpp
//author	:	steaKK

#include <tbb/tbb.h>
#include <iostream>

using namespace tbb;

const int nFib = 10;

class FibTask: public task {
public:
	const long n;
	long* const sum;
	FibTask( long n_, long* sum_ ) : n(n_), sum(sum_) {

	}

	long SerialFib( long n ) {
		if(n<2) return n;
		else return SerialFib(n-1) + SerialFib(n-2);
	}

	task* execute() { // Overrides virtual function task::execute
		if( n<2 ) {
			*sum = SerialFib(n);
		}
		else {
			long x, y;
			FibTask& a = *new( allocate_child() ) FibTask(n-1,&x);
			FibTask& b = *new( allocate_child() ) FibTask(n-2,&y);
			set_ref_count(3); // 3 = 2 children + 1 for wait
			spawn( b );
			spawn_and_wait_for_all( a );
			*sum = x+y;
		}
	return NULL;
	}
};

long ParallelFib (long n) {
	long sum;
	FibTask& a = *new(task::allocate_root()) FibTask(n,&sum);
	task::spawn_root_and_wait(a);
	return sum;
}

int main() {
	std::cout << ParallelFib(nFib) << std::endl;

	return 0;
}
