//file		:	third_task.cpp
//author	:	steaKK

#include <tbb/tbb.h>
#include <iostream>
#include <cstdlib>

using namespace tbb;

const float FLT_MAX = std::numeric_limits<float>::infinity();
const int GrainSize = 2;
const int DEFAULT_ARRAY_SIZE = 100000;

class MinIndexBody {
public:
	float value_of_min;
	long index_of_min;

	MinIndexBody( MinIndexBody& x, split ) : my_a(x.my_a), value_of_min(FLT_MAX), index_of_min(-1) {

	}

	MinIndexBody ( const float a[] ) : my_a(a), value_of_min(FLT_MAX), index_of_min(-1) {

	}

	void operator() ( const blocked_range<size_t>& r ) {
		const float* a = my_a;
		for( size_t i = r.begin(); i != r.end(); ++i ) {
			float value = a[i];
			if( value<value_of_min ) {
				value_of_min = value;
				index_of_min = i;
			}
		}
	}

	void join( const MinIndexBody& y ) {
		if( y.value_of_min < value_of_min ) {
			value_of_min = y.value_of_min;
			index_of_min = y.index_of_min;
		}
	}

private:
	const float *const my_a;
};

// Find index of smallest element in a[0...n-1]
long SerialMinIndex ( const float a[], size_t n ) {
	float value_of_min = FLT_MAX;
	long index_of_min = -1;
	for( size_t i=0; i<n; ++i ) {
		float value = a[i];
		if( value<value_of_min ) {
			value_of_min = value;
			index_of_min = i;
		}
	}
	return index_of_min;
}

// Find index of smallest element in a[0...n-1]
long ParallelMinIndex ( const float a[], size_t n ) {
	MinIndexBody mib(a);
	parallel_reduce(blocked_range<size_t>(0,n,GrainSize), mib );
	return mib.index_of_min;
}

int main() {
	float array[DEFAULT_ARRAY_SIZE];
	size_t size = sizeof(float);

	srand(time(NULL));
	for(int i=0;i<DEFAULT_ARRAY_SIZE;i++) {
		array[i] = (rand() % 100)/3.0;
	}

	std::cout << ParallelMinIndex(array,size) << std::endl;

	return 0;
}
