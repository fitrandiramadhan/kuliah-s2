//file		:	second_task.cpp
//author	:	steaKK

#include <tbb/tbb.h>
#include <iostream>

using namespace tbb;

const int N = 100000;
const int IdealGrainSize = 2;

void initialize_array(float* array) {
	parallel_for(0,100000,1,[=](int i) {
		array[i] = 1;
	});
}

class ChangeArrayBody {
public:
	ChangeArrayBody (float* a): array(a) {}
	void operator()( const blocked_range <int>& r ) const {
		for (int i = r.begin(); i != r.end(); i++ ) {
			array[i] *= 2;
		}
	}
private:
	float* array;
};

void parallel_change_array(float* array, int M) {
	ChangeArrayBody b(array);
	parallel_for (blocked_range <int>(0, M, IdealGrainSize), b);
}

void print(float* array) {
	for(int i=0;i<N;i++) {
		std::cout << array[i] << std::endl;
	}
}

int main () {
	task_scheduler_init init;
	float A[N];
	initialize_array(A);
	parallel_change_array(A, N);
	print(A);
	return 0;
}
