//file		:	BitString.hpp
//author	:	steaKK

#ifndef BitString_HPP
#define BitString_HPP

#include <iostream>

using namespace std;

class BitString {
public:
	BitString();
	BitString(int);
	BitString(const BitString&);
	BitString& operator=(const BitString&);
	~BitString();

	int get_size();
	void set_size(int);
	bool get_data(int);
	void set_data(int,bool);

	void print();

	void shift_left();
	void shift_right();
	void shift_n_left(int);
	void shift_n_right(int);
	static BitString concat(BitString);
	// static BitString& xor(const BitString&,const BitString&);

private:
	static const int DEFAULT_SIZE = 256;

	int size;
	bool* data;
};

#endif
