//file		:	coba.cpp
//author	:	steaKK

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {
	const int permutation_i_size = 64;

	int* permutation_i = new int[permutation_i_size];

	fstream file;
	file.open("table.dat");
	string s = "";
	while(!file.eof()) {
		file >> s;
		if(s=="<permutation_i>") {
			int i=0;
			file >> s;
			while(s!="</permutation_i>") {
				// cout << "ganteng " << i << " adalah " << s << endl;
				permutation_i[i] = atoi(s.c_str());
				i++;
				file >> s;
			}
			// cout << "done1 jumlah " << i-1 << endl;
		}
	}
	file.close();

	for(int i=0;i<permutation_i_size;i++) {
		cout << permutation_i[i] << endl;
	}

	return 0;
}
